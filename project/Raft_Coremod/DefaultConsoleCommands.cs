﻿using HarmonyLib;
using HMLLibrary;
using RaftModLoader;
using Steamworks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using UltimateWater;
using UnityEngine;
using UnityEngine.AzureSky;
using UnityEngine.SceneManagement;
using RTCP = RaftModLoader.RTCP;

namespace RaftModLoader
{
    public class DefaultConsoleCommands : MonoBehaviour
    {
        public static List<string> safemodeDisabledCommands = new List<string>()
        {
            "getLoadedMods",
            "mod.reload",
            "mod.load",
            "mod.unload",
            "setConsoleMaxLogs",
            "resetConsoleMaxLogs",
            "timeset",
            "settimescale",
            "kill",
            "getcurrentlevel",
            "loadlevel",
            "poopmode",
            "noclip",
            "gotoraft",
            "togglecustomchat",
            "csrun",
            "safemode"
        };

        [ConsoleCommand("unityVersion", "Provides information about the Unity version utilized by the game.")]
        public static string UnityVersion()
        {
            return "Here's the Unity Engine Version: <b>" + Application.unityVersion + "</b>";
        }

        [ConsoleCommand("getLoadedMods", "Lists the mods currently loaded in the game.")]
        public static string LoadedMods()
        {
            string mods = "";
            if (ModManagerPage.activeModInstances.Count > 0)
            {
                mods = "Mods (" + ModManagerPage.activeModInstances.Count + ") : ";
                List<Mod> copy = ModManagerPage.activeModInstances.ToList();
                foreach (Mod mod in copy)
                {
                    mods += mod.name + "@" + mod.version + ",";
                }
                copy.Clear();
                copy = null;
                mods = mods.TrimEnd(',');
                mods += ".";
            }
            else
            {
                mods = "Mods (0) : No mods are currently loaded.";
            }
            return mods;
        }

        [ConsoleCommand("mod.reload", "Reload the specified mod. </color><color=#78c9ff>Syntax: mod.reload <mod name>")]
        public static async void ModReload(string[] args)
        {
            if (args.Length > 0)
            {
                string name = string.Join(" ", args);
                ModData md = ModManagerPage.modList.Find(x => x.jsonmodinfo.name.ToLower() == name.ToLower());
                if (md != null)
                {
                    if (md.modinfo.modState == ModInfo.ModStateEnum.running)
                    {
                        md.modinfo.modHandler.UnloadMod(md, true);
                        await Task.Delay(50);
                        md.modinfo.modHandler.LoadMod(md, true);
                    }
                    else
                    {
                        Debug.LogWarning("The mod \"" + md.jsonmodinfo.name + "\" wasn't running. Loading it...");
                        md.modinfo.modHandler.LoadMod(md, true);

                    }
                }
                else
                {
                    Debug.LogWarning("Couldn't find a mod with the name \"" + string.Join(" ", args) + "\"");
                }
            }
            else
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: mod.reload <mod name></color>");
            }
        }

        [ConsoleCommand("mod.load", "Load the specified mod. </color><color=#78c9ff>Syntax: mod.load <mod name>")]
        public static async void ModLoad(string[] args)
        {
            if (args.Length > 0)
            {
                string name = string.Join(" ", args);
                ModData md = ModManagerPage.modList.Find(x => x.jsonmodinfo.name.ToLower() == name.ToLower());
                if (md != null)
                {
                    if (md.modinfo.modState != ModInfo.ModStateEnum.running)
                    {
                        md.modinfo.modHandler.LoadMod(md, true);
                    }
                    else if (md.modinfo.modState == ModInfo.ModStateEnum.running)
                    {
                        md.modinfo.modHandler.UnloadMod(md, true);
                        await Task.Delay(50);
                        md.modinfo.modHandler.LoadMod(md, true);
                    }
                    else
                    {

                        Debug.LogWarning("The mod \"" + md.jsonmodinfo.name + "\" can't be loaded, check its status.");
                    }
                }
                else
                {
                    Debug.LogWarning("Couldn't find a mod with the name \"" + string.Join(" ", args) + "\"");
                }
            }
            else
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: mod.load <mod name></color>");
            }
        }

        [ConsoleCommand("mod.unload", "Unload the specified mod. </color><color=#78c9ff>Syntax: mod.unload <mod name>")]
        public static async void ModUnload(string[] args)
        {
            if (args.Length > 0)
            {
                string name = string.Join(" ", args);
                ModData md = ModManagerPage.modList.Find(x => x.jsonmodinfo.name.ToLower() == name.ToLower());
                if (md != null)
                {
                    if (md.modinfo.modState == ModInfo.ModStateEnum.running)
                    {
                        md.modinfo.modHandler.UnloadMod(md, true);
                    }
                    else
                    {
                        Debug.LogWarning("The mod \"" + md.jsonmodinfo.name + "\" needs to be running to be unloaded.");
                    }
                }
                else
                {
                    Debug.LogWarning("Couldn't find a mod with the name \"" + string.Join(" ", args) + "\"");
                }
            }
            else
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: mod.unload <mod name></color>");
            }
        }

        [ConsoleCommand("raftVersion", "Displays the current Raft version.", new[] { "version" })]
        public static string RaftVersion()
        {
            return "Here's the Raft version: <b>" + Settings.VersionNumberText + "</b>";
        }

        [ConsoleCommand("dotnetVersion", "Shows the current .NET version being utilized by the game environment.")]
        static string DotNetVersion()
        {
            return "Here's the .NET version: <b>" + Environment.Version.ToString() + "</b>";
        }

        [ConsoleCommand("clear", "Erases the content of the console, providing a clear and empty output space.")]
        public static void clear() => RConsole.ClearConsole();

        [ConsoleCommand("listRafters", "Displays all your friends currently playing Raft.")]
        private static void ListRafters()
        {
            int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
            List<string> rafters = new List<string>();
            for (int i = 0; i < friendCount; ++i)
            {
                CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
                string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
                FriendGameInfo_t gameInfo;
                SteamFriends.GetFriendGamePlayed(friendSteamId, out gameInfo);
                if (gameInfo.m_gameID.ToString() == "648800")
                {
                    rafters.Add(friendName + " is playing Raft! its SteamID is " + friendSteamId.GetAccountID().ToString());
                }
            }
            if (rafters.Count > 0)
            {
                Debug.Log("You have " + rafters.Count + " friend(s) currently playing Raft!");
                foreach (string s in rafters)
                {
                    Debug.Log(s);
                }
            }
            else
            {
                Debug.Log("None of your friends are currently playing Raft!");
            }
        }
        // CSteamID csteamid = new CSteamID(new AccountID_t(1), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
        //ServersPage.ConnectToServer(csteamid, "", false);
        [ConsoleCommand("connect", "Establishes a connection with the server specified. </color><color=#78c9ff>Syntax: connect <IP/SteamID> <password>")]
        public static async void TryJoinServer(string[] args)
        {
            if (args.Length < 1)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: connect <IP/SteamID> <password></color>");
                return;
            }
            string steamid = args[0].Replace("localhost", "127.0.0.1");
            string password = "";
            if (args.Length >= 2)
            {
                args[0] = null;
                password = String.Join(" ", args).TrimStart(' ');
            }
            if (steamid.Contains("."))
            {
                string ip = steamid.Split(':')[0];
                string sport = steamid.Contains(":") ? steamid.Split(':')[1] : "22000";
                int port = 22000;
                bool validPort = int.TryParse(sport, out port);
                if (!validPort)
                {
                    Debug.LogWarning("Syntax error! The specified port is invalid.");
                    return;
                }
                IPAddress address = null;
                bool valid = IPAddress.TryParse(ip, out address);
                if (valid)
                {
                    Debug.Log("Connecting to <b>" + ip + ":" + port + "</b>" + (password == "" ? "" : " with the specified password") + "...");
                    ServersPage.lastPassword = password;
                    RaftModLoader.RTCP.JoinServer(address, port);
                    return;
                }
                else
                {
                    Debug.LogWarning("Syntax error! The specified IP is invalid.");
                    return;
                }
            }
            try
            {
                if (steamid.Length > 5 && steamid.Length < 12)
                {
                    uint accountid = uint.Parse(steamid);
                    CSteamID csteamid = new CSteamID(new AccountID_t(accountid), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
                    if (!csteamid.IsValid())
                    {
                        Debug.LogWarning("Syntax error! The provided SteamID is invalid, Valid SteamID's are SteamID,SteamID64,SteamID3 and AccountID.");
                        return;
                    }
                    ServersPage.ConnectToServer(csteamid, password, false);
                    return;
                }
            }
            catch { }
            RSocket.convertSteamid(steamid, password);
            return;
        }

        [ConsoleCommand("fullscreenMode", "Adjusts the game display to the specified fullscreen mode. </color><color=#78c9ff>Syntax: fullscreenMode <windowed/fullscreen/borderless>")]
        public static void fullscreen(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: fullscreenMode <windowed/fullscreen/borderless></color>");
                return;
            }
            string mode = args[0].ToLower();
            switch (mode)
            {
                case "windowed":
                    Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.Windowed);
                    break;
                case "fullscreen":
                    Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.ExclusiveFullScreen);
                    break;
                case "borderless":
                    Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.FullScreenWindow);
                    break;
                default:
                    Debug.LogWarning("Syntax error! Invalid fullscreen mode, Valid modes are <i>Windowed</i>, <i>Fullscreen</i> or <i>Borderless</i>.");
                    break;
            }
        }

        [ConsoleCommand("setConsoleMaxLogs", "Alters the maximum number of logs displayed in the console. </color><color=#78c9ff>Syntax: setConsoleMaxLogs <amount>")]
        public static async void SetMaxLogs(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: setConsoleMaxLogs <amount></color>");
            }
            else
            {
                int newValue = -1;
                int.TryParse(args[0], out newValue);

                if (newValue < 1 || newValue > 10000)
                {
                    Debug.LogWarning("Syntax error! The value that you specified is invalid, It should be between 1 and 10000.");
                    return;
                }
                else
                {
                    PlayerPrefs.SetInt("rmlSettings_MaxLogs", newValue);
                    RConsole console = (RConsole.instance as RConsole);
                    RConsole.logPool.ForEach(x =>
                    {
                        GameObject.Destroy(x);
                    });
                    RConsole.logPool.Clear();
                    RConsole.logs.ForEach(x =>
                    {
                        GameObject.Destroy(x);
                    });
                    RConsole.logs.Clear();
                    RConsole.latestlog = null;
                    RConsole.MaxLogs = newValue;
                    await console.InitializeConsolePool();
                    Debug.Log("The maximum logs amount has been set to " + newValue + " !");
                }

            }
        }

        [ConsoleCommand("resetConsoleMaxLogs", "Restores the maximum number of logs in the console to its default setting.")]
        public static async void ResetMaxLogs(string[] args)
        {
            PlayerPrefs.DeleteKey("rmlSettings_MaxLogs");
            RConsole console = (RConsole.instance as RConsole);
            RConsole.logPool.ForEach(x =>
            {
                GameObject.Destroy(x);
            });
            RConsole.logPool.Clear();
            RConsole.logs.ForEach(x =>
            {
                GameObject.Destroy(x);
            });
            RConsole.logs.Clear();
            RConsole.latestlog = null;
            RConsole.MaxLogs = RConsole.DefaultMaxLogs;
            await console.InitializeConsolePool();
            Debug.Log("The maximum logs amount has been reseted to its default value (" + RConsole.DefaultMaxLogs + ")!");
        }

        [ConsoleCommand("timeset", "Adjusts the in-game time to the specified hour. </color><color=#78c9ff>Syntax: timeset <hour>", new[] { "settime" })]
        public static void Timeset(string[] args) => SetTime(args, true);

        public static void SetTime(string[] args, bool log = false)
        {
            if (args.Length != 1)
            {
                if (log)
                    Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: timeset <hour></color>");
            }
            else
            {
                int newValue = -1;
                int.TryParse(args[0], out newValue);

                if (newValue < 0 || newValue > 24)
                {
                    if (log)
                        Debug.LogWarning("Syntax error! The value that you specified is invalid, It should be between 0 and 24.");
                    return;
                }
                else
                {
                    bool timeFrozen = Patch_UpdateSky.freezetime;
                    if (timeFrozen) Patch_UpdateSky.freezetime = false;
                    FindObjectOfType<AzureSkyController>().timeOfDay.GotoTime(newValue);
                    Task.Run(async () =>
                    {
                        await Task.Delay(100);
                        UnityMainThreadDispatcher.Instance().Enqueue(() =>
                        {
                            if (timeFrozen) Patch_UpdateSky.freezetime = true;
                        });
                    });
                    if (log)
                        Debug.Log("The game time has been changed to " + newValue);
                }
            }
        }

        [ConsoleCommand("freezetime", "Halts or resume the progression of in-game time. </color><color=#78c9ff>Syntax: freezetime <none/time>", new[] { "pausetime" })]
        public static void Freezetime(string[] args)
        {
            Patch_UpdateSky.freezetime = !Patch_UpdateSky.freezetime;
            Debug.Log(!Patch_UpdateSky.freezetime ? "The game time has resumed." : "The game time has been paused.");
            if (args.Length != 0)
                SetTime(args);
        }

        [ConsoleCommand("settimescale", "Adjusts the speed at which game-time passes. </color><color=#78c9ff>Syntax: settimescale <speed>")]
        public static void Settimescale(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: settimescale <speed></color>");
                return;
            }

            int newValue = 1;
            int.TryParse(args[0], out newValue);

            if (newValue < 0 || newValue > 100)
            {
                Debug.LogWarning("Syntax error! The value that you specified is invalid, It should be between 0 and 100.");
                return;
            }
            else
            {
                Time.timeScale = newValue;
                Debug.Log("The game timescale has been changed to " + newValue + ".");
            }
        }

        [ConsoleCommand("kill", "Ends the life of your character in the game.")]
        public static void Kill()
        {
            Network_Player ply = RAPI.GetLocalPlayer();
            if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Raft_Network.GameSceneName)
            {
                Message_NetworkBehaviour message = new Message_NetworkBehaviour(Messages.PlayerKilled, ply);
                if (Raft_Network.IsHost)
                {
                    ply.Network.RPC(message, Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    ply.PlayerScript.Kill(false);
                    return;
                }
                ply.SendP2P(message, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);

                Debug.Log("The suicide note was written in blue ink.");
                return;
            }
            Debug.LogWarning("You can't suicide right now.");
        }

        [ConsoleCommand("exit", "Abandon ship! Escape the virtual realm and re-enter the real world.", new[] { "quit" })]
        public static void exit() => Application.Quit();

        [ConsoleCommand("getcurrentlevel", "Provides information about the current game level.")]
        public static string getcurrentlevel()
        {
            string result = "Current Level Info :";
            result += "\nName: <b>" + SceneManager.GetActiveScene().name + "</b>";
            result += "\nBuild Index: <b>" + SceneManager.GetActiveScene().buildIndex + "</b>";
            result += "\nPath: <b>" + SceneManager.GetActiveScene().path + "</b>";
            result += "\nGameObjects Count: <b>" + FindObjectsOfType<GameObject>().Length + "</b>";

            return result;
        }

        [ConsoleCommand("loadlevel", "Loads the specified level. </color><color=#78c9ff>Syntax: loadlevel <name>")]
        public static void loadlevel(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: loadlevel <name></color>");
                return;
            }
            else
            {
                Debug.Log("Loading scene " + args[0] + "...");
                SceneManager.LoadScene(args[0]);
            }
        }

        static PoopModeVariables pmv = new PoopModeVariables();
        [ConsoleCommand("poopmode", "Alters the game's graphics drastically to enhance performance on low-end computers.")]
        public static void PoopMode()
        {
            if (!pmv.IsEnabled)
            {
                pmv.IsEnabled = true;
                pmv.SaveCurrentVariables();
                QualitySettings.pixelLightCount = 0;
                QualitySettings.masterTextureLimit = 5;
                QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
                QualitySettings.antiAliasing = 0;
                QualitySettings.softParticles = false;
                QualitySettings.realtimeReflectionProbes = false;
                QualitySettings.billboardsFaceCameraPosition = false;
                QualitySettings.shadowCascades = 0;
                QualitySettings.shadows = ShadowQuality.Disable;
                QualitySettings.softVegetation = false;
                QualitySettings.vSyncCount = 0;
                QualitySettings.lodBias = 0;
                QualitySettings.maximumLODLevel = 0;
                WaterQualitySettings.Instance.SetQualityLevel(0);
                QualitySettings.shadowDistance = 0;
                QualitySettings.maxQueuedFrames = 1000;
                Settings settings = FindObjectOfType<Settings>();
                settings.graphicsBox.postEffects.ambientOcclusion.enabled = false;
                settings.graphicsBox.postEffects.antialiasing.enabled = false;
                settings.graphicsBox.postEffects.bloom.enabled = false;
                settings.graphicsBox.postEffects.chromaticAberration.enabled = false;
                settings.graphicsBox.postEffects.colorGrading.enabled = false;
                settings.graphicsBox.postEffects.debugViews.enabled = false;
                settings.graphicsBox.postEffects.depthOfField.enabled = false;
                settings.graphicsBox.postEffects.dithering.enabled = false;
                settings.graphicsBox.postEffects.eyeAdaptation.enabled = false;
                settings.graphicsBox.postEffects.fog.enabled = false;
                settings.graphicsBox.postEffects.grain.enabled = false;
                settings.graphicsBox.postEffects.motionBlur.enabled = false;
                settings.graphicsBox.postEffects.screenSpaceReflection.enabled = false;
                settings.graphicsBox.postEffects.userLut.enabled = false;
                settings.graphicsBox.postEffects.vignette.enabled = false;
                FindObjectOfType<VolumetricLightRenderer>().Resolution = VolumetricLightRenderer.VolumtericResolution.Quarter;
                FindObjectOfType<VolumetricLightRenderer>().enabled = false;
                Application.backgroundLoadingPriority = ThreadPriority.Low;
                Application.runInBackground = true;
            }
            else
            {
                pmv.IsEnabled = false;
                QualitySettings.pixelLightCount = pmv.pixelLightCount;
                QualitySettings.masterTextureLimit = pmv.masterTextureLimit;
                QualitySettings.anisotropicFiltering = pmv.anisotropicFiltering;
                QualitySettings.antiAliasing = pmv.antiAliasing;
                QualitySettings.softParticles = pmv.softParticles;
                QualitySettings.realtimeReflectionProbes = pmv.realtimeReflectionProbes;
                QualitySettings.billboardsFaceCameraPosition = pmv.billboardsFaceCameraPosition;
                QualitySettings.shadowCascades = pmv.shadowCascades;
                QualitySettings.shadows = pmv.shadows;
                QualitySettings.softVegetation = pmv.softVegetation;
                QualitySettings.vSyncCount = pmv.vSyncCount;
                QualitySettings.lodBias = pmv.lodBias;
                QualitySettings.maximumLODLevel = pmv.maximumLODLevel;
                WaterQualitySettings.Instance.SetQualityLevel(pmv.WaterQualityLevel);
                QualitySettings.shadowDistance = pmv.shadowDistance;
                QualitySettings.maxQueuedFrames = pmv.maxQueuedFrames;
                Settings settings = FindObjectOfType<Settings>();
                settings.graphicsBox.postEffects.ambientOcclusion.enabled = pmv.ambientOcclusion;
                settings.graphicsBox.postEffects.antialiasing.enabled = pmv.antialiasing;
                settings.graphicsBox.postEffects.bloom.enabled = pmv.bloom;
                settings.graphicsBox.postEffects.chromaticAberration.enabled = pmv.chromaticAberration;
                settings.graphicsBox.postEffects.colorGrading.enabled = pmv.colorGrading;
                settings.graphicsBox.postEffects.debugViews.enabled = pmv.debugViews;
                settings.graphicsBox.postEffects.depthOfField.enabled = pmv.depthOfField;
                settings.graphicsBox.postEffects.dithering.enabled = pmv.dithering;
                settings.graphicsBox.postEffects.eyeAdaptation.enabled = pmv.eyeAdaptation;
                settings.graphicsBox.postEffects.fog.enabled = pmv.fog;
                settings.graphicsBox.postEffects.grain.enabled = pmv.grain;
                settings.graphicsBox.postEffects.motionBlur.enabled = pmv.motionBlur;
                settings.graphicsBox.postEffects.screenSpaceReflection.enabled = pmv.screenSpaceReflection;
                settings.graphicsBox.postEffects.userLut.enabled = pmv.userLut;
                settings.graphicsBox.postEffects.vignette.enabled = pmv.vignette;
                FindObjectOfType<VolumetricLightRenderer>().Resolution = pmv.VolumtericResolution;
                FindObjectOfType<VolumetricLightRenderer>().enabled = pmv.VolumetricLightRenderer_Enabled;
                Application.backgroundLoadingPriority = pmv.backgroundLoadingPriority;
                Application.runInBackground = pmv.runInBackground;
            }
        }

        [ConsoleCommand("noclip", "Enables the flight camera mode.", new[] { "fly" })]
        public static void noclip()
        {
            Network_Player ply = RAPI.GetLocalPlayer();
            if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Raft_Network.GameSceneName)
            {
                if (ply.currentModel.thirdPersonSettings.ThirdPersonState)
                    ply.currentModel.thirdPersonSettings.ForceThirdPersonState(false);

                ply.flightCamera.Toggle(true);
                typeof(ThirdPerson).GetMethod("SetThirdPersonModel", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(ply.currentModel.thirdPersonSettings, new object[] { ply.flightCamera.enabled, true });
                return;
            }
            Debug.LogWarning("You can't use noclip right now.");
        }

        [ConsoleCommand("gotoraft", "Instantly transports you back to your raft, saving you from the perils of the open sea.", new[] { "tpraft" })]
        static void gotoraft()
        {
            Network_Player ply = RAPI.GetLocalPlayer();
            if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Raft_Network.GameSceneName)
            {
                ply.PersonController.CameraSubmersionChanged(UltimateWater.SubmersionState.None, true);
                ply.PersonController.SwitchControllerType(ControllerType.Ground);
                ply.SetToWalkableBlockPosition();
                ply.PlayerScript.MakeUnStuck();
                return;
            }
            Debug.LogWarning("You can't teleport to your raft right now.");
        }

        public static Dictionary<KeyCode, string> boundCommands = new Dictionary<KeyCode, string>();

        [ConsoleCommand("bind", "Links a specific console command to a designated key. </color><color=#78c9ff>Syntax: bind <key> <command>")]
        public static void BindCommand(string[] args)
        {
            if (args.Length < 2)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: bind <key> <command></color>");
                return;
            }

            KeyCode key = HUtils.KeyCodeFromString(args[0]);
            if (key == KeyCode.None) { return; }
            string command = string.Join(" ", args.Skip(1).ToArray());
            boundCommands[key] = command;
            UpdateBoundCommandsFile();
        }

        [ConsoleCommand("unbind", "Unlinks a specific console command from a designated key. </color><color=#78c9ff>Syntax: unbind <key>")]
        public static void UnbindCommand(string[] args)
        {
            if (args.Length < 1)
            {
                Debug.LogWarning("Syntax error! <color=#78c9ff>Correct Syntax: bind unbind <key></color>");
                return;
            }

            KeyCode key = HUtils.KeyCodeFromString(args[0]);
            if (key == KeyCode.None) { return; }
            boundCommands.Remove(key);
            UpdateBoundCommandsFile();
        }

        [ConsoleCommand("unbindall", "Detaches all console commands previously linked to keys.", new[] { "clearbinds" })]
        public static void Unbindall()
        {
            boundCommands.Clear();
            UpdateBoundCommandsFile();
        }

        public static void LoadBoundCommands()
        {
            if (PlayerPrefs.HasKey("rml4.boundcommands"))
            {
                JSONObject j = new JSONObject(PlayerPrefs.GetString("rml4.boundcommands"));
                if (j.IsArray && j.list.Count >= 1)
                {
                    foreach (JSONObject obj in j.list)
                    {
                        try
                        {
                            string[] args = obj.str.Split(new[] { "•◆•◆•" }, StringSplitOptions.None);
                            if (args.Length < 2)
                            {
                                Debug.LogWarning("You must specify a key and a command as arguments to 'bind'.");
                                return;
                            }

                            KeyCode key = HUtils.KeyCodeFromString(args[0]);
                            if (key == KeyCode.None) { return; }
                            string command = string.Join(" ", args.Skip(1).ToArray());
                            boundCommands[key] = command;
                            UpdateBoundCommandsFile();
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                    }
                    Debug.Log("Successfully loaded bound commands.");
                }
            }
        }

        public static void UpdateBoundCommandsFile()
        {
            string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RaftModLoader");
            JSONObject j = new JSONObject(JSONObject.Type.ARRAY);
            foreach (KeyValuePair<KeyCode, string> v in boundCommands)
            {
                j.Add(v.Key.ToString() + "•◆•◆•" + v.Value);
            }
            PlayerPrefs.SetString("rml4.boundcommands", j.Print());
        }
    }

    class PoopModeVariables
    {
        public bool IsEnabled;

        public int pixelLightCount;
        public int masterTextureLimit;
        public AnisotropicFiltering anisotropicFiltering;
        public int antiAliasing;
        public bool softParticles;
        public bool realtimeReflectionProbes;
        public bool billboardsFaceCameraPosition;
        public int shadowCascades;
        public ShadowQuality shadows;
        public bool softVegetation;
        public int vSyncCount;
        public float lodBias;
        public int maximumLODLevel;
        public int WaterQualityLevel;
        public float shadowDistance;
        public int maxQueuedFrames;
        public bool ambientOcclusion;
        public bool antialiasing;
        public bool bloom;
        public bool chromaticAberration;
        public bool colorGrading;
        public bool debugViews;
        public bool depthOfField;
        public bool dithering;
        public bool eyeAdaptation;
        public bool fog;
        public bool grain;
        public bool motionBlur;
        public bool screenSpaceReflection;
        public bool userLut;
        public bool vignette;
        public VolumetricLightRenderer.VolumtericResolution VolumtericResolution;
        public bool VolumetricLightRenderer_Enabled;
        public ThreadPriority backgroundLoadingPriority;
        public bool runInBackground;

        public void SaveCurrentVariables()
        {
            pixelLightCount = QualitySettings.pixelLightCount;
            masterTextureLimit = QualitySettings.masterTextureLimit;
            anisotropicFiltering = QualitySettings.anisotropicFiltering;
            antiAliasing = QualitySettings.antiAliasing;
            softParticles = QualitySettings.softParticles;
            realtimeReflectionProbes = QualitySettings.realtimeReflectionProbes;
            billboardsFaceCameraPosition = QualitySettings.billboardsFaceCameraPosition;
            shadowCascades = QualitySettings.shadowCascades;
            shadows = QualitySettings.shadows;
            softVegetation = QualitySettings.softVegetation;
            vSyncCount = QualitySettings.vSyncCount;
            lodBias = QualitySettings.lodBias;
            maximumLODLevel = QualitySettings.maximumLODLevel;
            WaterQualityLevel = WaterQualitySettings.Instance.GetQualityLevel();
            shadowDistance = QualitySettings.shadowDistance;
            maxQueuedFrames = QualitySettings.maxQueuedFrames;
            Settings settings = UnityEngine.GameObject.FindObjectOfType<Settings>();
            ambientOcclusion = settings.graphicsBox.postEffects.ambientOcclusion.enabled;
            antialiasing = settings.graphicsBox.postEffects.antialiasing.enabled;
            bloom = settings.graphicsBox.postEffects.bloom.enabled;
            chromaticAberration = settings.graphicsBox.postEffects.chromaticAberration.enabled;
            colorGrading = settings.graphicsBox.postEffects.colorGrading.enabled;
            debugViews = settings.graphicsBox.postEffects.debugViews.enabled;
            depthOfField = settings.graphicsBox.postEffects.depthOfField.enabled;
            dithering = settings.graphicsBox.postEffects.dithering.enabled;
            eyeAdaptation = settings.graphicsBox.postEffects.eyeAdaptation.enabled;
            fog = settings.graphicsBox.postEffects.fog.enabled;
            grain = settings.graphicsBox.postEffects.grain.enabled;
            motionBlur = settings.graphicsBox.postEffects.motionBlur.enabled;
            screenSpaceReflection = settings.graphicsBox.postEffects.screenSpaceReflection.enabled;
            userLut = settings.graphicsBox.postEffects.userLut.enabled;
            vignette = settings.graphicsBox.postEffects.vignette.enabled;
            VolumtericResolution = UnityEngine.GameObject.FindObjectOfType<VolumetricLightRenderer>().Resolution;
            VolumetricLightRenderer_Enabled = UnityEngine.GameObject.FindObjectOfType<VolumetricLightRenderer>().enabled;
            backgroundLoadingPriority = Application.backgroundLoadingPriority;
            runInBackground = Application.runInBackground;
        }
    }
}