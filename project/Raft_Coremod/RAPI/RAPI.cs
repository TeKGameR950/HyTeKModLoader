﻿using HarmonyLib;
using RaftModLoader;
using Sirenix.Serialization;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RAPI
{
    /// <summary>
    /// Checks if the game is running on a dedicated server.
    /// </summary>
    /// <returns>True if the game is running on a dedicated server, false otherwise.</returns>
    /// <method>public static bool IsDedicatedServer()</method>
    public static bool IsDedicatedServer() => false;

    /// <summary>
    /// Checks if the current active scene is the main menu.
    /// </summary>
    /// <returns>True if the current active scene is the main menu, false otherwise.</returns>
    /// <method>public static bool IsCurrentSceneMainMenu()</method>
    public static bool IsCurrentSceneMainMenu()
    {
        return SceneManager.GetActiveScene().name == "MainMenuScene";
    }

    /// <summary>
    /// Checks if the current active scene is the main game scene.
    /// </summary>
    /// <returns>True if the current active scene is the main game scene, false otherwise.</returns>
    /// <method>public static bool IsCurrentSceneGame()</method>
    public static bool IsCurrentSceneGame()
    {
        return SceneManager.GetActiveScene().name == "MainScene";
    }

    /// <summary>
    /// Gets the username associated with a SteamID.
    /// </summary>
    /// <param name="steamid">The SteamID of the player.</param>
    /// <returns>The player's username.</returns>
    /// <example>
    /// <code>
    /// string username = RAPI.GetUsernameFromSteamID(playerSteamID);
    /// Debug.Log("Player username: " + username);
    /// </code>
    /// </example>
    /// <method>public static string GetUsernameFromSteamID(CSteamID steamid)</method>
    public static string GetUsernameFromSteamID(CSteamID steamid)
    {
        string username = SteamFriends.GetFriendPersonaName(steamid);
        if (username.ToLower() != "[unknown]")
        {
            return username;
        }
        Network_Player player = ComponentManager<Raft_Network>.Value.GetPlayerFromID(steamid);
        if (player != null)
        {
            return player.playerNameTextMesh.text;
        }
        return username;
    }

    /// <summary>
    /// Toggle the mouse cursor with high priority (mostly used by the mod loader).
    /// </summary>
    /// <param name="status">A boolean value indicating whether to show or hide the cursor.</param>
    /// <example>
    /// <code>
    /// RAPI.ToggleCursor(true); // This will show the cursor.
    /// RAPI.ToggleCursor(false); // This will hide the cursor.
    /// </code>
    /// </example>
    /// <method>public static void TogglePriorityCursor(bool status)</method>
    public static void TogglePriorityCursor(bool status)
    {
        if (RAPI.IsCurrentSceneMainMenu())
            return;
        try
        {
            if (status)
            {
                if (CanvasHelper.ActiveMenu == MenuType.None)
                {
                    Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                    CanvasHelper.ActiveMenu = (MenuType)666;
                    var ch = ComponentManager<CanvasHelper>.Value;
                    ch.SetUIState(true);
                }
                if (CanvasHelper.ActiveMenu == (MenuType)666 && !Cursor.visible)
                {
                    Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                    CanvasHelper.ActiveMenu = (MenuType)666;
                    var ch = ComponentManager<CanvasHelper>.Value;
                    ch.SetUIState(true);
                }
            }
            else
            {
                if (RConsole.isOpen || MainMenu.IsOpen) { return; }
                if (CanvasHelper.ActiveMenu == (MenuType)666)
                {
                    Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);
                    CanvasHelper.ActiveMenu = MenuType.None;
                    var ch = ComponentManager<CanvasHelper>.Value;
                    ch.SetUIState(true);
                }
            }
        }
        catch { }
    }

    /// <summary>
    /// Toggle the mouse cursor.
    /// </summary>
    /// <param name="status">A boolean value indicating whether to show or hide the cursor.</param>
    /// <example>
    /// <code>
    /// RAPI.ToggleCursor(true); // This will show the cursor.
    /// RAPI.ToggleCursor(false); // This will hide the cursor.
    /// </code>
    /// </example>
    /// <method>public static void ToggleCursor(bool status)</method>
    public static void ToggleCursor(bool status)
    {
        if (IsCurrentSceneMainMenu())
            return;
        try
        {
            if (status)
            {
                Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                CanvasHelper.ActiveMenu = MenuType.PauseMenu;
                var ch = ComponentManager<CanvasHelper>.Value;
                ch.SetUIState(true);
            }
            else
            {
                Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);
                CanvasHelper.ActiveMenu = MenuType.None;
                var ch = ComponentManager<CanvasHelper>.Value;
                ch.SetUIState(true);
            }
        }
        catch { }
    }

    /// <summary>
    /// Gets the local player object from the network.
    /// </summary>
    /// <returns>The local player object.</returns>
    /// <example>
    /// <code>
    /// Network_Player player = RAPI.GetLocalPlayer();
    /// // The player variable will contain the local player script.
    /// </code>
    /// </example>
    /// <method>public static Network_Player GetLocalPlayer()</method>
    public static Network_Player GetLocalPlayer() => ComponentManager<Raft_Network>.Value.GetLocalPlayer();

    /// <summary>
    /// Broadcasts a chat message to all players.
    /// </summary>
    /// <param name="message">The chat message to broadcast.</param>
    /// <example>
    /// <code>
    /// RAPI.BroadcastChatMessage("I'm a message");
    /// // Will send "I'm a message" to every player.
    /// </code>
    /// </example>
    /// <method>public static void BroadcastChatMessage(string message)</method>
    public static void BroadcastChatMessage(string message)
    {
        ChatManager chatManager = ComponentManager<ChatManager>.Value;
        Raft_Network network = ComponentManager<Raft_Network>.Value;
        CSteamID csteamid = new CSteamID();
        Message_IngameChat nmessage = new Message_IngameChat(Messages.Ingame_Chat_Message, chatManager, csteamid, message);
        network.RPC(nmessage, Target.All, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }

    /// <summary>
    /// Gives a specified amount of items to the local player.
    /// </summary>
    /// <param name="item">The item to give to the player.</param>
    /// <param name="amount">The amount of the item to give.</param>
    /// <example>
    /// <code>
    /// RAPI.GiveItem(ItemManager.GetItemByName("item_name"), 5);
    /// // Gives 5 items of "item_name" to the local player.
    /// </code>
    /// </example>
    /// <method>public static void GiveItem(Item_Base item, int amount)</method>
    public static void GiveItem(Item_Base item, int amount)
    {
        ComponentManager<Raft_Network>.Value.GetLocalPlayer().Inventory.AddItem(item.UniqueName, amount);
    }

    /// <summary>
    /// Allows an item to be placed on a specific block quad type.
    /// </summary>
    /// <param name="item">The item to allow placement for.</param>
    /// <param name="quadtype">The block quad type to allow placement on.</param>
    /// <example>
    /// <code>
    /// RAPI.AddItemToBlockQuadType(YourNewItem, RBlockQuadType.quad_foundation);
    /// // Allow "YourNewItem" to be placed on foundations.
    /// </code>
    /// </example>
    /// <method>public static void AddItemToBlockQuadType(Item_Base item, RBlockQuadType quadtype)</method>
    public static void AddItemToBlockQuadType(Item_Base item, RBlockQuadType quadtype)
    {
        string quadtypestring = "blockquadtype/" + quadtype.ToString();
        List<Item_Base> customquadtype = Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").GetValue<List<Item_Base>>();
        customquadtype.Add(item);
        Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").SetValue(customquadtype);
    }

    /// <summary>
    /// Disallows an item from being placed on a specific block quad type.
    /// </summary>
    /// <param name="itemUniqueName">The unique name of the item to disallow placement for.</param>
    /// <param name="quadtype">The block quad type to disallow placement on.</param>
    /// <example>
    /// <code>
    /// RAPI.RemoveItemFromBlockQuadType("YourItemUniqueName", RBlockQuadType.quad_foundation);
    /// // Disallow the item with the uniquename "YourItemUniqueName" to be placed on foundations.
    /// </code>
    /// </example>
    /// <method>public static void RemoveItemFromBlockQuadType(string itemUniqueName, RBlockQuadType quadtype)</method>
    public static void RemoveItemFromBlockQuadType(string itemUniqueName, RBlockQuadType quadtype)
    {
        if (string.IsNullOrWhiteSpace(itemUniqueName))
        {
            throw new ArgumentNullException("itemUniqueName");
        }

        string quadtypestring = "blockquadtype/" + quadtype.ToString();
        List<Item_Base> customquadtype = Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").GetValue<List<Item_Base>>();

        customquadtype.RemoveAll(_o => _o.UniqueName == itemUniqueName);

        Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").SetValue(customquadtype);
    }

    /// <summary>
    /// Registers a new item.
    /// </summary>
    /// <param name="item">The item to register.</param>
    /// <param name="ignoreMaxValues">If true, ignores the maximum index and maximum uses validation. Default is false.</param>
    /// <example>
    /// <code>
    /// RAPI.RegisterItem(yourItem); // Replace with actual item initialization
    /// </code>
    /// </example>
    /// <method>public static void RegisterItem(Item_Base item, bool ignoreMaxValues = false)</method>
    public static void RegisterItem(Item_Base item, bool ignoreMaxValues = false)
    {
        if (item != null)
        {
            if (!ignoreMaxValues && item.UniqueIndex >= short.MaxValue)
            {
                Debug.LogError("[RAPI.RegisterItem()] Failed! > The item \"" + item.UniqueName + "\" has an invalid UniqueIndex (Needs to be less than " + short.MaxValue + ")!");
                return;
            }
            else
            {
                if (!ignoreMaxValues && item.MaxUses < 1)
                {
                    Debug.LogError("[RAPI.RegisterItem()] Failed! The MaxUses value for item \"" + item.UniqueName + "\" is lower than 1!");
                    return;
                }

                if (!ItemManager.GetItemByIndex(item.UniqueIndex))
                {
                    try
                    {
                        Block block = item.settings_buildable.GetBlockPrefab(0);
                        if (block is Storage_Small)
                        {
                            Traverse t = Traverse.Create(block).Field("animatorMessageForwarder");
                            if (t.GetValue() == null)
                            {
                                AnimatorMessageForwarder anim = block.gameObject.AddComponent<AnimatorMessageForwarder>();
                                t.SetValue(anim);
                            }
                        }
                    }
                    catch { }
                    List<Item_Base> list = Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").GetValue<List<Item_Base>>();
                    list.Add(item);
                    Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(list);
                }
                else
                {
                    Debug.LogError("[RAPI.RegisterItem()] Failed! > The item \"" + item.UniqueName + "\" can't be registered because the item \"" + ItemManager.GetItemByIndex(item.UniqueIndex).UniqueName + "\" already use that UniqueIndex!");
                    return;
                }
            }
        }
        else
        {
            Debug.LogError("[RAPI.RegisterItem()] Failed! > The method has been invoked with a null argument!");
            return;
        }
    }

    /// <summary>
    /// Unregisters an item, removing it from inventories and world.
    /// </summary>
    /// <param name="item">The item to unregister.</param>
    /// <example>
    /// <code>
    /// RAPI.UnregisterItem(ItemManager.GetItemByName("item_name"));
    /// </code>
    /// </example>
    /// <method>public static void UnregisterItem(Item_Base item)</method>
    public static void UnregisterItem(Item_Base item)
    {
        if (item != null)
        {
            if (GetLocalPlayer()?.Inventory?.GetSelectedHotbarItem()?.UniqueIndex == item.UniqueIndex)
                GetLocalPlayer().PlayerItemManager?.SelectUsable(null);

            List<Slot> slots = StorageManager.allStorages.SelectMany(x => x.GetInventoryReference() == null ? new List<Slot>() : x.GetInventoryReference().allSlots).ToList();
            if (GetLocalPlayer()?.Inventory?.allSlots?.Count > 0)
                slots.AddRange(GetLocalPlayer()?.Inventory?.allSlots);
            slots.Where(x => x.itemInstance?.UniqueIndex == item.UniqueIndex).ToList().ForEach(slot => slot.Reset());

            GameObject.FindObjectsOfType<DropItem>().ToList().ForEach(drop =>
            {
                if (drop.GetComponent<PickupItem>()?.itemInstance?.baseItem == item)
                    GameObject.Destroy(drop.gameObject);
            });

            if (InternalItemAPI.itemObjects.ContainsKey(item))
                InternalItemAPI.UnsetItemObject(item);
            foreach (var q in Resources.FindObjectsOfTypeAll<SO_BlockQuadType>())
                Traverse.Create(q).Field("acceptableBlockTypes").GetValue<List<Item_Base>>().RemoveAll(x => x.UniqueIndex == item.UniqueIndex);
            foreach (var q in Resources.FindObjectsOfTypeAll<SO_BlockCollisionMask>())
                Traverse.Create(q).Field("blockTypesToIgnore").GetValue<List<Item_Base>>().RemoveAll(x => x.UniqueIndex == item.UniqueIndex);
            ItemManager.GetAllItems().RemoveAll(x => x.UniqueIndex == item.UniqueIndex);
            foreach (var b in BlockCreator.GetPlacedBlocks())
                if (b.buildableItem != null && b.buildableItem.UniqueIndex == item.UniqueIndex)
                    BlockCreator.RemoveBlock(b, null, true);
        }
        else
        {
            Debug.LogError("[RAPI.UnregisterItem()] Failed! > The method has been invoked with a null argument!");
            return;
        }
    }

    /// <summary>
    /// Sets the in-hand prefab object for a specific item.
    /// </summary>
    /// <param name="item">The item to set the prefab for.</param>
    /// <param name="prefab">The prefab GameObject to assign to the item.</param>
    /// <param name="parent">The hand to attach the item to. Default is right hand.</param>
    /// <example>
    /// <code>
    /// Item_Base newItem = new Item_Base(); // Replace with actual item initialization
    /// GameObject itemPrefab = new GameObject(); // Replace with actual prefab initialization
    /// RAPI.RegisterItem(newItem);
    /// RAPI.SetItemObject(newItem, itemPrefab);
    /// </code>
    /// </example>
    /// <method>public static void SetItemObject(Item_Base item, GameObject prefab, RItemHand parent = RItemHand.rightHand)</method>
    public static void SetItemObject(Item_Base item, GameObject prefab, RItemHand parent = RItemHand.rightHand)
    {
        if (item == null || prefab == null)
        {
            Debug.LogError("[RAPI.SetItemObject()] Failed! > The method has been invoked with a null argument!");
            return;
        }
        if (!InternalItemAPI.itemObjects.ContainsKey(item))
        {
            InternalItemAPI.itemObjects.Add(item, new InternalItemAPI.ItemObject(prefab, parent));
            if (RAPI.IsCurrentSceneGame())
            {
                ComponentManager<Raft_Network>.Value.remoteUsers.ToList().ForEach(x =>
                {
                    InternalItemAPI.SetItemObject(x.Value);
                });
            }
        }
        else
            Debug.LogError("[RAPI.SetItemObject()] Failed! > The item \"" + item.UniqueName + "\" already has an object!");
    }

    /// <summary>
    /// Sends a network message to all players.
    /// </summary>
    /// <param name="message">The network message to send.</param>
    /// <param name="channel">The channel on which to send the message. Default is 0.</param>
    /// <param name="ep2psend">The method of sending the message. Default is reliable.</param>
    /// <param name="target">The target of the message. Default is Other.</param>
    /// <param name="fallbackSteamID">The fallback SteamID if the local player is not available. Default is an empty SteamID.</param>
    /// <example>
    /// <code>
    /// public enum CustomMessages
    /// {
    ///     MyCustomMessage = 8000
    /// }
    /// // This will send your network message to all players.
    /// RAPI.SendNetworkMessage(new YourMessageClass((Messages)CustomMessages.MyCustomMessage)); // Replace with your own message class.
    /// </code>
    /// </example>
    /// <method>public static void SendNetworkMessage(Message message, int channel = 0, EP2PSend ep2psend = EP2PSend.k_EP2PSendReliable, Target target = Target.Other, CSteamID fallbackSteamID = new CSteamID())</method>
    public static void SendNetworkMessage(Message message, int channel = 0, EP2PSend ep2psend = EP2PSend.k_EP2PSendReliable, Target target = Target.Other, CSteamID fallbackSteamID = new CSteamID())
    {
        if (message == null)
        {
            throw new NullReferenceException("Message was null in RAPI.SendNetworkMessage()");
        }
        if (GetLocalPlayer() == null)
        {
            Raft_Network network = ComponentManager<Raft_Network>.Value;
            network.SendP2P(fallbackSteamID, message, ep2psend, (NetworkChannel)channel);
            return;
        }
        if (Raft_Network.IsHost)
        {
            GetLocalPlayer().Network.RPC(message, target, ep2psend, (NetworkChannel)channel);
        }
        else
        {
            GetLocalPlayer().SendP2P(message, ep2psend, (NetworkChannel)channel);
        }
    }

    /// <summary>
    /// Listens for network messages on a specific network channel.
    /// </summary>
    /// <param name="channel">The channel on which to listen for messages. Must be a unique number greater than 1. Default is 2.</param>
    /// <returns>The received network message.</returns>
    /// <remarks>
    /// Choose a unique ID for the channel id to avoid interference with other mods. 
    /// Bigger than 1000 is recommended.
    /// </remarks>
    /// <example>
    /// <code>
    /// public enum CustomMessages
    /// {
    ///     MyCustomMessage = 8000
    /// }
    /// NetworkMessage netMessage = RAPI.ListenForNetworkMessagesOnChannel(15);
    /// if (netMessage != null)
    /// {
    ///     CSteamID id = netMessage.steamid;
    ///     Message message = netMessage.message;
    ///     // Here we use 8000 because we can't modify an enum, you can use any values 
    ///     // as long as its not in the Messages enum already. Bigger than 1000 is perfect.
    ///     if(message.Type == (Messages)CustomMessages.MyCustomMessage){
    ///         // Do your stuff with the message now that you know 
    ///         // its yours and its the wanted type.
    ///         YourMessageClass msg = message as YourMessageClass;
    ///     }
    /// }
    /// </code>
    /// </example>
    /// <method>public static NetworkMessage ListenForNetworkMessagesOnChannel(int channel = 2)</method>
    public static NetworkMessage ListenForNetworkMessagesOnChannel(int channel = 2)
    {
        if (channel <= 1)
        {
            Debug.LogError("RAPI.ListenForNetworkMessagesOnChannel() can't be used to listen for messages on the channel 0 and 1! Please choose a unique number for your mod.");
            return null;
        }

        uint pcubMsgSize;
        while (SteamNetworking.IsP2PPacketAvailable(out pcubMsgSize, channel))
        {
            byte[] array = new byte[pcubMsgSize];
            uint num2;
            CSteamID csteamID;
            if (SteamNetworking.ReadP2PPacket(array, pcubMsgSize, out num2, out csteamID, channel))
            {
                Packet packet = SerializationUtility.DeserializeValue<Packet>(array, DataFormat.Binary, new DeserializationContext() { Binder = new TwoWayPreMergeToMergedDeserializationBinder(Assembly.GetCallingAssembly()) });
                Packet_Multiple packet_Multiple;
                if (packet.PacketType == PacketType.Single)
                {
                    Packet_Single packet_Single = packet as Packet_Single;
                    packet_Multiple = new Packet_Multiple(packet_Single.SendType);
                    packet_Multiple.messages = new Message[]
                    {
                        packet_Single.message
                    };
                }
                else
                {
                    packet_Multiple = (packet as Packet_Multiple);
                }
                return new NetworkMessage(packet_Multiple.messages[0], csteamID, packet_Multiple.messages.ToList());
            }
        }
        return null;
    }
}

public class NetworkMessage
{
    public Message message;
    public CSteamID steamid;

    public List<Message> messages = new List<Message>();

    public NetworkMessage(Message m, CSteamID s, List<Message> messages)
    {
        message = m;
        steamid = s;
        this.messages = messages;
    }
}

public class TwoWayPreMergeToMergedDeserializationBinder : TwoWaySerializationBinder
{
    public Assembly assembly;

    public TwoWayPreMergeToMergedDeserializationBinder(Assembly assembly)
    {
        this.assembly = assembly;
    }

    public override string BindToName(Type type, DebugContext debugContext = null)
    {
        return type.Name;
    }

    public override Type BindToType(string typeName, DebugContext debugContext = null)
    {
        // OOF BUT FCK
        if (typeName.Contains("RDSModWrapperNamespace"))
        {
            string name = "RaftModLoader." + typeName.Split(',')[0].Split('.').ToList().Last();
            Type type = assembly.GetType(name);
            return type;
        }
        Type typeToDeserialize = null;
        String exeAssembly = Assembly.GetCallingAssembly().FullName;
        typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, exeAssembly));
        if (typeToDeserialize == null && typeName.Contains(","))
        {
            typeName = typeName.Split(',')[0];
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assembly.FullName));
        }

        return typeToDeserialize;
    }

    public override bool ContainsType(string typeName)
    {
        Type typeToDeserialize = null;
        String exeAssembly = Assembly.GetExecutingAssembly().FullName;
        typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, exeAssembly));
        return typeToDeserialize != null;
    }
}

public static class RPlayerExtentions
{
    // Update 12 fixing some ass mods
    [Obsolete("This is obsolete, please use .FindChildRecursively(childName) instead.")]
    public static Transform FindChildRecursivly(this Transform parent, string childName)
    {
        return parent.FindChildRecursively(childName);
    }

    public static void SendChatMessage(this Network_Player player, string message)
    {
        ChatManager chatManager = ComponentManager<ChatManager>.Value;
        Raft_Network network = ComponentManager<Raft_Network>.Value;
        CSteamID csteamid = new CSteamID();
        Message_IngameChat nmessage = new Message_IngameChat(Messages.Ingame_Chat_Message, chatManager, csteamid, message);
        network.SendP2P(player.steamID, nmessage, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
}

public enum RBlockQuadType
{
    quad_corner_all_empty,
    quad_corner_inv_empty,
    quad_corner_normal_empty,
    quad_floor,
    quad_floor_empty,
    quad_foundation,
    quad_foundation_empty,
    quad_itemnet_empty,
    quad_pillar_empty,
    quad_pipe_empty,
    quad_roof_straight_45_inv,
    quad_table,
    quad_tikipole,
    quad_wall,
    quad_walltop_empty,
};

public enum RItemHand
{
    leftHand,
    rightHand
};