﻿using UnityEngine;
using HMLLibrary;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;
#if GAME_IS_RAFT
using Steamworks;
#endif

namespace HMLLibrary
{
    public class Mod : MonoBehaviour
    {
        public ModData modlistEntry;

        [Obsolete("This might actually work but please use your own instance instead.")]
        public static Mod modInstance
        {
            get
            {
                var t = new System.Diagnostics.StackTrace();
                foreach (var f in t.GetFrames())
                {
                    var m = f.GetMethod().DeclaringType.Assembly;
                    if (m == null)
                        continue;
                    foreach (var mod in ModManagerPage.activeModInstances)
                        if (mod.GetType().Assembly == m)
                            return mod;
                }
                return null;
            }
        }
        public Dictionary<string, MethodInfo> methods = new Dictionary<string, MethodInfo>();
        public bool delayWorldLoading = false;

        public Mod() { }

        /// <summary>
        /// Allows mods to determine if they can be unloaded at the said moment.
        /// </summary>
        /// <param name="message">A message indicating the reason if the mod cannot be unloaded.</param>
        /// <example>
        /// <code>
        /// bool stillLoading = true;
        /// public override bool CanUnload(ref string message)
        /// {
        ///     if (stillLoading)
        ///     {
        ///         message = "The mod is still loading";
        ///         return false;
        ///     }
        ///     return base.CanUnload(ref message);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual bool CanUnload(ref string message)</method>
        public virtual bool CanUnload(ref string message) => CanUnload();
        public virtual bool CanUnload() => !modlistEntry.jsonmodinfo.isModPermanent;

        /// <summary>
        /// Unloads the mod.
        /// </summary>
        /// <method>public virtual void UnloadMod()</method>
        public virtual void UnloadMod() => modlistEntry.modinfo.modHandler.UnloadMod(modlistEntry);

        /// <summary>
        /// Gets the bytes of an embedded file in the mod.
        /// </summary>
        /// <param name="path">The path to the embedded file.</param>
        /// <returns>The bytes of the embedded file, or null if the file doesn't exist.</returns>
        /// <example>
        /// <code>
        /// byte[] myBundleBytes = GetEmbeddedFileBytes("mybundle.assets");
        /// AssetBundle bundle = AssetBundle.LoadFromMemory(myBundleBytes);
        /// </code>
        /// </example>
        /// <method>public virtual byte[] GetEmbeddedFileBytes(string path)</method>
        public virtual byte[] GetEmbeddedFileBytes(string path)
        {
            byte[] value = null;
            modlistEntry.modinfo.modFiles.TryGetValue(path, out value);
            if (value == null)
                Debug.LogError("[ModManager] " + modlistEntry.jsonmodinfo.name + " > The file " + path + " doesn't exist!");
            return value;
        }

        /// <summary>
        /// Gets the mod information.
        /// </summary>
        /// <returns>The mod information from the modinfo.json file.</returns>
        /// <example>
        /// <code>
        /// Debug.Log("Mod version: "+GetModInfo().version);
        /// </code>
        /// </example>
        /// <method>public JsonModInfo GetModInfo()</method>
        public JsonModInfo GetModInfo() { return modlistEntry.jsonmodinfo; }

        /// <summary>
        /// Logs a message with the mod name as a prefix.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <example>
        /// <code>
        /// Log("This is a message"); // Outputs: "[ModName] This is a message".
        /// </code>
        /// </example>
        /// <method>public void Log(object message)</method>
        public void Log(object message)
        {
            Debug.Log("[" + GetModInfo().name + "] : " + message.ToString());
        }

#if GAME_IS_RAFT

        /// <summary>
        /// The WorldEvent_WorldLoaded event triggers on world load complete.
        /// </summary>
        /// <example>
        /// <code>
        /// public override void WorldEvent_WorldLoaded()
        /// {
        ///     Debug.Log("The world has loaded");
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void WorldEvent_WorldLoaded()</method>
        public virtual void WorldEvent_WorldLoaded() { }

        /// <summary>
        /// The WorldEvent_WorldSaved event triggers on world save.
        /// </summary>
        /// <example>
        /// <code>
        /// public override void WorldEvent_WorldSaved()
        /// {
        ///     Debug.Log("The world has been saved");
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void WorldEvent_WorldSaved()</method>
        public virtual void WorldEvent_WorldSaved() { }

        /// <summary>
        /// The LocalPlayerEvent_Hurt event triggers when the local player takes damage.
        /// </summary>
        /// <param name="damage">The amount of damage taken.</param>
        /// <param name="hitPoint">The point where the damage was dealt.</param>
        /// <param name="hitNormal">The normal vector at the point of impact.</param>
        /// <param name="damageInflictorEntityType">The entity type causing the damage.</param>
        /// <example>
        /// <code>
        /// public override void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)
        /// {
        ///     Debug.Log("Player hurt: " + damage + " damage taken");
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)</method>
        public virtual void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType) { }

        /// <summary>
        /// The LocalPlayerEvent_Death event triggers when the local player dies.
        /// </summary>
        /// <param name="deathPosition">The position where the player died.</param>
        /// <example>
        /// <code>
        /// public override void LocalPlayerEvent_Death(Vector3 deathPosition)
        /// {
        ///     Debug.Log("Player died at: " + deathPosition);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void LocalPlayerEvent_Death(Vector3 deathPosition)</method>
        public virtual void LocalPlayerEvent_Death(Vector3 deathPosition) { }

        /// <summary>
        /// The LocalPlayerEvent_Respawn event triggers when the local player respawns.
        /// </summary>
        /// <example>
        /// <code>
        /// public override void LocalPlayerEvent_Respawn()
        /// {
        ///     Debug.Log("Player respawned");
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void LocalPlayerEvent_Respawn()</method>
        public virtual void LocalPlayerEvent_Respawn() { }

        /// <summary>
        /// The LocalPlayerEvent_ItemCrafted event triggers when the local player crafts an item.
        /// </summary>
        /// <param name="item">The crafted item.</param>
        /// <example>
        /// <code>
        /// public override void LocalPlayerEvent_ItemCrafted(Item_Base item)
        /// {
        ///     Debug.Log("Item crafted: " + item.UniqueName);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void LocalPlayerEvent_ItemCrafted(Item_Base item)</method>
        public virtual void LocalPlayerEvent_ItemCrafted(Item_Base item) { }

        /// <summary>
        /// The LocalPlayerEvent_PickupItem event triggers when the local player picks up a dropped item.
        /// </summary>
        /// <param name="item">The picked up item.</param>
        /// <example>
        /// <code>
        /// public override void LocalPlayerEvent_PickupItem(PickupItem item)
        /// {
        ///     Debug.Log("Picked up item: " + item.itemInstance?.UniqueName);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void LocalPlayerEvent_PickupItem(PickupItem item)</method>
        public virtual void LocalPlayerEvent_PickupItem(PickupItem item) { }

        /// <summary>
        /// The LocalPlayerEvent_DropItem event triggers when the local player drops an item.
        /// </summary>
        /// <param name="item">The dropped item.</param>
        /// <param name="position">The position where the item was dropped.</param>
        /// <param name="direction">The direction in which the item was dropped.</param>
        /// <param name="parentedToRaft">Specifies if the item is parented to the raft.</param>
        /// <example>
        /// <code>
        /// public override void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)
        /// {
        ///     Debug.Log("Dropped item: " + item.UniqueName + " at position: " + position);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)</method>
        public virtual void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft) { }

        /// <summary>
        /// The WorldEvent_OnPlayerConnected event triggers when a player connects to the world.
        /// </summary>
        /// <param name="steamid">The SteamID of the connected player.</param>
        /// <param name="characterSettings">The character settings of the connected player.</param>
        /// <example>
        /// <code>
        /// public override void WorldEvent_OnPlayerConnected(CSteamID steamid, RGD_Settings_Character characterSettings)
        /// {
        ///     Debug.Log("Player connected: " + steamid);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void WorldEvent_OnPlayerConnected(CSteamID steamid, RGD_Settings_Character characterSettings)</method>
        public virtual void WorldEvent_OnPlayerConnected(CSteamID steamid, RGD_Settings_Character characterSettings) { }

        /// <summary>
        /// The WorldEvent_OnPlayerDisconnected event triggers when a player disconnects from the world.
        /// </summary>
        /// <param name="steamid">The SteamID of the disconnected player.</param>
        /// <param name="disconnectReason">The reason for disconnection.</param>
        /// <example>
        /// <code>
        /// public override void WorldEvent_OnPlayerDisconnected(CSteamID steamid, DisconnectReason disconnectReason)
        /// {
        ///     Debug.Log("Player disconnected: " + steamid + " Reason: " + disconnectReason);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void WorldEvent_OnPlayerDisconnected(CSteamID steamid, DisconnectReason disconnectReason)</method>
        public virtual void WorldEvent_OnPlayerDisconnected(CSteamID steamid, DisconnectReason disconnectReason) { }

        /// <summary>
        /// The WorldEvent_WorldUnloaded event triggers on world unload.
        /// </summary>
        /// <example>
        /// <code>
        /// public override void WorldEvent_WorldUnloaded()
        /// {
        ///     Debug.Log("World unloaded");
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void WorldEvent_WorldUnloaded()</method>
        public virtual void WorldEvent_WorldUnloaded() { }

        /// <summary>
        /// The ModEvent_OnModLoaded event triggers when a mod is loaded.
        /// </summary>
        /// <param name="mod">The loaded mod.</param>
        /// <example>
        /// <code>
        /// public override void ModEvent_OnModLoaded(Mod mod)
        /// {
        ///     Debug.Log("Mod loaded: " + mod.name);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void ModEvent_OnModLoaded(Mod mod)</method>
        public virtual void ModEvent_OnModLoaded(Mod mod) { }

        /// <summary>
        /// The ModEvent_OnModUnloaded event triggers when a mod is unloaded.
        /// </summary>
        /// <param name="mod">The unloaded mod.</param>
        /// <example>
        /// <code>
        /// public override void ModEvent_OnModUnloaded(Mod mod)
        /// {
        ///     Debug.Log("Mod unloaded: " + mod.name);
        /// }
        /// </code>
        /// </example>
        /// <method>public virtual void ModEvent_OnModUnloaded(Mod mod)</method>
        public virtual void ModEvent_OnModUnloaded(Mod mod) { }
#endif

        public new string name
        {
            get
            {
                return GetModInfo().name;
            }
        }
        public string author
        {
            get
            {
                return GetModInfo().author;
            }
        }
        public string description
        {
            get
            {
                return GetModInfo().description;
            }
        }
        public string version
        {
            get
            {
                return GetModInfo().version;
            }
        }
        public string license
        {
            get
            {
                return GetModInfo().license;
            }
        }
        public string icon
        {
            get
            {
                return GetModInfo().icon;
            }
        }
        public string banner
        {
            get
            {
                return GetModInfo().banner;
            }
        }
        public string gameVersion
        {
            get
            {
                return GetModInfo().gameVersion;
            }
        }
        public string updateUrl
        {
            get
            {
                return GetModInfo().updateUrl;
            }
        }
        public bool isModPermanent
        {
            get
            {
                return GetModInfo().isModPermanent;
            }
        }
        public bool requiredByAllPlayers
        {
            get
            {
                return GetModInfo().requiredByAllPlayers;
            }
        }
        public List<string> excludedFiles
        {
            get
            {
                return GetModInfo().excludedFiles;
            }
        }
    }
}