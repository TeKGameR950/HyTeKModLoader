﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;

namespace HMLLibrary
{
    public static class HCacheManager
    {
        public static Dictionary<Assembly, byte[]> assemblies = new Dictionary<Assembly, byte[]>();

        public static async void CacheModAssembly(ModData md)
        {
            if (md.modinfo.assembly == null) return;
            if (assemblies.ContainsKey(md.modinfo.assembly) && assemblies[md.modinfo.assembly] != null)
            {
                byte[] loadedAssembly = assemblies[md.modinfo.assembly];
                string n = GetCachedModFileName(md);
                if (n == null) return;
                DeleteOldVersions(md);
                File.WriteAllBytes(Path.Combine(HLib.path_cacheFolder_mods, n), loadedAssembly);
            }
        }

        public static void DeleteOldVersions(ModData md)
        {
            Directory.GetFiles(HLib.path_cacheFolder_mods).ToList().ForEach(x =>
            {
                try
                {
                    if (x.EndsWith(GetCleanFileName(md.jsonmodinfo.name + md.jsonmodinfo.author) + Application.version + ".cachedmod"))
                        File.Delete(x);
                }
                catch { }
            });
        }

        public static async void CacheCsrunAssembly(string evalCode, byte[] asm)
        {
            File.WriteAllBytes(Path.Combine(HLib.path_cacheFolder_mods, await GetCachedCsrunFileName(evalCode)), asm);
        }

        public static Assembly GetCachedAssembly(ModData md)
        {
            string n = GetCachedModFileName(md);
            if (n == null) return null;
            string file = Path.Combine(HLib.path_cacheFolder_mods, n);
            try
            {
                if (File.Exists(file))
                    return Assembly.Load(File.ReadAllBytes(file));
            }
            catch
            {
                try
                {
                    File.Delete(file);
                }
                catch { }
            }
            return null;
        }

        public static async Task<Assembly> GetCachedAssembly(string evalCode)
        {
            string file = Path.Combine(HLib.path_cacheFolder_mods, await GetCachedCsrunFileName(evalCode));
            try
            {
                if (File.Exists(file))
                    return Assembly.Load(File.ReadAllBytes(file));
            }
            catch
            {
                try
                {
                    File.Delete(file);
                }
                catch { }
            }
            return null;
        }

        public static async void RemoveCachedVersion(string evalCode)
        {
            string file = Path.Combine(HLib.path_cacheFolder_mods, await GetCachedCsrunFileName(evalCode));
            try
            {
                File.Delete(file);
            }
            catch { }
        }
        public static void RemoveCachedVersion(ModData md)
        {
            string file = Path.Combine(HLib.path_cacheFolder_mods, GetCachedModFileName(md));
            try
            {
                File.Delete(file);
            }
            catch { }
        }

        public static string GetCachedModFileName(ModData md)
        {
            try
            {
                string n = md.modinfo.fileHash + md.modinfo.modFile.LastWriteTime.Ticks + GetCleanFileName(md.jsonmodinfo.name + md.jsonmodinfo.author) + Application.version + ".cachedmod";
                return n;
            }
            catch { return null; }
        }

        public static string GetCleanFileName(string input)
        {
            // Remove invalid characters (e.g., < > : " / \ | ? *)
            string cleanedFileName = Regex.Replace(input, @"[<>:""/\\|?*]", "");

            // Remove spaces and other special characters, allowing only alphanumeric characters
            cleanedFileName = Regex.Replace(cleanedFileName, @"[^a-zA-Z0-9]", "");

            return cleanedFileName;
        }

        public static async Task<string> GetCachedCsrunFileName(string command) => (await CRC32Hashing.String(command)) + command.Length.ToString() + ".cachedmod";
    }
}