﻿using AssemblyLoader;
using HCompiler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;
using RoslynCSharp.Compiler;
using ICSharpCode.SharpZipLib.Checksum;
using HarmonyLib;

namespace HMLLibrary
{
    public enum ExitCode : int
    {
        None = -1,
        Success = 0,
        CompileFailed = 1,
        ParseError = 2,
        ReferenceError = 4
    }

    public class BaseModHandler : MonoBehaviour
    {
        public static Dictionary<string, ModData> dataCache = new Dictionary<string, ModData>();

        public virtual async Task<ModData> GetModData(FileInfo file) { throw new NotImplementedException(); }

        public virtual List<byte[]> GetModReferences(ModData moddata)
        {
            return moddata.modinfo.modFiles.Where(x => x.Key.ToLower().EndsWith(".dll")).Select(x => x.Value).ToList();
        }

        public virtual async void LoadModReferences(ModData moddata)
        {
            if (HLoader.SAFEMODE) return;
            try
            {
                List<byte[]> refs = GetModReferences(moddata);
                if (refs.Count > 0)
                {
                    refs.ForEach(reference =>
                    {
                        // Rename assembly to support hotloading.
                        Stream stream = new MemoryStream(reference);
                        Mono.Cecil.AssemblyDefinition asmDef = Mono.Cecil.AssemblyDefinition.ReadAssembly(stream);
                        string originalName = asmDef.FullName;
                        asmDef.Name = new Mono.Cecil.AssemblyNameDefinition("modRef[" + DateTime.Now.Ticks + "]" + originalName, Version.Parse("1.0.0.0"));
                        //string path = Path.Combine(HLib.path_cacheFolder_temp, "modRef" + DateTime.Now.Ticks + ".dll");
                        MemoryStream _stream = new MemoryStream();
                        asmDef.Write(_stream);
                        try
                        {
                            Assembly asm = Assembly.Load(_stream.ToArray());
                            if (ModManagerPage.activeModReferences.ContainsKey(moddata.jsonmodinfo.name))
                                ModManagerPage.activeModReferences[moddata.jsonmodinfo.name].Add(asm);
                            else
                                ModManagerPage.activeModReferences.Add(moddata.jsonmodinfo.name, new List<Assembly> { asm });
                        }
                        catch (Exception ex)
                        {
                            Debug.LogWarning("[ModManager] " + moddata.jsonmodinfo.name + "> The reference \"" + originalName + "\" could not be loaded !\n" + ex.ToString());
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + "> An error occured when loading the mod reference dll's ! Stacktrace :\n" + ex.ToString());
            }
        }

        public virtual async Task<Assembly> GetModAssembly(ModData moddata)
        {
            if (HLoader.SAFEMODE) return null;
            Assembly potentialCachedVersion = HCacheManager.GetCachedAssembly(moddata);
            if (potentialCachedVersion != null)
                return potentialCachedVersion;
            Assembly assembly = null;

            if (ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name))
                ModManagerPage.loadedAssemblies.Remove(moddata.modinfo.modFile.Name);

            moddata.modinfo.modState = ModInfo.ModStateEnum.compiling;
            ModManagerPage.RefreshModState(moddata);

            Dictionary<string, string> csFiles = await Task.Run(() => moddata.modinfo.modFiles.Where(x => x.Key.ToLower().EndsWith(".cs")).ToDictionary(x => x.Key, x => Encoding.UTF8.GetString(x.Value)));
            List<byte[]> dllFiles = await Task.Run(() => moddata.modinfo.modFiles.Where(x => x.Key.ToLower().EndsWith(".dll")).Select(x => x.Value).ToList());
            CompilationResult result = await HCompiler.Main.CompileCode(moddata.jsonmodinfo.name, csFiles, dllFiles, false);
            try
            {
                LoadModReferences(moddata);
                assembly = result.OutputAssembly;
                if (assembly == null || result.OutputAssemblyImage == null) return null;
                HCacheManager.assemblies.Add(assembly, result.OutputAssemblyImage);
                Task.Run(async () =>
                {
                    await Task.Delay(10000);
                    if (HCacheManager.assemblies.ContainsKey(assembly))
                        HCacheManager.assemblies.Remove(assembly);
                });
            }
            catch (Exception ex)
            {
#if GAME_IS_RAFT
                Debug.LogError("[ModCompiler] " + Settings.VersionNumberText + " " + moddata.modinfo.modFile.Name + " > The mod failed to load!\n" + ex);
#else
                Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to load!\n" + ex);
#endif
                moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                ModManagerPage.RefreshModState(moddata);
            }
            return assembly;
        }

        public virtual async void LoadMod(ModData moddata, bool force = false)
        {
            if (HLoader.SAFEMODE) return;
            try
            {
                if (moddata == null || moddata.modinfo.modState == ModInfo.ModStateEnum.running || moddata.modinfo.modState == ModInfo.ModStateEnum.compiling) { return; }
                if (!File.Exists(moddata.modinfo.modFile.FullName))
                {
                    await ModManagerPage.RefreshMods();
                    moddata = ModManagerPage.modList.Find((m) => m.jsonmodinfo.name == moddata.jsonmodinfo.name);
                }
                if (!HLib.CanLoadMod(moddata) && !force)
                {
                    HNotify.Get().AddNotification(HNotify.NotificationType.normal, moddata.jsonmodinfo.name + " can't be loaded while players are online!", 5, await HLib.bundle.TaskLoadAssetAsync<Sprite>("IconError"));
                    return;
                }
                string hash = moddata.modinfo.isShortcut ? await CRC32Hashing.Folder(moddata.modinfo.shortcutFolder) : await CRC32Hashing.File(moddata.modinfo.modFile.FullName);
                bool modHasUpdated = hash != moddata.modinfo.fileHash;
                bool hashesMismatch = false;
                string fileName = moddata.modinfo.modFile.Name.ToLower();
                if (ModManagerPage.lastLoadedFileHash.ContainsKey(fileName))
                {
                    string oldHash = ModManagerPage.lastLoadedFileHash[fileName];
                    if (oldHash != hash)
                        hashesMismatch = true;
                }
                if (hashesMismatch) modHasUpdated = true;
                if (modHasUpdated)
                {
                    await ModManagerPage.RefreshMod(moddata.modinfo.modFile);
                    moddata = ModManagerPage.modList.Find((m) => m.modinfo.modFile.Name == moddata.modinfo.modFile.Name);
                }
                if (!ModManagerPage.ModsGameObjectParent.transform.Find(moddata.modinfo.modFile.Name.ToLower()))
                {
                    if (modHasUpdated || moddata.modinfo.assembly == null) { moddata.modinfo.assembly = await GetModAssembly(moddata); }
                    if (!ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name))
                    {
                        ModManagerPage.loadedAssemblies.Add(moddata.modinfo.modFile.Name, moddata.modinfo.assembly);
                    }
                    else
                    {
                        ModManagerPage.loadedAssemblies[moddata.modinfo.modFile.Name] = moddata.modinfo.assembly;
                    }
                    if (moddata.modinfo.assembly == null) { moddata.modinfo.modState = ModInfo.ModStateEnum.errored; }
                    else
                    {
                        List<Type> types = new List<Type>();
                        Type mainType = null;
                        try
                        {
                            types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod))).ToList();
                            mainType = types.FirstOrDefault();
                        }
                        catch
                        {
                            moddata.modinfo.assembly = null;
                            moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                            HCacheManager.RemoveCachedVersion(moddata);
                            Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + "> An error occured while retrieving the mod types!");
                        }
                        if (types.Count != 1)
                        {
                            Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + "> The mod codebase doesn't specify a mod class or specifies more than one!");
                            moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                        }
                        else
                        {
                            GameObject ModObj = new GameObject();
                            ModObj.SetActive(false);
                            ModObj.name = moddata.modinfo.modFile.Name.ToLower();
                            ModObj.transform.parent = ModManagerPage.ModsGameObjectParent.transform;
                            moddata.modinfo.mainClass = ModObj.AddComponent(mainType) as Mod;
                            ModManagerPage.activeModInstances.Add(moddata.modinfo.mainClass);
                            moddata.modinfo.mainClass.modlistEntry = moddata;
                            ModObj.SetActive(true);
                            moddata.modinfo.goInstance = ModObj;
                            moddata.modinfo.modState = ModInfo.ModStateEnum.running;
                            if (ModManagerPage.lastLoadedFileHash.ContainsKey(moddata.modinfo.modFile.Name.ToLower()))
                                ModManagerPage.lastLoadedFileHash[moddata.modinfo.modFile.Name.ToLower()] = moddata.modinfo.fileHash;
                            else
                                ModManagerPage.lastLoadedFileHash.Add(moddata.modinfo.modFile.Name.ToLower(), moddata.modinfo.fileHash);

                            ModManagerPage.activeModInstances.ForEach(mod =>
                            {
                                if (mod == null || moddata.modinfo.mainClass == null) return;
                                if (mod == moddata.modinfo.mainClass) return;
                                try
                                {
                                    moddata.modinfo.mainClass.ModEvent_OnModLoaded(mod);
                                }
                                catch (Exception ex)
                                {
                                    Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + "> An error occured when processing the OnModLoaded(" + mod?.name + ") event !\nStacktrace : " + ex.ToString());
                                }
                            });
                            ModManagerPage.activeModInstances.ForEach(mod =>
                            {
                                if (mod == null || moddata.modinfo.mainClass == null) return;
                                if (mod == moddata.modinfo.mainClass) return;
                                try
                                {
                                    mod.ModEvent_OnModLoaded(moddata.modinfo.mainClass);
                                }
                                catch (Exception ex)
                                {
                                    Debug.LogError("[ModManager] " + mod.name + "> An error occured when processing the OnModLoaded(" + moddata?.jsonmodinfo?.name + ") event !\nStacktrace : " + ex.ToString());
                                }
                            });
                            HConsole.instance.RefreshCommands();
                            HCacheManager.CacheModAssembly(moddata);
                        }
                    }
                }

                if (modHasUpdated)
                {
                    ModManagerPage.RefreshMod(moddata.modinfo.modFile);
                }
                else
                {
                    ModManagerPage.RefreshModState(moddata);
                }
            }
            catch (Exception e)
            {
                moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                HCacheManager.RemoveCachedVersion(moddata);
#if GAME_IS_RAFT
                Debug.LogError("[ModManager] " + Settings.VersionNumberText + " " + moddata.jsonmodinfo.name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
#else
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
#endif
            }
        }

        public virtual async void UnloadMod(ModData moddata, bool force = false)
        {
            if (HLoader.SAFEMODE) return;
            try
            {
                if (moddata == null || moddata.modinfo.modState == ModInfo.ModStateEnum.compiling) { return; }
                if (!HLib.CanUnloadMod(moddata.jsonmodinfo.name, moddata.jsonmodinfo.version))
                {
                    HNotify.Get().AddNotification(HNotify.NotificationType.normal, moddata.jsonmodinfo.name + " is required by the server and can't be unloaded!", 5, await HLib.bundle.TaskLoadAssetAsync<Sprite>("IconError"));
                    return;
                }

                if (moddata.modinfo.modState == ModInfo.ModStateEnum.running)
                {
                    string msg = "";
                    if (moddata.modinfo.mainClass != null && !moddata.modinfo.mainClass.CanUnload(ref msg) && !force)
                    {
                        if (msg == "")
                        {
                            msg = moddata.jsonmodinfo.name + " can't be unloaded right now !";
                        }
                        HNotify.Get().AddNotification(HNotify.NotificationType.normal, msg, 5, await HLib.bundle.TaskLoadAssetAsync<Sprite>("IconError"));
                        return;
                    }

                    ModManagerPage.activeModInstances.ForEach(m =>
                    {
                        if (m == null || moddata.modinfo.mainClass == null) return;
                        if (m == moddata.modinfo.mainClass) return;
                        try
                        {
                            m.ModEvent_OnModUnloaded(moddata.modinfo.mainClass);
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("[ModManager] " + m.name + "> An error occured when processing the OnModLoaded(" + moddata?.jsonmodinfo?.name + ") event !\nStacktrace : " + ex.ToString());
                        }
                    });

                    if (ModManagerPage.activeModReferences.ContainsKey(moddata.jsonmodinfo.name))
                        ModManagerPage.activeModReferences.Remove(moddata.jsonmodinfo.name);
                    Transform mod = ModManagerPage.ModsGameObjectParent.transform.Find(moddata.modinfo.modFile.Name.ToLower());
                    try
                    {
                        if (mod != null && moddata.modinfo.assembly != null)
                        {
                            IEnumerable<Type> types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                            if (mod.GetComponent(types.First()))
                            {
                                MethodInfo methodInfo = AccessTools.Method(types.First(), "OnModUnload");
                                if (methodInfo != null)
                                    if (methodInfo.IsStatic) { methodInfo.Invoke(null, null); } else { methodInfo.Invoke(mod.GetComponent(types.First()), null); }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > An error occured while running the mod OnModUnload() method!\nStacktrace : " + e.ToString());
                    }
                    if (mod != null)
                    {
                        Destroy(mod.gameObject);
                    }
                    if (ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name)) { ModManagerPage.loadedAssemblies.Remove(moddata.modinfo.modFile.Name); }
                    moddata.modinfo.modState = ModInfo.ModStateEnum.idle;
                    try
                    {
                        ModManagerPage.activeModInstances = ModManagerPage.activeModInstances.Where(x => x.modlistEntry.modinfo.modFile != moddata.modinfo.modFile).ToList();
                    }
                    catch { }
                    ModManagerPage.RefreshModState(moddata);
                    HConsole.instance.RefreshCommands();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > An error occured while unloading the mod!\nStacktrace : " + e.ToString());
            }
        }
    }
}