﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Newtonsoft.Json.Serialization
{
    public class MemoryTraceWriter : ITraceWriter
    {
        private readonly Queue<string> _traceMessages;
        public TraceLevel LevelFilter { get; set; }
        public MemoryTraceWriter()
        {
            LevelFilter = TraceLevel.Verbose;
            _traceMessages = new Queue<string>();
        }
        public void Trace(TraceLevel level, string message, Exception ex)
        {
            if (_traceMessages.Count >= 1000)
            {
                _traceMessages.Dequeue();
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff", CultureInfo.InvariantCulture));
            sb.Append(" ");
            sb.Append(level.ToString("g"));
            sb.Append(" ");
            sb.Append(message);

            _traceMessages.Enqueue(sb.ToString());
        }
        public IEnumerable<string> GetTraceMessages()
        {
            return _traceMessages;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string traceMessage in _traceMessages)
            {
                if (sb.Length > 0)
                {
                    sb.AppendLine();
                }

                sb.Append(traceMessage);
            }

            return sb.ToString();
        }
    }
}