#region License
// Copyright (c) 2007 James Newton-King
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Reflection;
using Newtonsoft.Json.Utilities;

#if NET20
using Newtonsoft.Json.Utilities.LinqBridge;
#endif

namespace Newtonsoft.Json.Serialization
{
    public class JsonProperty
    {
        internal Required? _required;
        internal bool _hasExplicitDefaultValue;

        private object _defaultValue;
        private bool _hasGeneratedDefaultValue;
        private string _propertyName;
        internal bool _skipPropertyNameEscape;
        private Type _propertyType;

        // use to cache contract during deserialization
        internal JsonContract PropertyContract { get; set; }
        public string PropertyName
        {
            get { return _propertyName; }
            set
            {
                _propertyName = value;
                _skipPropertyNameEscape = !JavaScriptUtils.ShouldEscapeJavaScriptString(_propertyName, JavaScriptUtils.HtmlCharEscapeFlags);
            }
        }
        public Type DeclaringType { get; set; }
        public int? Order { get; set; }
        public string UnderlyingName { get; set; }
        public IValueProvider ValueProvider { get; set; }
        public IAttributeProvider AttributeProvider { get; set; }
        public Type PropertyType
        {
            get { return _propertyType; }
            set
            {
                if (_propertyType != value)
                {
                    _propertyType = value;
                    _hasGeneratedDefaultValue = false;
                }
            }
        }
        public JsonConverter Converter { get; set; }
        public JsonConverter MemberConverter { get; set; }
        public bool Ignored { get; set; }
        public bool Readable { get; set; }
        public bool Writable { get; set; }
        public bool HasMemberAttribute { get; set; }
        public object DefaultValue
        {
            get
            {
                if (!_hasExplicitDefaultValue)
                {
                    return null;
                }

                return _defaultValue;
            }
            set
            {
                _hasExplicitDefaultValue = true;
                _defaultValue = value;
            }
        }

        internal object GetResolvedDefaultValue()
        {
            if (_propertyType == null)
            {
                return null;
            }

            if (!_hasExplicitDefaultValue && !_hasGeneratedDefaultValue)
            {
                _defaultValue = ReflectionUtils.GetDefaultValue(PropertyType);
                _hasGeneratedDefaultValue = true;
            }

            return _defaultValue;
        }
        public Required Required
        {
            get { return _required ?? Required.Default; }
            set { _required = value; }
        }
        public bool? IsReference { get; set; }
        public NullValueHandling? NullValueHandling { get; set; }
        public DefaultValueHandling? DefaultValueHandling { get; set; }
        public ReferenceLoopHandling? ReferenceLoopHandling { get; set; }
        public ObjectCreationHandling? ObjectCreationHandling { get; set; }
        public TypeNameHandling? TypeNameHandling { get; set; }
        public Predicate<object> ShouldSerialize { get; set; }
        public Predicate<object> ShouldDeserialize { get; set; }
        public Predicate<object> GetIsSpecified { get; set; }
        public Action<object, object> SetIsSpecified { get; set; }
        public override string ToString()
        {
            return PropertyName;
        }
        public JsonConverter ItemConverter { get; set; }
        public bool? ItemIsReference { get; set; }
        public TypeNameHandling? ItemTypeNameHandling { get; set; }
        public ReferenceLoopHandling? ItemReferenceLoopHandling { get; set; }

        internal void WritePropertyName(JsonWriter writer)
        {
            if (_skipPropertyNameEscape)
            {
                writer.WritePropertyName(PropertyName, false);
            }
            else
            {
                writer.WritePropertyName(PropertyName);
            }
        }
    }
}