﻿
#if (DOTNET || PORTABLE40 || PORTABLE)
using Newtonsoft.Json.Serialization;

namespace Newtonsoft.Json
{
    public enum TraceLevel
    {
        Off = 0,
        Error = 1,
        Warning = 2,
        Info = 3,
        Verbose = 4
    }
}

#endif