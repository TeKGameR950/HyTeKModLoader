﻿
#if DOTNET || PORTABLE40 || PORTABLE

namespace System.Runtime.Serialization.Formatters
{
    public enum FormatterAssemblyStyle
    {
        Simple = 0,
        Full = 1
    }
}

#endif