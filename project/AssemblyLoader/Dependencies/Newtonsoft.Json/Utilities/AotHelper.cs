﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Newtonsoft.Json.Utilities
{
    public class AotHelper
    {
        public static void Ensure(Action action)
        {
            if (IsFalse())
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException("", e);
                }
            }
        }
        public static void EnsureType<T>() where T : new()
        {
            Ensure(() => new T());
        }
        public static void EnsureList<T>()
        {
            Ensure(() =>
            {
                var a = new List<T>();
                var b = new HashSet<T>();
                var c = new CollectionWrapper<T>((IList)a);
                var d = new CollectionWrapper<T>((ICollection<T>)a);
            });
        }
        public static void EnsureDictionary<TKey, TValue>()
        {
            Ensure(() =>
            {
                var a = new Dictionary<TKey, TValue>();
                var b = new DictionaryWrapper<TKey, TValue>((IDictionary)null);
                var c = new DictionaryWrapper<TKey, TValue>((IDictionary<TKey, TValue>)null);
            });
        }

        private static bool s_alwaysFalse = DateTime.UtcNow.Year < 0;
        public static bool IsFalse()
        {
            return s_alwaysFalse;
        }
    }
}
