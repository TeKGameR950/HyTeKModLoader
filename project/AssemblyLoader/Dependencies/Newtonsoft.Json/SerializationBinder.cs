﻿
#if (DOTNET || PORTABLE40 || PORTABLE)
using System;
using System.Reflection;

namespace Newtonsoft.Json
{
    public abstract class SerializationBinder
    {
        public abstract Type BindToType(string assemblyName, string typeName);
        public virtual void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = null;
        }
    }
}

#endif