﻿#region License
// Copyright (c) 2007 James Newton-King
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion

#if !NO_JSONLINQ

using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Utilities;
using System.Globalization;

namespace Newtonsoft.Json.Linq
{
    public class JConstructor : JContainer
    {
        private string _name;
        private readonly List<JToken> _values = new List<JToken>();
        protected override IList<JToken> ChildrenTokens
        {
            get { return _values; }
        }

        internal override int IndexOfItem(JToken item)
        {
            return _values.IndexOfReference(item);
        }

        internal override void MergeItem(object content, JsonMergeSettings settings)
        {
            JConstructor c = content as JConstructor;
            if (c == null)
            {
                return;
            }

            if (c.Name != null)
            {
                Name = c.Name;
            }
            MergeEnumerableContent(this, c, settings);
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public override JTokenType Type
        {
            get { return JTokenType.Constructor; }
        }
        public JConstructor()
        {
        }
        public JConstructor(JConstructor other)
            : base(other)
        {
            _name = other.Name;
        }
        public JConstructor(string name, params object[] content)
            : this(name, (object)content)
        {
        }
        public JConstructor(string name, object content)
            : this(name)
        {
            Add(content);
        }
        public JConstructor(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (name.Length == 0)
            {
                throw new ArgumentException("Constructor name cannot be empty.", nameof(name));
            }

            _name = name;
        }

        internal override bool DeepEquals(JToken node)
        {
            JConstructor c = node as JConstructor;
            return (c != null && _name == c.Name && ContentsEqual(c));
        }

        internal override JToken CloneToken()
        {
            return new JConstructor(this);
        }
        public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
        {
            writer.WriteStartConstructor(_name);

            foreach (JToken token in Children())
            {
                token.WriteTo(writer, converters);
            }

            writer.WriteEndConstructor();
        }
        public override JToken this[object key]
        {
            get
            {
                ValidationUtils.ArgumentNotNull(key, nameof(key));

                if (!(key is int))
                {
                    throw new ArgumentException("Accessed JConstructor values with invalid key value: {0}. Argument position index expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
                }

                return GetItem((int)key);
            }
            set
            {
                ValidationUtils.ArgumentNotNull(key, nameof(key));

                if (!(key is int))
                {
                    throw new ArgumentException("Set JConstructor values with invalid key value: {0}. Argument position index expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
                }

                SetItem((int)key, value);
            }
        }

        internal override int GetDeepHashCode()
        {
            return _name.GetHashCode() ^ ContentsHashCode();
        }
        public new static JConstructor Load(JsonReader reader)
        {
            return Load(reader, null);
        }
        public new static JConstructor Load(JsonReader reader, JsonLoadSettings settings)
        {
            if (reader.TokenType == JsonToken.None)
            {
                if (!reader.Read())
                {
                    throw JsonReaderException.Create(reader, "Error reading JConstructor from JsonReader.");
                }
            }

            reader.MoveToContent();

            if (reader.TokenType != JsonToken.StartConstructor)
            {
                throw JsonReaderException.Create(reader, "Error reading JConstructor from JsonReader. Current JsonReader item is not a constructor: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
            }

            JConstructor c = new JConstructor((string)reader.Value);
            c.SetLineInfo(reader as IJsonLineInfo, settings);

            c.ReadTokenFrom(reader, settings);

            return c;
        }
    }
}

#endif
