﻿#if !NO_JSONLINQ

using System;

namespace Newtonsoft.Json.Linq
{
    [Flags]
    public enum MergeNullValueHandling
    {
        Ignore = 0,
        Merge = 1
    }
}

#endif
