﻿#if NO_JSONLINQ

// Dummy namespace
namespace Newtonsoft.Json.Linq
{
    class Dummy { }
}

#else

namespace Newtonsoft.Json.Linq
{
    public enum CommentHandling
    {
        Ignore = 0,
        Load = 1
    }
    public enum LineInfoHandling
    {
        Ignore = 0,
        Load = 1
    }
}

#endif