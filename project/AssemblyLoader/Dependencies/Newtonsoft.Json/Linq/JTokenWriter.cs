﻿#region License
// Copyright (c) 2007 James Newton-King
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion

#if !NO_JSONLINQ

using System;
using System.Globalization;
#if (NET20 || NET35 || PORTABLE40 || PORTABLE)
using System.Numerics;
#endif
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
    public class JTokenWriter : JsonWriter
    {
        private JContainer _token;
        private JContainer _parent;
        // used when writer is writing single value and the value has no containing parent
        private JValue _value;
        private JToken _current;
        public JToken CurrentToken
        {
            get { return _current; }
        }
        public JToken Token
        {
            get
            {
                if (_token != null)
                {
                    return _token;
                }

                return _value;
            }
        }
        public JTokenWriter(JContainer container)
        {
            ValidationUtils.ArgumentNotNull(container, nameof(container));

            _token = container;
            _parent = container;
        }
        public JTokenWriter()
        {
        }
        public override void Flush()
        {
        }
        public override void Close()
        {
            base.Close();
        }
        public override void WriteStartObject()
        {
            base.WriteStartObject();

            AddParent(new JObject());
        }

        private void AddParent(JContainer container)
        {
            if (_parent == null)
            {
                _token = container;
            }
            else
            {
                _parent.AddAndSkipParentCheck(container);
            }

            _parent = container;
            _current = container;
        }

        private void RemoveParent()
        {
            _current = _parent;
            _parent = _parent.Parent;

            if (_parent != null && _parent.Type == JTokenType.Property)
            {
                _parent = _parent.Parent;
            }
        }
        public override void WriteStartArray()
        {
            base.WriteStartArray();

            AddParent(new JArray());
        }
        public override void WriteStartConstructor(string name)
        {
            base.WriteStartConstructor(name);

            AddParent(new JConstructor(name));
        }
        protected override void WriteEnd(JsonToken token)
        {
            RemoveParent();
        }
        public override void WritePropertyName(string name)
        {
            JObject o = _parent as JObject;
            if (o != null)
            {
                // avoid duplicate property name exception
                // last property name wins
                o.Remove(name);
            }

            AddParent(new JProperty(name));

            // don't set state until after in case of an error
            // incorrect state will cause issues if writer is disposed when closing open properties
            base.WritePropertyName(name);
        }

        private void AddValue(object value, JsonToken token)
        {
            AddValue(new JValue(value), token);
        }

        internal void AddValue(JValue value, JsonToken token)
        {
            if (_parent != null)
            {
                _parent.Add(value);
                _current = _parent.Last;

                if (_parent.Type == JTokenType.Property)
                {
                    _parent = _parent.Parent;
                }
            }
            else
            {
                _value = value ?? JValue.CreateNull();
                _current = _value;
            }
        }

#region WriteValue methods
        public override void WriteValue(object value)
        {
#if (NET20 || NET35 || PORTABLE || PORTABLE40)
            if (value is BigInteger)
            {
                InternalWriteValue(JsonToken.Integer);
                AddValue(value, JsonToken.Integer);
            }
            else
#endif
            {
                base.WriteValue(value);
            }
        }
        public override void WriteNull()
        {
            base.WriteNull();
            AddValue(null, JsonToken.Null);
        }
        public override void WriteUndefined()
        {
            base.WriteUndefined();
            AddValue(null, JsonToken.Undefined);
        }
        public override void WriteRaw(string json)
        {
            base.WriteRaw(json);
            AddValue(new JRaw(json), JsonToken.Raw);
        }
        public override void WriteComment(string text)
        {
            base.WriteComment(text);
            AddValue(JValue.CreateComment(text), JsonToken.Comment);
        }
        public override void WriteValue(string value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.String);
        }
        public override void WriteValue(int value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        
        public override void WriteValue(uint value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        public override void WriteValue(long value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        
        public override void WriteValue(ulong value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        public override void WriteValue(float value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Float);
        }
        public override void WriteValue(double value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Float);
        }
        public override void WriteValue(bool value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Boolean);
        }
        public override void WriteValue(short value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        
        public override void WriteValue(ushort value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        public override void WriteValue(char value)
        {
            base.WriteValue(value);
            string s = null;
#if !(DOTNET || PORTABLE40 || PORTABLE)
            s = value.ToString(CultureInfo.InvariantCulture);
#else
            s = value.ToString();
#endif
            AddValue(s, JsonToken.String);
        }
        public override void WriteValue(byte value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        
        public override void WriteValue(sbyte value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Integer);
        }
        public override void WriteValue(decimal value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Float);
        }
        public override void WriteValue(DateTime value)
        {
            base.WriteValue(value);
            value = DateTimeUtils.EnsureDateTime(value, DateTimeZoneHandling);
            AddValue(value, JsonToken.Date);
        }

#if !NET20
        public override void WriteValue(DateTimeOffset value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Date);
        }
#endif
        public override void WriteValue(byte[] value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.Bytes);
        }
        public override void WriteValue(TimeSpan value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.String);
        }
        public override void WriteValue(Guid value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.String);
        }
        public override void WriteValue(Uri value)
        {
            base.WriteValue(value);
            AddValue(value, JsonToken.String);
        }
#endregion

        internal override void WriteToken(JsonReader reader, bool writeChildren, bool writeDateConstructorAsDate, bool writeComments)
        {
            JTokenReader tokenReader = reader as JTokenReader;

            // closing the token wrather than reading then writing it doesn't lose some type information, e.g. Guid, byte[], etc
            if (tokenReader != null && writeChildren && writeDateConstructorAsDate && writeComments)
            {
                if (tokenReader.TokenType == JsonToken.None)
                {
                    if (!tokenReader.Read())
                    {
                        return;
                    }
                }

                JToken value = tokenReader.CurrentToken.CloneToken();

                if (_parent != null)
                {
                    _parent.Add(value);
                    _current = _parent.Last;

                    // if the writer was in a property then move out of it and up to its parent object
                    if (_parent.Type == JTokenType.Property)
                    {
                        _parent = _parent.Parent;
                        InternalWriteValue(JsonToken.Null);
                    }
                }
                else
                {
                    _current = value;

                    if (_token == null && _value == null)
                    {
                        _token = value as JContainer;
                        _value = value as JValue;
                    }
                }

                tokenReader.Skip();
            }
            else
            {
                base.WriteToken(reader, writeChildren, writeDateConstructorAsDate, writeComments);
            }
        }
    }
}

#endif
