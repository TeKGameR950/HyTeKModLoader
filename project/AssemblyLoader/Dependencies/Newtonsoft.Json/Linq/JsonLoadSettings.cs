﻿#if !NO_JSONLINQ

using System;

namespace Newtonsoft.Json.Linq
{
    public class JsonLoadSettings
    {
        private CommentHandling _commentHandling;
        private LineInfoHandling _lineInfoHandling;
        public CommentHandling CommentHandling
        {
            get { return _commentHandling; }
            set
            {
                if (value < CommentHandling.Ignore || value > CommentHandling.Load)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                _commentHandling = value;
            }
        }
        public LineInfoHandling LineInfoHandling
        {
            get { return _lineInfoHandling; }
            set
            {
                if (value < LineInfoHandling.Ignore || value > LineInfoHandling.Load)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                _lineInfoHandling = value;
            }
        }
    }
}

#endif
