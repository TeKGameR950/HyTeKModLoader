﻿#region License
// Copyright (c) 2007 James Newton-King
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion

#if !NO_JSONLINQ

using System;
using System.Collections.Generic;
using Newtonsoft.Json.Utilities;
using System.Globalization;
#if NET20
using Newtonsoft.Json.Utilities.LinqBridge;
#else
using System.Linq;

#endif

namespace Newtonsoft.Json.Linq
{
    public static class Extensions
    {
        public static IJEnumerable<JToken> Ancestors<T>(this IEnumerable<T> source) where T : JToken
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            return source.SelectMany(j => j.Ancestors()).AsJEnumerable();
        }
        public static IJEnumerable<JToken> AncestorsAndSelf<T>(this IEnumerable<T> source) where T : JToken
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            return source.SelectMany(j => j.AncestorsAndSelf()).AsJEnumerable();
        }
        public static IJEnumerable<JToken> Descendants<T>(this IEnumerable<T> source) where T : JContainer
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            return source.SelectMany(j => j.Descendants()).AsJEnumerable();
        }
        public static IJEnumerable<JToken> DescendantsAndSelf<T>(this IEnumerable<T> source) where T : JContainer
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            return source.SelectMany(j => j.DescendantsAndSelf()).AsJEnumerable();
        }
        public static IJEnumerable<JProperty> Properties(this IEnumerable<JObject> source)
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            return source.SelectMany(d => d.Properties()).AsJEnumerable();
        }
        public static IJEnumerable<JToken> Values(this IEnumerable<JToken> source, object key)
        {
            return Values<JToken, JToken>(source, key).AsJEnumerable();
        }
        public static IJEnumerable<JToken> Values(this IEnumerable<JToken> source)
        {
            return source.Values(null);
        }
        public static IEnumerable<U> Values<U>(this IEnumerable<JToken> source, object key)
        {
            return Values<JToken, U>(source, key);
        }
        public static IEnumerable<U> Values<U>(this IEnumerable<JToken> source)
        {
            return Values<JToken, U>(source, null);
        }
        public static U Value<U>(this IEnumerable<JToken> value)
        {
            return value.Value<JToken, U>();
        }
        public static U Value<T, U>(this IEnumerable<T> value) where T : JToken
        {
            ValidationUtils.ArgumentNotNull(value, nameof(value));

            JToken token = value as JToken;
            if (token == null)
            {
                throw new ArgumentException("Source value must be a JToken.");
            }

            return token.Convert<JToken, U>();
        }

        internal static IEnumerable<U> Values<T, U>(this IEnumerable<T> source, object key) where T : JToken
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            foreach (JToken token in source)
            {
                if (key == null)
                {
                    if (token is JValue)
                    {
                        yield return Convert<JValue, U>((JValue)token);
                    }
                    else
                    {
                        foreach (JToken t in token.Children())
                        {
                            yield return t.Convert<JToken, U>();
                        }
                    }
                }
                else
                {
                    JToken value = token[key];
                    if (value != null)
                    {
                        yield return value.Convert<JToken, U>();
                    }
                }
            }

            yield break;
        }

        //TODO
        //public static IEnumerable<T> InDocumentOrder<T>(this IEnumerable<T> source) where T : JObject;
        public static IJEnumerable<JToken> Children<T>(this IEnumerable<T> source) where T : JToken
        {
            return Children<T, JToken>(source).AsJEnumerable();
        }
        public static IEnumerable<U> Children<T, U>(this IEnumerable<T> source) where T : JToken
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            return source.SelectMany(c => c.Children()).Convert<JToken, U>();
        }

        internal static IEnumerable<U> Convert<T, U>(this IEnumerable<T> source) where T : JToken
        {
            ValidationUtils.ArgumentNotNull(source, nameof(source));

            foreach (T token in source)
            {
                yield return Convert<JToken, U>(token);
            }
        }

        internal static U Convert<T, U>(this T token) where T : JToken
        {
            if (token == null)
            {
                return default(U);
            }

            if (token is U
                // don't want to cast JValue to its interfaces, want to get the internal value
                && typeof(U) != typeof(IComparable) && typeof(U) != typeof(IFormattable))
            {
                // HACK
                return (U)(object)token;
            }
            else
            {
                JValue value = token as JValue;
                if (value == null)
                {
                    throw new InvalidCastException("Cannot cast {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, token.GetType(), typeof(T)));
                }

                if (value.Value is U)
                {
                    return (U)value.Value;
                }

                Type targetType = typeof(U);

                if (ReflectionUtils.IsNullableType(targetType))
                {
                    if (value.Value == null)
                    {
                        return default(U);
                    }

                    targetType = Nullable.GetUnderlyingType(targetType);
                }

                return (U)System.Convert.ChangeType(value.Value, targetType, CultureInfo.InvariantCulture);
            }
        }

        //TODO
        //public static void Remove<T>(this IEnumerable<T> source) where T : JContainer;
        public static IJEnumerable<JToken> AsJEnumerable(this IEnumerable<JToken> source)
        {
            return source.AsJEnumerable<JToken>();
        }
        public static IJEnumerable<T> AsJEnumerable<T>(this IEnumerable<T> source) where T : JToken
        {
            if (source == null)
            {
                return null;
            }
            else if (source is IJEnumerable<T>)
            {
                return (IJEnumerable<T>)source;
            }
            else
            {
                return new JEnumerable<T>(source);
            }
        }
    }
}

#endif
