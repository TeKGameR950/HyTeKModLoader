﻿using System;

namespace ShellLink.Flags
{    public enum FontFamily : UInt32
    {        FF_DONTCARE = 0x0000,        FF_ROMAN = 0x0010,        FF_SWISS = 0x0020,        FF_MODERN = 0x0030,        FF_SCRIPT = 0x0040,        FF_DECORATIVE = 0x0050
    }
}