﻿using System;

namespace ShellLink.Flags
{    public enum ShowCommand : UInt32
    {        SW_SHOWNORMAL = 0x00000001,        SW_SHOWMAXIMIZED = 0x00000003,        SW_SHOWMINNOACTIVE = 0x00000007
    }
}
