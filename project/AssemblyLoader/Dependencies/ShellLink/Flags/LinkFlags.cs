﻿using System;

namespace ShellLink.Flags
{    [Flags] public enum LinkFlags : UInt32
    {        HasLinkTargetIDList = 0x1,        HasLinkInfo = 0x2,        HasName = 0x4,        HasRelativePath = 0x8,        HasWorkingDir = 0x10,        HasArguments = 0x20,        HasIconLocation = 0x40,        IsUnicode = 0x80,        ForceNoLinkInfo = 0x100,        HasExpString = 0x200,        RunInSeparateProcess = 0x400,        Unused1 = 0x800,        HasDarwinID = 0x1000,        RunAsUser = 0x2000,        HasExpIcon = 0x4000,        NoPidlAlias = 0x8000,        Unused2 = 0x10000,        RunWithShimLayer = 0x20000,        ForceNoLinkTrack = 0x40000,        EnableTargetMetadata = 0x80000,        DisableLinkPathTracking = 0x100000,        DisableKnownFolderTracking = 0x200000,        DisableKnownFolderAlias = 0x400000,        AllowLinkToLink = 0x800000,        UnaliasOnSave = 0x1000000,        PreferEnvironmentPath = 0x2000000,        KeepLocalIDListForUNCTarget = 0x4000000
    }
}
