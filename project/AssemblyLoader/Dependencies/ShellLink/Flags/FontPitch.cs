﻿using System;

namespace ShellLink.Flags
{    public enum FontPitch : UInt32
    {        TMPF_NONE = 0x0000,        TMPF_FIXED_PITCH = 0x0001,        TMPF_VECTOR = 0x0002,        TMPF_TRUETYPE = 0x0004,        TMPF_DEVICE = 0x0008
    }
}
