﻿using System;

namespace ShellLink.Flags
{    [Flags] public enum FileAttributesFlags : UInt32
    {        FILE_ATTRIBUTE_READONLY = 0x1,        FILE_ATTRIBUTE_HIDDEN = 0x2,        FILE_ATTRIBUTE_SYSTEM = 0x4,        Reserved1 = 0x8,        FILE_ATTRIBUTE_DIRECTORY = 0x10,        FILE_ATTRIBUTE_ARCHIVE = 0x20,        Reserved2 = 0x40,        FILE_ATTRIBUTE_NORMAL = 0x80,        FILE_ATTRIBUTE_TEMPORARY = 0x100,        FILE_ATTRIBUTE_SPARSE_FILE = 0x200,        FILE_ATTRIBUTE_REPARSE_POINT = 0x400,        FILE_ATTRIBUTE_COMPRESSED = 0x800,        FILE_ATTRIBUTE_OFFLINE = 0x1000,        FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = 0x2000,        FILE_ATTRIBUTE_ENCRYPTED = 0x4000
    }
}
