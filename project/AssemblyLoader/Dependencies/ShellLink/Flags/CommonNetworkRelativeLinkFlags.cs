﻿using System;

namespace ShellLink.Flags
{    [Flags] public enum CommonNetworkRelativeLinkFlags : UInt32
    {        ValidDevice = 0x1,        ValidNetType = 0x2
    }
}
