﻿using System;

namespace ShellLink.Flags
{    [Flags] public enum LinkInfoFlags : UInt32
    {        VolumeIDAndLocalBasePath = 0x1,        CommonNetworkRelativeLinkAndPathSuffix = 0x2
    }
}
