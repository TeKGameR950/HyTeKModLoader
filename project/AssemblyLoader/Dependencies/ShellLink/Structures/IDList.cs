﻿using System;
using System.Collections.Generic;
using System.Text;
using ShellLink.Internal;
using System.Runtime.InteropServices;
using System.Linq;

namespace ShellLink.Structures
{    public class IDList : Structure
    {        public readonly byte[] TerminalID = new byte[] { 0x00, 0x00 };

        #region Constructor        public IDList() : this(new List<ItemID>()) { }        public IDList(List<ItemID> idList) { ItemIDList = idList; }
        #endregion // Constructor        public List<ItemID> ItemIDList { get; set; }

        #region IDListSize        public UInt16 IDListSize
        {
            get
            {
                UInt16 Size = 2;
                for (int i = 0; i < ItemIDList.Count; i++)
                {
                    Size += ItemIDList[i].ItemIDSize;
                }
                return Size;
            }
        }
        #endregion // IDListSize

        #region Bytes
        private byte[] Bytes
        {
            get
            {
                byte[] idList = new byte[IDListSize];
                int Offset = 0;
                for (int i = 0; i < ItemIDList.Count; i++)
                {
                    ItemID item = ItemIDList[i];
                    Buffer.BlockCopy(item.GetBytes(), 0, idList, Offset, (int)item.ItemIDSize);
                    Offset += item.ItemIDSize;
                }
                Buffer.BlockCopy(TerminalID, 0, idList, Offset, 2);
                return idList;
            }
        }
        #endregion // Bytes

        #region Path        public String Path
        {
            get
            {
                byte[] pszPath = new byte[Win32.MAX_PATH * 2];
                if (!Win32.SHGetPathFromIDListW(Bytes, pszPath))
                {
                    // error
                }
                return Encoding.Unicode.GetString(pszPath).TrimEnd(new char[] { (char)0 });
            }

            set
            {
                ItemIDList.Clear();
                IntPtr pIdList = Win32.ILCreateFromPath(value);

                if(pIdList == IntPtr.Zero)
                {
                    // FIXME SHSimpleIDListFromPath is deprecated
                    pIdList = Win32.SHSimpleIDListFromPath(value);
                }

                if (pIdList != IntPtr.Zero)
                {
                    UInt16 cb;
                    IntPtr ptr = pIdList;
                    while ((cb = (UInt16)Marshal.ReadInt16(ptr)) > 2)
                    {
                        ptr += 2;
                        cb -= 2;
                        byte[] abID = new byte[cb];
                        Marshal.Copy(ptr, abID, 0, cb);
                        ItemID itemId = new ItemID(abID);
                        ItemIDList.Add(itemId);
                        ptr += cb;
                    }

                    Win32.ILFree(pIdList);
                }
            }
        }
        #endregion // Path

        #region DisplayName        public String DisplayName
        {
            get
            {
                if (Win32.SHGetNameFromIDList(Bytes, SIGDN.SIGDN_NORMALDISPLAY, out IntPtr pszName) == 0)
                {
                    try
                    {
                        return Marshal.PtrToStringAuto(pszName);
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                    finally
                    {
                        Win32.CoTaskMemFree(pszName);
                    }
                }
                return "";
            }
        }
        #endregion // DisplayName

        #region GetOffsetByIndex        public UInt32 GetOffsetByIndex(int Index)
        {
            uint Offset = 0;

            if(Index < 0 || Index >= ItemIDList.Count)
            {
                throw new ArgumentOutOfRangeException(String.Format("Invalid index {0} provided", Index));
            }

            for(int i = 0; i < Index; i++)
            {
                Offset += ItemIDList[i].ItemIDSize;
            }

            return Offset;
        }
        #endregion // GetOffsetByIndex

        #region GetBytes        public override byte[] GetBytes()
        {
            return Bytes;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("IDListSize: {0} (0x{0:X})", IDListSize);
            builder.AppendLine();
            builder.AppendFormat("DisplayName: {0}", DisplayName);
            builder.AppendLine();
            builder.AppendFormat("Path: {0}", Path);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString
    }
}
