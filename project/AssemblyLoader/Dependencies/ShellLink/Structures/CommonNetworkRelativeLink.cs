﻿using System;
using System.Text;
using System.Linq;
using ShellLink.Flags;

namespace ShellLink.Structures
{    public class CommonNetworkRelativeLink : Structure
    {
        #region Constructor        public CommonNetworkRelativeLink() : base() { NetName = "";  }
        #endregion // Constructor        public Boolean IsUnicode => NetNameOffset > 0x14;

        #region CommonNetworkRelativeLinkSize        public UInt32 CommonNetworkRelativeLinkSize
        {
            get
            {
                UInt32 Size = 0x14;

                Size += (UInt32)NetName.Length + 1;
                if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
                {
                    Size += (UInt32)DeviceName.Length + 1;
                }

                if (IsUnicode)
                {
                    Size += 8;
                    if (DeviceNameUnicode == null)
                    {
                        DeviceNameUnicode = DeviceName;
                    }
                    if (NetNameUnicode == null)
                    {
                        NetNameUnicode = NetName;
                    }
                    Size += ((UInt32)NetNameUnicode.Length + 1) * 2;
                    if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
                    {
                        Size += ((UInt32)DeviceNameUnicode.Length + 1) * 2;
                    }
                }

                return Size;
            }
        }
        #endregion // CommonNetworkRelativeLinkSize        public CommonNetworkRelativeLinkFlags CommonNetworkRelativeLinkFlags => (NetworkProviderType != 0 ? CommonNetworkRelativeLinkFlags.ValidNetType : 0) | (DeviceName != null ? CommonNetworkRelativeLinkFlags.ValidDevice : 0);        public UInt32 NetNameOffset => (UInt32)(NetNameUnicode != null ? 0x1C : 0x14);        public UInt32 DeviceNameOffset => (UInt32)((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0 ? NetNameOffset + NetName.Length + 1 : 0);        public NetworkProviderType NetworkProviderType { get; set; }

        #region NetNameOffsetUnicode        public UInt32 NetNameOffsetUnicode
        {
            get
            {
                if(IsUnicode)
                {
                    if((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
                    {
                        return (UInt32)(DeviceNameOffset + DeviceName.Length + 1);
                    }

                    return (UInt32)(NetNameOffset + NetName.Length + 1);
                }

                return 0;
            }
        }
        #endregion // NetNameOffsetUnicode        public UInt32 DeviceNameOffsetUnicode => (UInt32)(IsUnicode && (CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0 ? NetNameOffsetUnicode + (NetNameUnicode.Length + 1) * 2 : 0);        public String NetName { get; set; }        public String DeviceName { get; set; }        public String NetNameUnicode { get; set; }        public String DeviceNameUnicode { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] CommonNetworkRelativeLink = new byte[CommonNetworkRelativeLinkSize];
            Buffer.BlockCopy(BitConverter.GetBytes(CommonNetworkRelativeLinkSize), 0, CommonNetworkRelativeLink, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)CommonNetworkRelativeLinkFlags), 0, CommonNetworkRelativeLink, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(NetNameOffset), 0, CommonNetworkRelativeLink, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(DeviceNameOffset), 0, CommonNetworkRelativeLink, 12, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)NetworkProviderType), 0, CommonNetworkRelativeLink, 16, 4);
            if (IsUnicode)
            {
                Buffer.BlockCopy(BitConverter.GetBytes(NetNameOffsetUnicode), 0, CommonNetworkRelativeLink, 20, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(DeviceNameOffsetUnicode), 0, CommonNetworkRelativeLink, 24, 4);
                Buffer.BlockCopy(Encoding.Unicode.GetBytes(NetNameUnicode), 0, CommonNetworkRelativeLink, (int)NetNameOffsetUnicode, NetNameUnicode.Length * 2);
                if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
                {
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(DeviceNameUnicode), 0, CommonNetworkRelativeLink, (int)DeviceNameOffsetUnicode, DeviceNameUnicode.Length * 2);
                }
            }
            Buffer.BlockCopy(Encoding.Default.GetBytes(NetName), 0, CommonNetworkRelativeLink, (int)NetNameOffset, NetName.Length);
            if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
            {
                Buffer.BlockCopy(Encoding.Default.GetBytes(DeviceName), 0, CommonNetworkRelativeLink, (int)DeviceNameOffset, DeviceName.Length);

            }
            return CommonNetworkRelativeLink;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("CommonNetworkRelativeLinkSize: {0} (0x{0:X})", CommonNetworkRelativeLinkSize);
            builder.AppendLine();
            builder.AppendFormat("CommonNetworkRelativeLinkFlags: {0}", CommonNetworkRelativeLinkFlags);
            builder.AppendLine();
            builder.AppendFormat("NetNameOffset: {0} (0x{0:X})", NetNameOffset);
            builder.AppendLine();
            builder.AppendFormat("DeviceNameOffset: {0} (0x{0:X})", DeviceNameOffset);
            builder.AppendLine();
            builder.AppendFormat("NetworkProviderType: {0}", NetworkProviderType);
            builder.AppendLine();
            if (NetNameOffset > 0x14)
            {
                builder.AppendFormat("NetNameOffsetUnicode: {0} (0x{0:X})", NetNameOffsetUnicode);
                builder.AppendLine();
                builder.AppendFormat("DeviceNameOffsetUnicode: {0} (0x{0:X})", DeviceNameOffsetUnicode);
                builder.AppendLine();

            }
            builder.AppendFormat("NetName: {0}", NetName);
            builder.AppendLine();
            if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
            {
                builder.AppendFormat("DeviceName: {0}", DeviceName);
                builder.AppendLine();
            }
            if (NetNameOffset > 0x14)
            {
                builder.AppendFormat("NetNameUnicode: {0}", NetNameUnicode);
                builder.AppendLine();
                if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
                {
                    builder.AppendFormat("DeviceNameUnicode: {0}", DeviceNameUnicode);
                    builder.AppendLine();
                }
            }
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray
        public static CommonNetworkRelativeLink FromByteArray(byte[] ba)
        {
            CommonNetworkRelativeLink CommonNetworkRelativeLink = new CommonNetworkRelativeLink();
            if (ba.Length < 0x14)
            {
                throw new ArgumentException(String.Format("Size of the CommonNetworkRelativeLink Structure is less than 20 ({0})", ba.Length));
            }

            UInt32 CommonNetworkRelativeLinkSize = BitConverter.ToUInt32(ba, 0);
            if (CommonNetworkRelativeLinkSize > ba.Length)
            {
                throw new ArgumentException(String.Format("The CommonNetworkRelativeLinkSize is {0} is incorrect (expected {1})", CommonNetworkRelativeLink, ba.Length));
            }

            CommonNetworkRelativeLinkFlags CommonNetworkRelativeLinkFlags = (CommonNetworkRelativeLinkFlags)BitConverter.ToUInt32(ba, 4);
            UInt32 NetNameOffset = BitConverter.ToUInt32(ba, 8);
            UInt32 DeviceNameOffset = BitConverter.ToUInt32(ba, 12);
            UInt32 NetNameOffsetUnicode = 0;
            UInt32 DeviceNameOffsetUnicode = 0;
            CommonNetworkRelativeLink.NetworkProviderType = (NetworkProviderType)BitConverter.ToUInt32(ba, 16);

            if (NetNameOffset > 0x14)
            {
                NetNameOffsetUnicode = BitConverter.ToUInt32(ba, 20);
                DeviceNameOffsetUnicode = BitConverter.ToUInt32(ba, 24);
            }

            byte[] tmp = ba.Skip((int)NetNameOffset).ToArray();
            CommonNetworkRelativeLink.NetName = Encoding.Default.GetString(tmp.Take(Array.IndexOf(tmp, (byte)0x00) + 1).ToArray()).TrimEnd(new char[] { (char)0 });

            if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
            {
                tmp = ba.Skip((int)DeviceNameOffset).ToArray();
                CommonNetworkRelativeLink.DeviceName = Encoding.Default.GetString(tmp.Take(Array.IndexOf(tmp, (byte)0x00) + 1).ToArray()).TrimEnd(new char[] { (char)0 });
            }

            if (NetNameOffset > 0x14)
            {
                int Index = 0;
                tmp = ba.Skip((int)NetNameOffsetUnicode).ToArray();
                for (int i = 0; i < tmp.Length - 1; i++)
                {
                    if (tmp[i] == 0x00 && tmp[i + 1] == 0x00)
                    {
                        Index = i;
                        break;
                    }
                }
                CommonNetworkRelativeLink.NetNameUnicode = Encoding.Unicode.GetString(tmp.Take(Index + 1).ToArray()).TrimEnd(new char[] { (char)0 });

                if ((CommonNetworkRelativeLinkFlags & CommonNetworkRelativeLinkFlags.ValidDevice) != 0)
                {
                    tmp = ba.Skip((int)DeviceNameOffsetUnicode).ToArray();
                    for (int i = 0; i < tmp.Length - 1; i++)
                    {
                        if (tmp[i] == 0x00 && tmp[i + 1] == 0x00)
                        {
                            Index = i;
                            break;
                        }
                    }
                    CommonNetworkRelativeLink.DeviceNameUnicode = Encoding.Unicode.GetString(tmp.Take(Index + 1).ToArray()).TrimEnd(new char[] { (char)0 });
                }
            }

            return CommonNetworkRelativeLink;
        }
        #endregion // FromByteArray
    }
}