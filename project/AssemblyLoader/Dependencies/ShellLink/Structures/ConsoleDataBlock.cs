﻿using System;
using System.Text;
using ShellLink.Flags;

namespace ShellLink.Structures
{    public class ConsoleDataBlock : ExtraDataBlock
    {
        #region Constructor        public ConsoleDataBlock() : base()
        {
            FaceName = "";
            ColorTable = new byte[64];
        }
        #endregion //Constructor        public override UInt32 BlockSize => 0xCC;        public override BlockSignature BlockSignature => BlockSignature.CONSOLE_PROPS;        public FillAttributes FillAttributes { get; set; }        public FillAttributes PopupFillAttributes { get; set; }        public UInt16 ScreenBufferSizeX { get; set; }        public UInt16 ScreenBufferSizeY { get; set; }        public UInt16 WindowSizeX { get; set; }        public UInt16 WindowSizeY { get; set; }        public UInt16 WindowOriginX { get; set; }        public UInt16 WindowOriginY { get; set; }        public UInt32 Unused1 { get; set; }        public UInt32 Unused2 { get; set; }        public UInt32 FontSize { get; set; }        public FontFamily FontFamily { get; set; }        public FontPitch FontPitch { get; set; }        public UInt32 FontWeight { get; set; }        public String FaceName { get; set; }        public UInt32 CursorSize { get; set; }        public Boolean FullScreen { get; set; }        public Boolean QuickEdit { get; set; }        public Boolean InsertMode { get; set; }        public Boolean AutoPosition { get; set; }        public UInt32 HistoryBufferSize { get; set; }        public UInt32 NumberOfHistoryBuffers { get; set; }        public Boolean HistoryNoDup { get; set; }        public byte[] ColorTable { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] ConsoleDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, ConsoleDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, ConsoleDataBlock, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt16)FillAttributes), 0, ConsoleDataBlock, 8, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt16)PopupFillAttributes), 0, ConsoleDataBlock, 10, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(ScreenBufferSizeX), 0, ConsoleDataBlock, 12, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(ScreenBufferSizeY), 0, ConsoleDataBlock, 14, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(WindowSizeX), 0, ConsoleDataBlock, 16, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(WindowSizeY), 0, ConsoleDataBlock, 18, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(WindowOriginX), 0, ConsoleDataBlock, 20, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(WindowOriginY), 0, ConsoleDataBlock, 22, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(Unused1), 0, ConsoleDataBlock, 24, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(Unused2), 0, ConsoleDataBlock, 28, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(FontSize), 0, ConsoleDataBlock, 32, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)FontFamily | (UInt32)FontPitch), 0, ConsoleDataBlock, 36, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(FontWeight), 0, ConsoleDataBlock, 40, 4);
            Buffer.BlockCopy(Encoding.Unicode.GetBytes(FaceName), 0, ConsoleDataBlock, 44, FaceName.Length < 32 ? FaceName.Length * 2 : 62);
            Buffer.BlockCopy(BitConverter.GetBytes(CursorSize), 0, ConsoleDataBlock, 108, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)(FullScreen ? 1 : 0)), 0, ConsoleDataBlock, 112, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)(QuickEdit ? 1 : 0)), 0, ConsoleDataBlock, 116, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)(InsertMode ? 1 : 0)), 0, ConsoleDataBlock, 120, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)(AutoPosition ? 1 : 0)), 0, ConsoleDataBlock, 124, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(HistoryBufferSize), 0, ConsoleDataBlock, 128, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(NumberOfHistoryBuffers), 0, ConsoleDataBlock, 132, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)(HistoryNoDup ? 1 : 0)), 0, ConsoleDataBlock, 136, 4);
            Buffer.BlockCopy(ColorTable, 0, ConsoleDataBlock, 140, ColorTable.Length < 64 ? ColorTable.Length : 64);

            return ConsoleDataBlock;
        }
        #endregion /// GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("FillAttributes: {0}", FillAttributes);
            builder.AppendLine();
            builder.AppendFormat("PopupFillAttributes: {0}", PopupFillAttributes);
            builder.AppendLine();
            builder.AppendFormat("ScreenBufferSizeX: {0}", ScreenBufferSizeX);
            builder.AppendLine();
            builder.AppendFormat("ScreenBufferSizeY: {0}", ScreenBufferSizeY);
            builder.AppendLine();
            builder.AppendFormat("WindowSizeX: {0}", WindowSizeX);
            builder.AppendLine();
            builder.AppendFormat("WindowSizeY: {0}", WindowSizeY);
            builder.AppendLine();
            builder.AppendFormat("WindowOriginX: {0}", WindowOriginX);
            builder.AppendLine();
            builder.AppendFormat("WindowOriginY: {0}", WindowOriginY);
            builder.AppendLine();
            builder.AppendFormat("FontSize: {0}", FontSize);
            builder.AppendLine();
            builder.AppendFormat("FontFamily: {0}", FontFamily);
            builder.AppendLine();
            builder.AppendFormat("FontPitch: {0}", FontPitch);
            builder.AppendLine();
            builder.AppendFormat("FontWeight: {0}", FontWeight);
            builder.AppendLine();
            builder.AppendFormat("FaceName: {0}", FaceName);
            builder.AppendLine();
            builder.AppendFormat("CursorSize: {0}", CursorSize);
            builder.AppendLine();
            builder.AppendFormat("FullScreen: {0}", FullScreen);
            builder.AppendLine();
            builder.AppendFormat("QuickEdit: {0}", QuickEdit);
            builder.AppendLine();
            builder.AppendFormat("InsertMode: {0}", InsertMode);
            builder.AppendLine();
            builder.AppendFormat("AutoPosition: {0}", AutoPosition);
            builder.AppendLine();
            builder.AppendFormat("HistoryBufferSize: {0} (0x{0:X})", HistoryBufferSize);
            builder.AppendLine();
            builder.AppendFormat("NumberOfHistoryBuffers: {0}", NumberOfHistoryBuffers);
            builder.AppendLine();
            builder.AppendFormat("HistoryNoDup: {0}", HistoryNoDup);
            builder.AppendLine();
            builder.AppendFormat("ColorTable: {0}", BitConverter.ToString(ColorTable).Replace("-", " "));
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static ConsoleDataBlock FromByteArray(byte[] ba)
        {
            ConsoleDataBlock ConsoleDataBlock = new ConsoleDataBlock();

            if (ba.Length < 0xCC)
            {
                throw new ArgumentException(String.Format("Size of the ConsoleDataBlock Structure is less than 204 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, ConsoleDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != ConsoleDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, ConsoleDataBlock.BlockSignature));
            }

            ConsoleDataBlock.FillAttributes = (FillAttributes)BitConverter.ToUInt16(ba, 8);
            ConsoleDataBlock.PopupFillAttributes = (FillAttributes)BitConverter.ToUInt16(ba, 10);
            ConsoleDataBlock.ScreenBufferSizeX = BitConverter.ToUInt16(ba, 12);
            ConsoleDataBlock.ScreenBufferSizeY = BitConverter.ToUInt16(ba, 14);
            ConsoleDataBlock.WindowSizeX = BitConverter.ToUInt16(ba, 16);
            ConsoleDataBlock.WindowSizeY = BitConverter.ToUInt16(ba, 18);
            ConsoleDataBlock.WindowOriginX = BitConverter.ToUInt16(ba, 20);
            ConsoleDataBlock.WindowOriginY = BitConverter.ToUInt16(ba, 22);
            ConsoleDataBlock.Unused1 = BitConverter.ToUInt32(ba, 24);
            ConsoleDataBlock.Unused2 = BitConverter.ToUInt32(ba, 28);
            ConsoleDataBlock.FontSize = BitConverter.ToUInt32(ba, 32);
            ConsoleDataBlock.FontFamily = (FontFamily)(BitConverter.ToUInt32(ba, 36) & 0xF0);
            ConsoleDataBlock.FontPitch = (FontPitch)(BitConverter.ToUInt32(ba, 36) &0xF);
            ConsoleDataBlock.FontWeight = BitConverter.ToUInt32(ba, 40);
            byte[] FaceName = new byte[64];
            Buffer.BlockCopy(ba, 44, FaceName, 0, 64);
            ConsoleDataBlock.FaceName = Encoding.Unicode.GetString(FaceName).TrimEnd(new char[] { (char)0 });
            ConsoleDataBlock.CursorSize = BitConverter.ToUInt32(ba, 108);
            ConsoleDataBlock.FullScreen = BitConverter.ToUInt32(ba, 112) != 0;
            ConsoleDataBlock.QuickEdit = BitConverter.ToUInt32(ba, 116) != 0;
            ConsoleDataBlock.InsertMode = BitConverter.ToUInt32(ba, 120) != 0;
            ConsoleDataBlock.AutoPosition = BitConverter.ToUInt32(ba, 124) != 0;
            ConsoleDataBlock.HistoryBufferSize = BitConverter.ToUInt32(ba, 128);
            ConsoleDataBlock.NumberOfHistoryBuffers = BitConverter.ToUInt32(ba, 132);
            ConsoleDataBlock.HistoryNoDup = BitConverter.ToUInt32(ba, 136) != 0;
            Buffer.BlockCopy(ba, 140, ConsoleDataBlock.ColorTable, 0, 64);

            return ConsoleDataBlock;
        }
        #endregion // FromByteArray
    }
}
