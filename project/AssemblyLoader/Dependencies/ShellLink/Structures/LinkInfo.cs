﻿using System;
using System.Text;
using System.Linq;
using ShellLink.Flags;

namespace ShellLink.Structures
{    public class LinkInfo : Structure
    {
        #region Constructor        public LinkInfo() : base()
        {
            LocalBasePath = "";
            CommonPathSuffix = "";
        }
        #endregion // Constructor

        #region LinkInfoSize        public UInt32 LinkInfoSize
        {
            get
            {
                UInt32 Size = LinkInfoHeaderSize;

                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
                {
                    Size += VolumeID.VolumeIDSize;
                    Size += (UInt32)LocalBasePath.Length + 1;
                    if (LocalBasePathUnicode != null)
                    {
                        Size += (UInt32)LocalBasePathUnicode.Length * 2 + 2;
                    }
                }

                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
                {
                    Size += CommonNetworkRelativeLink.CommonNetworkRelativeLinkSize;
                    Size += (UInt32)CommonPathSuffix.Length + 1;
                    if (CommonPathSuffixUnicode != null)
                    {
                        Size += (UInt32)CommonPathSuffixUnicode.Length * 2 + 2;
                    }
                }

                return Size;
            }
        }
        #endregion // LinkInfoSize

        #region LinkInfoHeaderSize        public UInt32 LinkInfoHeaderSize
        {
            get
            {
                UInt32 Size = 0x1C;
                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0 && LocalBasePathUnicode != null)
                {
                    Size += 4;
                }
                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0 && CommonPathSuffixUnicode != null)
                {
                    Size += 4;
                }
                return Size;
            }
        }
        #endregion // LinkInfoHeaderSize        public LinkInfoFlags LinkInfoFlags => (VolumeID != null ? LinkInfoFlags.VolumeIDAndLocalBasePath : 0) | (CommonNetworkRelativeLink != null ? LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix : 0);        public UInt32 VolumeIDOffset => (VolumeID != null ? LinkInfoHeaderSize : 0);        public UInt32 LocalBasePathOffset => (VolumeID != null ? LinkInfoHeaderSize + VolumeID.VolumeIDSize : 0);

        #region CommonNetworkRelativeLinkOffset        public UInt32 CommonNetworkRelativeLinkOffset
        {
            get
            {
                if (CommonNetworkRelativeLink != null)
                {
                    if (VolumeID != null)
                    {
                        return LocalBasePathOffset + (UInt32)LocalBasePath.Length + 1;
                    }
                    return LinkInfoHeaderSize;
                }
                return 0;
            }
        }
        #endregion // CommonNetworkRelativeLinkOffset        public UInt32 CommonPathSuffixOffset => (CommonNetworkRelativeLink != null ? CommonNetworkRelativeLinkOffset + CommonNetworkRelativeLink.CommonNetworkRelativeLinkSize : 0);

        #region LocalBasePathOffsetUnicode        public UInt32 LocalBasePathOffsetUnicode
        {
            get
            {
                if (LocalBasePathUnicode != null)
                {
                    if (VolumeID != null)
                    {
                        if(CommonNetworkRelativeLink != null)
                        {
                            return CommonPathSuffixOffset + (UInt32)CommonPathSuffix.Length + 2;
                        }

                        return LocalBasePathOffset + (UInt32)LocalBasePath.Length + 1;
                    }
                }

                return 0;
            }
        }
        #endregion // LocalBasePathOffsetUnicode

        #region CommonPathSuffixOffsetUnicode        public UInt32 CommonPathSuffixOffsetUnicode
        {
            get
            {
                if(CommonPathSuffixUnicode != null && CommonNetworkRelativeLink != null)
                {
                    if (VolumeID != null)
                    {
                        if(LocalBasePathUnicode != null)
                        {
                            return LocalBasePathOffsetUnicode + (UInt32)LocalBasePathUnicode.Length + 2;
                        }
                        return CommonPathSuffixOffset + (UInt32)CommonPathSuffix.Length + 1;
                    }
                    return CommonPathSuffixOffset + (UInt32)CommonPathSuffix.Length + 1;
                }
                return 0;
            }
        }
        #endregion // CommonPathSuffixOffsetUnicode        public VolumeID VolumeID { get; set; }        public String LocalBasePath { get; set; }        public CommonNetworkRelativeLink CommonNetworkRelativeLink { get; set; }        public string CommonPathSuffix { get; set; }        public string LocalBasePathUnicode { get; set; }        public string CommonPathSuffixUnicode { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] LinkInfo = new byte[LinkInfoSize];
            Buffer.BlockCopy(BitConverter.GetBytes(LinkInfoSize), 0, LinkInfo, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(LinkInfoHeaderSize), 0, LinkInfo, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)LinkInfoFlags), 0, LinkInfo, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(VolumeIDOffset), 0, LinkInfo, 12, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(LocalBasePathOffset), 0, LinkInfo, 16, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(CommonNetworkRelativeLinkOffset), 0, LinkInfo, 20, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(CommonPathSuffixOffset), 0, LinkInfo, 24, 4);

            if (LinkInfoHeaderSize > 0x1C)
            {
                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
                {
                    Buffer.BlockCopy(BitConverter.GetBytes(LocalBasePathOffsetUnicode), 0, LinkInfo, 28, 4);
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(LocalBasePathUnicode), 0, LinkInfo, (int)LocalBasePathOffsetUnicode, LocalBasePathUnicode.Length * 2);
                }

                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
                {
                    Buffer.BlockCopy(BitConverter.GetBytes(CommonPathSuffixOffsetUnicode), 0, LinkInfo, 32, 4);
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(CommonPathSuffixUnicode), 0, LinkInfo, (int)CommonPathSuffixOffsetUnicode, CommonPathSuffixUnicode.Length * 2);
                }
            }

            if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
            {
                Buffer.BlockCopy(VolumeID.GetBytes(), 0, LinkInfo, (int)VolumeIDOffset, (int)VolumeID.VolumeIDSize);
                Buffer.BlockCopy(Encoding.Default.GetBytes(LocalBasePath), 0, LinkInfo, (int)LocalBasePathOffset, LocalBasePath.Length);
            }

            if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
            {
                Buffer.BlockCopy(CommonNetworkRelativeLink.GetBytes(), 0, LinkInfo, (int)CommonNetworkRelativeLinkOffset, (int)CommonNetworkRelativeLink.CommonNetworkRelativeLinkSize);
                Buffer.BlockCopy(Encoding.Default.GetBytes(CommonPathSuffix), 0, LinkInfo, (int)CommonPathSuffixOffset, CommonPathSuffix.Length);
            }
            return LinkInfo;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("LinkInfoSize: {0} (0x{0:X})", LinkInfoSize);
            builder.AppendLine();
            builder.AppendFormat("LinkInfoHeaderSize: {0} (0x{0:X})", LinkInfoHeaderSize);
            builder.AppendLine();
            builder.AppendFormat("LinkInfoFlags: {0}", LinkInfoFlags);
            builder.AppendLine();
            builder.AppendFormat("VolumeIDOffset: {0} (0x{0:X})", VolumeIDOffset);
            builder.AppendLine();
            builder.AppendFormat("LocalBasePathOffset: {0} (0x{0:X})", LocalBasePathOffset);
            builder.AppendLine();
            builder.AppendFormat("CommonNetworkRelativeLinkOffset: {0} (0x{0:X})", CommonNetworkRelativeLinkOffset);
            builder.AppendLine();
            builder.AppendFormat("CommonPathSuffixOffset: {0} (0x{0:X})", CommonPathSuffixOffset);
            builder.AppendLine();
            if (LinkInfoHeaderSize > 0x1C)
            {
                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
                {
                    builder.AppendFormat("LocalBasePathOffsetUnicode: {0} (0x{0:X})", LocalBasePathOffsetUnicode);
                    builder.AppendLine();
                }
                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
                {
                    builder.AppendFormat("CommonPathSuffixOffsetUnicode: {0} (0x{0:X})", CommonPathSuffixOffsetUnicode);
                    builder.AppendLine();
                }
            }
            if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
            {
                builder.Append(VolumeID.ToString());
                builder.AppendFormat("LocalBasePath: {0}", LocalBasePath);
                builder.AppendLine();
            }
            if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
            {
                builder.Append(CommonNetworkRelativeLink.ToString());
            }
            builder.AppendFormat("CommonPathSuffix: {0}", CommonPathSuffix);
            builder.AppendLine();
            if (LinkInfoHeaderSize > 0x1C)
            {
                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
                {
                    builder.AppendFormat("LocalBasePathUnicode: {0}", LocalBasePathUnicode);
                    builder.AppendLine();
                }
                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
                {
                    builder.AppendFormat("CommonPathSuffixUnicode: {0}", CommonPathSuffixUnicode);
                    builder.AppendLine();
                }
            }
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static LinkInfo FromByteArray(byte[] ba)
        {
            LinkInfo LinkInfo = new LinkInfo();
            if (ba.Length < 0x1C)
            {
                throw new ArgumentException(String.Format("Size of the LinkInfo Structure is less than 28 ({0})", ba.Length));
            }

            UInt32 LinkInfoSize = BitConverter.ToUInt32(ba, 0);
            if (LinkInfoSize > ba.Length)
            {
                throw new ArgumentException(String.Format("The LinkInfoSize is {0} is incorrect (expected {1})", LinkInfoSize, ba.Length));
            }

            UInt32 LinkInfoHeaderSize = BitConverter.ToUInt32(ba, 4);
            if (LinkInfoHeaderSize < 0x1C)
            {
                throw new ArgumentException(String.Format("The LinkInfoHeaderSize is {0} is incorrect)", LinkInfoHeaderSize));
            }

            LinkInfoFlags LinkInfoFlags = (LinkInfoFlags)BitConverter.ToUInt32(ba, 8);

            // TODO: check offsets
            UInt32 VolumeIDOffset = BitConverter.ToUInt32(ba, 12);
            UInt32 LocalBasePathOffset = BitConverter.ToUInt32(ba, 16);
            UInt32 CommonNetworkRelativeLinkOffset = BitConverter.ToUInt32(ba, 20);
            UInt32 CommonPathSuffixOffset = BitConverter.ToUInt32(ba, 24);
            UInt32 LocalBasePathOffsetUnicode = 0;
            UInt32 CommonPathSuffixOffsetUnicode = 0;

            if (LinkInfoHeaderSize > 0x1C)
            {
                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
                {
                    LocalBasePathOffsetUnicode = BitConverter.ToUInt32(ba, 28);
                }
                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
                {
                    CommonPathSuffixOffsetUnicode = BitConverter.ToUInt32(ba, 32);
                }
            }

            byte[] tmp;
            if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
            {
                tmp = ba.Skip((int)VolumeIDOffset).ToArray();
                LinkInfo.VolumeID = VolumeID.FromByteArray(tmp);
                tmp = ba.Skip((int)LocalBasePathOffset).ToArray();
                LinkInfo.LocalBasePath = Encoding.Default.GetString(tmp.Take(Array.IndexOf(tmp, (byte)0x00) + 1).ToArray()).TrimEnd(new char[] { (char)0 });
            }

            if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
            {
                tmp = ba.Skip((int)CommonNetworkRelativeLinkOffset).ToArray();
                LinkInfo.CommonNetworkRelativeLink = CommonNetworkRelativeLink.FromByteArray(tmp);
                tmp = ba.Skip((int)CommonPathSuffixOffset).ToArray();
                LinkInfo.CommonPathSuffix = Encoding.Default.GetString(tmp.Take(Array.IndexOf(tmp, (byte)0x00) + 1).ToArray()).TrimEnd(new char[] { (char)0 });
            }

            if (LinkInfoHeaderSize >= 0x24)
            {
                if ((LinkInfoFlags & LinkInfoFlags.VolumeIDAndLocalBasePath) != 0)
                {
                    int Index = 0;
                    tmp = ba.Skip((int)LocalBasePathOffsetUnicode).ToArray();
                    for (int i = 0; i < tmp.Length - 1; i++)
                    {
                        if (tmp[i] == 0x00 && tmp[i + 1] == 0x00)
                        {
                            Index = i;
                            break;
                        }
                    }

                    LinkInfo.LocalBasePathUnicode = Encoding.Unicode.GetString(tmp.Take(Index + 1).ToArray()).TrimEnd(new char[] { (char)0 });
                }

                if ((LinkInfoFlags & LinkInfoFlags.CommonNetworkRelativeLinkAndPathSuffix) != 0)
                {
                    int Index = 0;
                    tmp = ba.Skip((int)CommonPathSuffixOffsetUnicode).ToArray();
                    for (int i = 0; i < tmp.Length - 1; i++)
                    {
                        if (tmp[i] == 0x00 && tmp[i + 1] == 0x00)
                        {
                            Index = i;
                            break;
                        }
                    }
                    LinkInfo.CommonPathSuffixUnicode = Encoding.Unicode.GetString(tmp.Take(Index + 1).ToArray()).TrimEnd(new char[] { (char)0 });
                }
            }

            return LinkInfo;
        }
        #endregion // FromByteArray
    }
}
