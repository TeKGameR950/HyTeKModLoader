﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public class ShimDataBlock : ExtraDataBlock
    {
        #region Constructor        public ShimDataBlock() : base()
        {
            LayerName = "";
        }        public ShimDataBlock(String LayerName) : base()
        {
            this.LayerName = LayerName;
        }
        #endregion // Constructor        public override UInt32 BlockSize => (UInt32)(LayerName.Length < 0x40 ? 0x88 : (LayerName.Length + 1) * 2 + 8);        public override BlockSignature BlockSignature => BlockSignature.SHIM_PROPS;        public String LayerName { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] ShimDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, ShimDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, ShimDataBlock, 4, 4);
            Buffer.BlockCopy(Encoding.Unicode.GetBytes(LayerName), 0, ShimDataBlock, 8, LayerName.Length * 2);
            return ShimDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("LayerName: {0}", LayerName);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static ShimDataBlock FromByteArray(byte[] ba)
        {
            ShimDataBlock ShimDataBlock = new ShimDataBlock();
            if (ba.Length < 0x88)
            {
                throw new ArgumentException(String.Format("Size of the ShimDataBlock Structure is less than 136 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, ShimDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != ShimDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, ShimDataBlock.BlockSignature));
            }

            byte[] LayerName = new byte[BlockSize - 8];
            Buffer.BlockCopy(ba, 8, LayerName, 0, (int)BlockSize - 8);
            ShimDataBlock.LayerName = Encoding.Unicode.GetString(LayerName).TrimEnd(new char[] { (char)0 });

            return ShimDataBlock;
        }
        #endregion // FromByteArray
    }
}
