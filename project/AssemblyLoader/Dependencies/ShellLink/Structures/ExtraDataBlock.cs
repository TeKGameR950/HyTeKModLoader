﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public abstract class ExtraDataBlock : Structure
    {        public abstract UInt32 BlockSize { get; }        public abstract BlockSignature BlockSignature { get; }

        #region ToString        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("BlockSize: {0} (0x{0:X})", BlockSize);
            builder.AppendLine();
            builder.AppendFormat("BlockSignature: 0x{0:X}", BlockSignature);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString
    }

    #region BlockSignature    public enum BlockSignature : UInt32
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        ENVIRONMENT_PROPS = 0xA0000001,
        CONSOLE_PROPS = 0xA0000002,
        TRACKER_PROPS = 0xA0000003,
        CONSOLE_FE_PROPS = 0xA0000004,
        SPECIAL_FOLDER_PROPS = 0xA0000005,
        DARWIN_PROPS = 0xA0000006,
        ICON_ENVIRONMENT_PROPS = 0xA0000007,
        SHIM_PROPS = 0xA0000008,
        PROPERTY_STORE_PROPS = 0xA0000009,
        KNOWN_FOLDER_PROPS = 0xA000000B,
        VISTA_AND_ABOVE_IDLIST_PROPS = 0xA000000C
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
    #endregion // BlockSignature
}
