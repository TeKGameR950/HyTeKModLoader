﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShellLink.Structures
{    public class LinkTargetIDList : IDList
    {
        #region Constructor        public LinkTargetIDList() : base() { }        public LinkTargetIDList(List<ItemID> idList) : base(idList) { }
        #endregion // Constructor

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] IdList = new byte[IDListSize + 2];
            Buffer.BlockCopy(BitConverter.GetBytes(IDListSize), 0, IdList, 0, 2);
            Buffer.BlockCopy(base.GetBytes(), 0, IdList, 2, IDListSize - 2);
            return IdList;
        }
        #endregion // GetBytes

        #region FromByteArray        public static LinkTargetIDList FromByteArray(byte[] ba)
        {
            LinkTargetIDList IdList = new LinkTargetIDList();

            if (ba.Length < 4)
            {
                throw new ArgumentException(String.Format("Size of the LinkTargetIDList is less than 4 ({0})", ba.Length));
            }

            UInt16 IDListSize = BitConverter.ToUInt16(ba, 0);
            if (ba.Length < IDListSize)
            {
                throw new ArgumentException(String.Format("Size of the LinkTargetIDList is less than {0} ({1})", IDListSize, ba.Length));
            }

            IDListSize -= 2;
            ba = ba.Skip(2).ToArray();
            while (IDListSize > 2)
            {
                ItemID itemId = ItemID.FromByteArray(ba);
                UInt16 ItemIDSize = BitConverter.ToUInt16(ba, 0);
                IdList.ItemIDList.Add(itemId);
                IDListSize -= ItemIDSize;
                ba = ba.Skip(ItemIDSize).ToArray();
            }

            return IdList;
        }
        #endregion // FromByteArray
    }
}
