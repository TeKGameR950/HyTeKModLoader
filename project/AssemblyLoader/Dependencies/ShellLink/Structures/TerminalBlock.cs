﻿using System;
using System.Text;
using ShellLink.Internal;

namespace ShellLink.Structures
{    public class TerminalBlock : Structure
    {
        #region Constructor        public TerminalBlock() : base() { }
        #endregion // Constructor

        #region GetBytes        public override byte[] GetBytes() => new byte[] { 0x00, 0x00, 0x00, 0x00 };
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("TerminalBlock: {0}", BitConverter.ToString(GetBytes()).Replace("-", " "));
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static TerminalBlock FromByteArray(byte[] ba)
        {
            TerminalBlock terminalBlock = new TerminalBlock();
            if (ba.Length < 4)
            {
                throw new ArgumentException(String.Format("Size of the TerminalBlock Structure is less than 4 ({0})", ba.Length));
            }
            return terminalBlock;
        }
        #endregion // FromByteArray
    }
}
