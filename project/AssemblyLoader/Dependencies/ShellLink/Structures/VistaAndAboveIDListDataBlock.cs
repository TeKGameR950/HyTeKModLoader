﻿using System;
using System.Linq;
using System.Text;

namespace ShellLink.Structures
{    public class VistaAndAboveIDListDataBlock : ExtraDataBlock
    {
        #region Constructor        public VistaAndAboveIDListDataBlock() : base()
        {
            IDList = new IDList();
        }        public VistaAndAboveIDListDataBlock(IDList IDList) : base()
        {
            this.IDList = IDList;
        }
        #endregion // Constructor        public override UInt32 BlockSize => 8 + (UInt32)IDList.IDListSize;        public override BlockSignature BlockSignature => BlockSignature.VISTA_AND_ABOVE_IDLIST_PROPS;        public IDList IDList { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] VistaAndAboveIDListDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, VistaAndAboveIDListDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, VistaAndAboveIDListDataBlock, 4, 4);
            Buffer.BlockCopy(IDList.GetBytes(), 0, VistaAndAboveIDListDataBlock, 8, (int)BlockSize - 8);
            return VistaAndAboveIDListDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.Append(IDList.ToString());
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static VistaAndAboveIDListDataBlock FromByteArray(byte[] ba)
        {
            VistaAndAboveIDListDataBlock VistaAndAboveIDListDataBlock = new VistaAndAboveIDListDataBlock();
            if (ba.Length < 0xA)
            {
                throw new ArgumentException(String.Format("Size of the VistaAndAboveIDListDataBlock Structure is less than 10 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, VistaAndAboveIDListDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != VistaAndAboveIDListDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, VistaAndAboveIDListDataBlock.BlockSignature));
            }

            ba = ba.Skip(8).ToArray();
            UInt32 Count = BlockSize - 8;
            while (Count > 0)
            {
                UInt16 ItemIDSize = BitConverter.ToUInt16(ba, 0);
                if (ItemIDSize != 0)
                {
                    byte[] itemID = new byte[ItemIDSize - 2];
                    Buffer.BlockCopy(ba, 2, itemID, 0, itemID.Length);
                    Count -= ItemIDSize;
                    VistaAndAboveIDListDataBlock.IDList.ItemIDList.Add(new ItemID(itemID));
                    ba = ba.Skip(ItemIDSize).ToArray();
                }
                else
                {
                    break;
                }
            }

            return VistaAndAboveIDListDataBlock;
        }
        #endregion // FromByteArray
    }
}
