﻿using System;
using System.Text;
using System.Linq;
using ShellLink.Flags;

namespace ShellLink.Structures
{    public class VolumeID : Structure
    {
        #region Constructor        public VolumeID() : this(false) { }        public VolumeID(Boolean IsUnicode) : base()
        {
            this.IsUnicode = IsUnicode;
            VolumeLabel = "";
        }
        #endregion // Constructor        public Boolean IsUnicode { get; set; }        public UInt32 VolumeIDSize => (UInt32)(IsUnicode ? 0x14 + (VolumeLabel.Length + 1) * 2 : 0x10 + VolumeLabel.Length + 1);        public DriveType DriveType { get; set; }        public UInt32 DriveSerialNumber { get; set; }        public UInt32 VolumeLabelOffset => (UInt32)(IsUnicode ? 0x14 : 0x10);        public UInt32 VolumeLabelOffsetUnicode => (UInt32)(IsUnicode ? 0x14 : 0);        public string VolumeLabel { get; set; }        public byte[] Data => (IsUnicode ? Encoding.Unicode.GetBytes(VolumeLabel) : Encoding.Default.GetBytes(VolumeLabel));

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] VolumeId = new byte[VolumeIDSize];
            Buffer.BlockCopy(BitConverter.GetBytes(VolumeIDSize), 0, VolumeId, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)DriveType), 0, VolumeId, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(DriveSerialNumber), 0, VolumeId, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(VolumeLabelOffset), 0, VolumeId, 12, 4);
            if(VolumeLabelOffset == 0x14)
            {
                Buffer.BlockCopy(BitConverter.GetBytes(VolumeLabelOffsetUnicode), 0, VolumeId, 16, 4);
                Buffer.BlockCopy(Encoding.Unicode.GetBytes(VolumeLabel), 0, VolumeId, 0x14, VolumeLabel.Length * 2);
            }
            else
            {
                Buffer.BlockCopy(Encoding.Default.GetBytes(VolumeLabel), 0, VolumeId, 0x10, VolumeLabel.Length);
            }
            return VolumeId;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("VolumeIDSize: {0} (0x{0:X})", VolumeIDSize);
            builder.AppendLine();
            builder.AppendFormat("DriveType: {0}", DriveType);
            builder.AppendLine();
            builder.AppendFormat("DriveSerialNumber: 0x{0:X}", DriveSerialNumber);
            builder.AppendLine();
            builder.AppendFormat("VolumeLabelOffset: {0} (0x{0:X})", VolumeLabelOffset);
            builder.AppendLine();
            if (VolumeLabelOffset == 0x14)
            {
                builder.AppendFormat("VolumeLabelOffsetUnicode: {0} (0x{0:X})", VolumeLabelOffsetUnicode);
                builder.AppendLine();
            }
            builder.AppendFormat("VolumeLabel: {0}", VolumeLabel);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static VolumeID FromByteArray(byte[] ba)
        {
            VolumeID VolumeId = new VolumeID(false);
            if (ba.Length <= 0x10)
            {
                throw new ArgumentException(String.Format("Size of the VolumeID Structure is less than 17 ({0})", ba.Length));
            }

            UInt32 VolumeIDSize = BitConverter.ToUInt32(ba, 0);
            if (VolumeIDSize > ba.Length)
            {
                throw new ArgumentException(String.Format("The VolumeIDSize is {0} is incorrect (expected {1})", VolumeIDSize, ba.Length));
            }

            VolumeId.DriveType = (DriveType)BitConverter.ToUInt32(ba, 4);
            VolumeId.DriveSerialNumber = BitConverter.ToUInt32(ba, 8);
            UInt32 VolumeLabelOffset = BitConverter.ToUInt32(ba, 12);

            if (VolumeLabelOffset == 0x14)
            {
                UInt32 VolumeLabelOffsetUnicode = BitConverter.ToUInt32(ba, 16);
                byte[] Data = new byte[VolumeIDSize - 0x14];
                Buffer.BlockCopy(ba, 0x14, Data, 0, Data.Length);
                VolumeId.VolumeLabel = Encoding.Unicode.GetString(Data).TrimEnd(new char[] { (char)0 });
                VolumeId.IsUnicode = true;
            }
            else
            {
                byte[] Data = new byte[VolumeIDSize - 0x10];
                Buffer.BlockCopy(ba, 0x10, Data, 0, Data.Length);
                VolumeId.VolumeLabel = Encoding.Default.GetString(Data).TrimEnd(new char[] { (char)0 });
            }
            return VolumeId;
        }
        #endregion // FromByteArray
    }
}
