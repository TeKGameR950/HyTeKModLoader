﻿using System;
using System.Text;
using ShellLink.Internal;
using System.Runtime.InteropServices;

namespace ShellLink.Structures
{    public class ItemID : Structure
    {
        #region Constructor        public ItemID() : this(new byte[0]) { }        public ItemID(byte[] itemID) { Data = itemID; }
        #endregion // Constructor

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] ItemID = new byte[ItemIDSize];
            Buffer.BlockCopy(BitConverter.GetBytes(ItemIDSize), 0, ItemID, 0, 2);
            Buffer.BlockCopy(Data, 0, ItemID, 2, Data.Length);
            return ItemID;
        }
        #endregion // GetBytes        public UInt16 ItemIDSize => (UInt16)(Data.Length + 2);        public byte[] Data { get; set; }

        #region DisplayName        public String DisplayName
        {
            get
            {
                IntPtr pszName;
                if (Win32.SHGetNameFromIDList(GetBytes(), SIGDN.SIGDN_PARENTRELATIVE, out pszName) == 0)
                {
                    try
                    {
                        return Marshal.PtrToStringAuto(pszName);
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                    finally
                    {
                        Win32.CoTaskMemFree(pszName);
                    }
                }
                return "";
            }
        }
        #endregion // DisplayName

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("ItemIDSize: {0}", ItemIDSize);
            builder.AppendLine();
            builder.AppendFormat("DisplayName: {0}", DisplayName);
            builder.AppendLine();
            builder.AppendFormat("Data: {0}", BitConverter.ToString(Data).Replace("-", " "));
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static ItemID FromByteArray(byte[] ba)
        {
            ItemID ItemId = new ItemID();

            if (ba.Length < 2)
            {
                throw new ArgumentException(String.Format("Size of the ItemID is less than 2 ({0})", ba.Length));
            }

            UInt16 ItemIDSize = BitConverter.ToUInt16(ba, 0);
            if (ba.Length < ItemIDSize)
            {
                throw new ArgumentException(String.Format("Size of the ItemID is not equal to {0} ({1})", ItemIDSize, ba.Length));
            }

            ItemId.Data = new byte[ItemIDSize - 2];
            Buffer.BlockCopy(ba, 2, ItemId.Data, 0, ItemId.Data.Length);

            return ItemId;
        }
        #endregion // FromByteArray
    }
}
