﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public abstract class Structure
    {        public abstract byte[] GetBytes();        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}:", this.GetType().Name);
            builder.AppendLine();
            return builder.ToString();
        }
    }
}
