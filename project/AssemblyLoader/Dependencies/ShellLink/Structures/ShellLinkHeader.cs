﻿using System;
using System.Collections;
using ShellLink.Flags;
using System.Text;

namespace ShellLink.Structures
{    public class ShellLinkHeader : Structure
    {        public readonly UInt32 HeaderSize = 0x4C;        public readonly Guid LinkCLSID = new Guid("00021401-0000-0000-C000-000000000046");        public readonly UInt16 Reserved1 = 0;        public readonly UInt32 Reserved2 = 0;        public readonly UInt32 Reserved3 = 0;

        #region Constructor        public ShellLinkHeader()
        {
            //CreationTime = DateTime.Now.ToFileTime();
            //AccessTime = DateTime.Now.ToFileTime();
            //sWriteTime = DateTime.Now.ToFileTime();
            IconIndex = -1;
            ShowCommand = ShowCommand.SW_SHOWNORMAL;
            HotKey = new HotKeyFlags();
        }
        #endregion // Constructor        public virtual LinkFlags LinkFlags { get; set; }        public FileAttributesFlags FileAttributes { get; set; }        public Int64 CreationTime { get; set; }        public Int64 AccessTime { get; set; }        public Int64 WriteTime { get; set; }        public UInt32 FileSize { get; set; }        public Int32 IconIndex { get; set; }        public ShowCommand ShowCommand { get; set; }        public HotKeyFlags HotKey { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] Header = new byte[HeaderSize];
            Buffer.BlockCopy(BitConverter.GetBytes(HeaderSize), 0, Header, 0, 4);
            Buffer.BlockCopy(LinkCLSID.ToByteArray(), 0, Header, 4, 16);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)LinkFlags), 0, Header, 20, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)FileAttributes), 0, Header, 24, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(CreationTime), 0, Header, 28, 8);
            Buffer.BlockCopy(BitConverter.GetBytes(AccessTime), 0, Header, 36, 8);
            Buffer.BlockCopy(BitConverter.GetBytes(WriteTime), 0, Header, 44, 8);
            Buffer.BlockCopy(BitConverter.GetBytes(FileSize), 0, Header, 52, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(IconIndex), 0, Header, 56, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)ShowCommand), 0, Header, 60, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(HotKey.HotKey), 0, Header, 64, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt16)Reserved1), 0, Header, 66, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)Reserved2), 0, Header, 68, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)Reserved3), 0, Header, 72, 4);
            return Header;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("HeaderSize: {0} (0x{0:X})", HeaderSize);
            builder.AppendLine();
            builder.AppendFormat("LinkCLSID: {0}", LinkCLSID);
            builder.AppendLine();
            builder.AppendFormat("LinkFlags: {0}", LinkFlags);
            builder.AppendLine();
            builder.AppendFormat("FileAttributes: {0}", FileAttributes);
            builder.AppendLine();
            builder.AppendFormat("CreationTime: {0}", DateTime.FromFileTimeUtc(CreationTime));
            builder.AppendLine();
            builder.AppendFormat("AccessTime: {0}", DateTime.FromFileTimeUtc(AccessTime));
            builder.AppendLine();
            builder.AppendFormat("WriteTime: {0}", DateTime.FromFileTimeUtc(WriteTime));
            builder.AppendLine();
            builder.AppendFormat("FileSize: {0} (0x{0:X})", FileSize);
            builder.AppendLine();
            builder.AppendFormat("IconIndex: {0}", IconIndex);
            builder.AppendLine();
            builder.AppendFormat("ShowCommand: {0}", ShowCommand);
            builder.AppendLine();
            if (HotKey.HighByte != 0)
            {
                builder.AppendFormat("HotKey: {0} + {1}", HotKey.HighByte, HotKey.LowByte);
                builder.AppendLine();
            }
            else
            {
                builder.AppendFormat("HotKey: {0}", HotKey.LowByte);
                builder.AppendLine();
            }

            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static ShellLinkHeader FromByteArray(byte[] ba)
        {
            ShellLinkHeader Header = new ShellLinkHeader();

            if (ba.Length < Header.HeaderSize)
            {
                throw new ArgumentException(String.Format("Size of the LNK Header is less than {0} ({1})", Header.HeaderSize, ba.Length));
            }

            if (BitConverter.ToUInt32(ba, 0) != Header.HeaderSize)
            {
                throw new ArgumentException(String.Format("The LNK Header Size is {0} is incorrect (expected {1})", BitConverter.ToUInt32(ba, 0), Header.HeaderSize));
            }

            byte[] linkCLSID = new byte[16];
            Buffer.BlockCopy(ba, 4, linkCLSID, 0, linkCLSID.Length);
            if (!StructuralComparisons.StructuralEqualityComparer.Equals(Header.LinkCLSID.ToByteArray(), linkCLSID))
            {
                throw new ArgumentException(String.Format("The LNK CLSID is {0} is incorrect (expected {1})", new Guid(linkCLSID), Header.LinkCLSID));
            }

            Header.LinkFlags = (LinkFlags)BitConverter.ToUInt32(ba, 20);
            Header.FileAttributes = (FileAttributesFlags)BitConverter.ToUInt32(ba, 24);
            Header.CreationTime = BitConverter.ToInt64(ba, 28);
            Header.AccessTime = BitConverter.ToInt64(ba, 36);
            Header.WriteTime = BitConverter.ToInt64(ba, 44);
            Header.FileSize = BitConverter.ToUInt32(ba, 52);
            Header.IconIndex = BitConverter.ToInt32(ba, 56);
            Header.ShowCommand = (ShowCommand)BitConverter.ToUInt32(ba, 60);
            Header.HotKey = new HotKeyFlags(BitConverter.ToUInt16(ba, 64));
            return Header;
        }
        #endregion // FromByteArray
    }
}
