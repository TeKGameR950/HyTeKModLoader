﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public class ConsoleFEDataBlock : ExtraDataBlock
    {
        #region Constructor        public ConsoleFEDataBlock() : base() { }        public ConsoleFEDataBlock(UInt32 CodePage) : base()
        {
            this.CodePage = CodePage;
        }
        #endregion // Constructor        public override UInt32 BlockSize => 0xC;        public override BlockSignature BlockSignature => BlockSignature.CONSOLE_FE_PROPS;        public UInt32 CodePage { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] ConsoleFEDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, ConsoleFEDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, ConsoleFEDataBlock, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(CodePage), 0, ConsoleFEDataBlock, 8, 4);
            return ConsoleFEDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("CodePage: {0} (0x{0:X})", CodePage);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static ConsoleFEDataBlock FromByteArray(byte[] ba)
        {
            ConsoleFEDataBlock ConsoleFEDataBlock = new ConsoleFEDataBlock();
            if (ba.Length < 0xC)
            {
                throw new ArgumentException(String.Format("Size of the ConsoleFEDataBlock Structure is less than 12 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, ConsoleFEDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != ConsoleFEDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, ConsoleFEDataBlock.BlockSignature));
            }

            ConsoleFEDataBlock.CodePage = BitConverter.ToUInt32(ba, 8);

            return ConsoleFEDataBlock;
        }
        #endregion // FromByteArray
    }
}
