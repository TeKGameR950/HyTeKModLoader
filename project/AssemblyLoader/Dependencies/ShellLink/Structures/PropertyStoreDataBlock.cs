﻿using System;
using System.Text;
using PropertyStore;
using PropertyStore.Structures;
using System.Collections.Generic;

namespace ShellLink.Structures
{    public class PropertyStoreDataBlock : ExtraDataBlock
    {
        #region Constructor        public PropertyStoreDataBlock() : base()
        {
            PropertyStore = new List<SerializedPropertyStorage>();
        }        public PropertyStoreDataBlock(List<SerializedPropertyStorage> PropertyStore) : base()
        {
            this.PropertyStore = PropertyStore;
        }
        #endregion // Constructor

        #region BlockSize        public override UInt32 BlockSize
        {
            get
            {
                UInt32 Size = 12;
                for(int i = 0; i < PropertyStore.Count; i++)
                {
                    Size += PropertyStore[i].StorageSize;
                }
                return Size;
            }
        }
        #endregion // BlockSize        public override BlockSignature BlockSignature => BlockSignature.PROPERTY_STORE_PROPS;        public List<SerializedPropertyStorage> PropertyStore { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            int Offset = 8;
            byte[] PropertyStoreDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, PropertyStoreDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, PropertyStoreDataBlock, 4, 4);
            for (int i = 0; i < PropertyStore.Count; i++)
            {
                SerializedPropertyStorage PropertyStorage = PropertyStore[i];
                Buffer.BlockCopy(PropertyStorage.GetBytes(), 0, PropertyStoreDataBlock, Offset, (int)PropertyStorage.StorageSize);
                Offset += (int)PropertyStorage.StorageSize;
            }
            return PropertyStoreDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            for (int i = 0; i < PropertyStore.Count; i++)
            {
                builder.Append(PropertyStore[i].ToString());
            }
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static PropertyStoreDataBlock FromByteArray(byte[] ba)
        {
            PropertyStoreDataBlock PropertyStoreDataBlock = new PropertyStoreDataBlock();
            if (ba.Length < 0xC)
            {
                throw new ArgumentException(String.Format("Size of the PropertyStoreDataBlock Structure is less than 12 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, PropertyStoreDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != PropertyStoreDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, PropertyStoreDataBlock.BlockSignature));
            }

            byte[] data = new byte[BlockSize - 4];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize - 4), 0, data, 0, 4);
            Buffer.BlockCopy(ba, 8, data, 4, (int)BlockSize - 8);
            SerializedPropertyStore SerializedPropertyStore = SerializedPropertyStore.FromByteArray(data);
            PropertyStoreDataBlock.PropertyStore = SerializedPropertyStore.PropertyStorage;

            return PropertyStoreDataBlock;
        }
        #endregion // FromByteArray
    }
}
