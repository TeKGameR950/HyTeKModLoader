﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public class TrackerDataBlock : ExtraDataBlock
    {
        #region Constructor        public TrackerDataBlock() : base()
        {
            MachineID = "";
            Droid = new Guid[2];
            DroidBirth = new Guid[2];
        }
        #endregion // Constructor        public override UInt32 BlockSize => Length + 8;        public override BlockSignature BlockSignature => BlockSignature.TRACKER_PROPS;        public UInt32 Length => (UInt32)(MachineID.Length < 16 ? 0x58 : MachineID.Length + 0x48);        public UInt32 Version { get; set; }        public String MachineID { get; set; }        public Guid[] Droid { get; set; }        public Guid[] DroidBirth { get; set; }
        
        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] TrackerDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, TrackerDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, TrackerDataBlock, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(Length), 0, TrackerDataBlock, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(Version), 0, TrackerDataBlock, 12, 4);
            Buffer.BlockCopy(Encoding.Default.GetBytes(MachineID), 0, TrackerDataBlock, 16, (int)MachineID.Length);
            Buffer.BlockCopy(Droid[0].ToByteArray(), 0, TrackerDataBlock, (int)BlockSize - 64, 16);
            Buffer.BlockCopy(Droid[1].ToByteArray(), 0, TrackerDataBlock, (int)BlockSize - 48, 16);
            Buffer.BlockCopy(DroidBirth[0].ToByteArray(), 0, TrackerDataBlock, (int)BlockSize - 32, 16);
            Buffer.BlockCopy(DroidBirth[1].ToByteArray(), 0, TrackerDataBlock, (int)BlockSize - 16, 16);
            return TrackerDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("Version: {0}", Version);
            builder.AppendLine();
            builder.AppendFormat("MachineID: {0}", MachineID);
            builder.AppendLine();
            builder.AppendFormat("Droid: {0} {1}", Droid[0], Droid[1]);
            builder.AppendLine();
            builder.AppendFormat("DroidBirth: {0} {1}", DroidBirth[0], DroidBirth[1]);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static TrackerDataBlock FromByteArray(byte[] ba)
        {
            TrackerDataBlock TrackerDataBlock = new TrackerDataBlock();
            if (ba.Length < 0x60)
            {
                throw new ArgumentException(String.Format("Size of the TrackerDataBlock Structure is less than 96 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, TrackerDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != TrackerDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, TrackerDataBlock.BlockSignature));
            }

            UInt32 Length = BitConverter.ToUInt32(ba, 8);
            if (Length < 0x58)
            {
                throw new ArgumentException(String.Format("Length is {0} is incorrect (expected 88)", Length));
            }

            TrackerDataBlock.Version = BitConverter.ToUInt32(ba, 12);

            byte[] MachineID = new byte[Length - 0x48];
            Buffer.BlockCopy(ba, 16, MachineID, 0, MachineID.Length);
            TrackerDataBlock.MachineID = Encoding.Default.GetString(MachineID).TrimEnd(new char[] { (char)0 });

            byte[] Guid = new byte[16];
            Buffer.BlockCopy(ba, 16 + MachineID.Length, Guid, 0, Guid.Length);
            TrackerDataBlock.Droid = new Guid[2];
            TrackerDataBlock.Droid[0] = new Guid(Guid);
            Buffer.BlockCopy(ba, 32 + MachineID.Length, Guid, 0, Guid.Length);
            TrackerDataBlock.Droid[1] = new Guid(Guid);

            Buffer.BlockCopy(ba, 48 + MachineID.Length, Guid, 0, Guid.Length);
            TrackerDataBlock.DroidBirth = new Guid[2];
            TrackerDataBlock.DroidBirth[0] = new Guid(Guid);
            Buffer.BlockCopy(ba, 64 + MachineID.Length, Guid, 0, Guid.Length);
            TrackerDataBlock.DroidBirth[1] = new Guid(Guid);

            return TrackerDataBlock;
        }
        #endregion // FromByteArray
    }
}
