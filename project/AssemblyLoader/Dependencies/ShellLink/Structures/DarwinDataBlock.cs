﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public class DarwinDataBlock : ExtraDataBlock
    {
        #region Constructor        public DarwinDataBlock() : base()
        {
            DarwinDataAnsi = "";
            DarwinDataUnicode = "";
        }        public DarwinDataBlock(String DarwinData) : base()
        {
            DarwinDataAnsi = DarwinData;
            DarwinDataUnicode = DarwinData;
        }
        #endregion // Constructor        public override UInt32 BlockSize => 0x314;        public override BlockSignature BlockSignature => BlockSignature.DARWIN_PROPS;        public String DarwinDataAnsi { get; set; }        public String DarwinDataUnicode { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] EarwinDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, EarwinDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, EarwinDataBlock, 4, 4);
            Buffer.BlockCopy(Encoding.Default.GetBytes(DarwinDataAnsi), 0, EarwinDataBlock, 8, DarwinDataAnsi.Length < 259 ? DarwinDataAnsi.Length : 259);
            Buffer.BlockCopy(Encoding.Unicode.GetBytes(DarwinDataUnicode), 0, EarwinDataBlock, 268, DarwinDataUnicode.Length < 259 ? DarwinDataUnicode.Length * 2 : 518);
            return EarwinDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("DarwinDataAnsi: {0}", DarwinDataAnsi);
            builder.AppendLine();
            builder.AppendFormat("DarwinDataUnicode: {0}", DarwinDataUnicode);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static DarwinDataBlock FromByteArray(byte[] ba)
        {
            DarwinDataBlock DarwinDataBlock = new DarwinDataBlock();
            if (ba.Length < 0x314)
            {
                throw new ArgumentException(String.Format("Size of the DarwinDataBlock Structure is less than 788 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, DarwinDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != DarwinDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, DarwinDataBlock.BlockSignature));
            }

            byte[] DarwinDataAnsi = new byte[260];
            Buffer.BlockCopy(ba, 8, DarwinDataAnsi, 0, 260);
            DarwinDataBlock.DarwinDataAnsi = Encoding.Default.GetString(DarwinDataAnsi).TrimEnd(new char[] { (char)0 });

            byte[] DarwinDataUnicode = new byte[520];
            Buffer.BlockCopy(ba, 268, DarwinDataUnicode, 0, 520);
            DarwinDataBlock.DarwinDataUnicode = Encoding.Unicode.GetString(DarwinDataUnicode).TrimEnd(new char[] { (char)0 });

            return DarwinDataBlock;
        }
        #endregion // FromByteArray
    }
}
