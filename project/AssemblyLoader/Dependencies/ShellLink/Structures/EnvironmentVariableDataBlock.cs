﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public class EnvironmentVariableDataBlock : ExtraDataBlock
    {
        #region Constructor        public EnvironmentVariableDataBlock() : this("") { }        public EnvironmentVariableDataBlock(String Target) : base()
        {
            TargetAnsi = Target;
            TargetUnicode = Target;
        }
        #endregion // Constructor        public override UInt32 BlockSize => 0x314;        public override BlockSignature BlockSignature => BlockSignature.ENVIRONMENT_PROPS;        public String TargetAnsi { get; set; }        public String TargetUnicode { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] EnvironmentVariableDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, EnvironmentVariableDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, EnvironmentVariableDataBlock, 4, 4);
            Buffer.BlockCopy(Encoding.Default.GetBytes(TargetAnsi), 0, EnvironmentVariableDataBlock, 8, TargetAnsi.Length < 259 ? TargetAnsi.Length : 259);
            Buffer.BlockCopy(Encoding.Unicode.GetBytes(TargetUnicode), 0, EnvironmentVariableDataBlock, 268, TargetUnicode.Length < 259 ? TargetUnicode.Length * 2 : 518);
            return EnvironmentVariableDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("TargetAnsi: {0}", TargetAnsi);
            builder.AppendLine();
            builder.AppendFormat("TargetUnicode: {0}", TargetUnicode);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static EnvironmentVariableDataBlock FromByteArray(byte[] ba)
        {
            EnvironmentVariableDataBlock EnvironmentVariableDataBlock = new EnvironmentVariableDataBlock();
            if (ba.Length < 0x314)
            {
                throw new ArgumentException(String.Format("Size of the EnvironmentVariableDataBlock Structure is less than 788 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, EnvironmentVariableDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != EnvironmentVariableDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, EnvironmentVariableDataBlock.BlockSignature));
            }

            byte[] TargetAnsi = new byte[260];
            Buffer.BlockCopy(ba, 8, TargetAnsi, 0, 260);
            EnvironmentVariableDataBlock.TargetAnsi = Encoding.Default.GetString(TargetAnsi).TrimEnd(new char[] { (char)0 });

            byte[] TargetUnicode = new byte[520];
            Buffer.BlockCopy(ba, 268, TargetUnicode, 0, 520);
            EnvironmentVariableDataBlock.TargetUnicode = Encoding.Unicode.GetString(TargetUnicode).TrimEnd(new char[] { (char)0 });

            return EnvironmentVariableDataBlock;
        }
        #endregion // FromByteArray
    }
}
