﻿using System;
using System.Text;

namespace ShellLink.Structures
{    public class StringData : Structure
    {
        #region Constructor        public StringData() : this(true) { }        public StringData(Boolean IsUnicode) : base() { this.IsUnicode = IsUnicode; }
        #endregion // Constructor        public Boolean IsUnicode { get; set; }        public String NameString { get; set; }        public String RelativePath { get; set; }        public String WorkingDir { get; set; }        public String CommandLineArguments { get; set; }        public String IconLocation { get; set; }

        #region StringDataSize        public int StringDataSize
        {
            get
            {
                int Size = 0;
                if (NameString != null)
                {
                    Size += (IsUnicode ? (NameString.Length + 1) * 2 : NameString.Length + 1) + 2;
                }
                if (RelativePath != null)
                {
                    Size += (IsUnicode ? (RelativePath.Length + 1) * 2 : RelativePath.Length + 1) + 2;
                }
                if (WorkingDir != null)
                {
                    Size += (IsUnicode ? (WorkingDir.Length + 1) * 2 : WorkingDir.Length + 1) + 2;
                }
                if (CommandLineArguments != null)
                {
                    Size += (IsUnicode ? (CommandLineArguments.Length + 1) * 2 : CommandLineArguments.Length + 1) + 2;
                }
                if (IconLocation != null)
                {
                    Size += (IsUnicode ? (IconLocation.Length + 1) * 2 : IconLocation.Length + 1) + 2;
                }
                return Size;
            }
        }
        #endregion // StringDataSize

        #region GetBytes        public override byte[] GetBytes()
        {
            UInt16 Count = 0;
            int Offset = 0;
            byte[] StringData = new byte[StringDataSize];
            if (NameString != null)
            {
                Count = (UInt16)NameString.Length;
                Buffer.BlockCopy(BitConverter.GetBytes(Count + 1), 0, StringData, Offset, 2);
                if (IsUnicode)
                {
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(NameString), 0, StringData, Offset + 2, Count * 2);
                    Offset += (Count * 2) + 4;

                }
                else
                {
                    Buffer.BlockCopy(Encoding.Default.GetBytes(NameString), 0, StringData, Offset + 2, Count);
                    Offset += Count + 3;
                }
            }
            if (RelativePath != null)
            {
                Count = (UInt16)RelativePath.Length;
                Buffer.BlockCopy(BitConverter.GetBytes(Count + 1), 0, StringData, Offset, 2);
                if (IsUnicode)
                {
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(RelativePath), 0, StringData, Offset + 2, Count * 2);
                    Offset += (Count * 2) + 4;
                }
                else
                {
                    Buffer.BlockCopy(Encoding.Default.GetBytes(RelativePath), 0, StringData, Offset + 2, Count);
                    Offset += Count + 3;
                }
            }
            if (WorkingDir != null)
            {
                Count = (UInt16)WorkingDir.Length;
                Buffer.BlockCopy(BitConverter.GetBytes(Count + 1), 0, StringData, Offset, 2);
                if (IsUnicode)
                {
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(WorkingDir), 0, StringData, Offset + 2, Count * 2);
                    Offset += (Count * 2) + 4;
                }
                else
                {
                    Buffer.BlockCopy(Encoding.Default.GetBytes(WorkingDir), 0, StringData, Offset + 2, Count);
                    Offset += Count + 3;
                }
            }
            if (CommandLineArguments != null)
            {
                Count = (UInt16)CommandLineArguments.Length;
                Buffer.BlockCopy(BitConverter.GetBytes(Count + 1), 0, StringData, Offset, 2);
                if (IsUnicode)
                {
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(CommandLineArguments), 0, StringData, Offset + 2, Count * 2);
                    Offset += (Count * 2) + 4;
                }
                else
                {
                    Buffer.BlockCopy(Encoding.Default.GetBytes(CommandLineArguments), 0, StringData, Offset + 2, Count);
                    Offset += Count + 3;
                }
            }
            if (IconLocation != null)
            {
                Count = (UInt16)IconLocation.Length;
                Buffer.BlockCopy(BitConverter.GetBytes(Count + 1), 0, StringData, Offset, 2);
                if (IsUnicode)
                {
                    Buffer.BlockCopy(Encoding.Unicode.GetBytes(IconLocation), 0, StringData, Offset + 2, Count * 2);
                    Offset += (Count * 2) + 4;
                }
                else
                {
                    Buffer.BlockCopy(Encoding.Default.GetBytes(IconLocation), 0, StringData, Offset + 2, Count);
                    Offset += Count + 3;
                }
            }
            return StringData;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            if (NameString != null)
            {
                builder.AppendFormat("NameString: {0}", NameString);
                builder.AppendLine();
            }
            if (RelativePath != null)
            {
                builder.AppendFormat("RelativePath: {0}", RelativePath);
                builder.AppendLine();
            }
            if (WorkingDir != null)
            {
                builder.AppendFormat("WorkingDir: {0}", WorkingDir);
                builder.AppendLine();
            }
            if (CommandLineArguments != null)
            {
                builder.AppendFormat("CommandLineArguments: {0}", CommandLineArguments);
                builder.AppendLine();
            }
            if (IconLocation != null)
            {
                builder.AppendFormat("IconLocation: {0}", IconLocation);
                builder.AppendLine();
            }
            return builder.ToString();
        }
        #endregion // ToString
    }
}
