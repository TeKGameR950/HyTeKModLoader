﻿using System;
using System.Text;
using ShellLink.Flags;

namespace ShellLink.Structures
{    public class SpecialFolderDataBlock : ExtraDataBlock
    {
        #region Constructor        public SpecialFolderDataBlock() : base() { }        public SpecialFolderDataBlock(CSIDL SpecialFolderID, UInt32 Offset) : base()
        {
            this.SpecialFolderID = SpecialFolderID;
            this.Offset = Offset;
        }
        #endregion // Constructor        public override UInt32 BlockSize => 0x10;        public override BlockSignature BlockSignature => BlockSignature.SPECIAL_FOLDER_PROPS;        public CSIDL SpecialFolderID { get; set; }        public UInt32 Offset { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] SpecialFolderDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, SpecialFolderDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, SpecialFolderDataBlock, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)SpecialFolderID), 0, SpecialFolderDataBlock, 8, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(Offset), 0, SpecialFolderDataBlock, 12, 4);
            return SpecialFolderDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("SpecialFolderID: {0}", SpecialFolderID);
            builder.AppendLine();
            builder.AppendFormat("Offset: {0} (0x{0:X})", Offset);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static SpecialFolderDataBlock FromByteArray(byte[] ba)
        {
            SpecialFolderDataBlock SpecialFolderDataBlock = new SpecialFolderDataBlock();
            if (ba.Length < 0x10)
            {
                throw new ArgumentException(String.Format("Size of the SpecialFolderDataBlock Structure is less than 16 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, SpecialFolderDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != SpecialFolderDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, SpecialFolderDataBlock.BlockSignature));
            }

            SpecialFolderDataBlock.SpecialFolderID = (CSIDL)BitConverter.ToUInt32(ba, 8);
            SpecialFolderDataBlock.Offset = BitConverter.ToUInt32(ba, 12);

            return SpecialFolderDataBlock;
        }
        #endregion // FromByteArray
    }
}
