﻿using System;
using System.Text;
using ShellLink.Const;

namespace ShellLink.Structures
{    public class KnownFolderDataBlock : ExtraDataBlock
    {
        #region Constructor        public KnownFolderDataBlock() : base() { }        public KnownFolderDataBlock(Guid KnownFolderID, UInt32 Offset) : base()
        {
            this.KnownFolderID = KnownFolderID;
            this.Offset = Offset;
        }
        #endregion // Constructor        public override UInt32 BlockSize => 0x1C;        public override BlockSignature BlockSignature => BlockSignature.KNOWN_FOLDER_PROPS;        public Guid KnownFolderID { get; set; }        public UInt32 Offset { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] KnownFolderDataBlock = new byte[BlockSize];
            Buffer.BlockCopy(BitConverter.GetBytes(BlockSize), 0, KnownFolderDataBlock, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((UInt32)BlockSignature), 0, KnownFolderDataBlock, 4, 4);
            Buffer.BlockCopy(KnownFolderID.ToByteArray(), 0, KnownFolderDataBlock, 8, 16);
            Buffer.BlockCopy(BitConverter.GetBytes(Offset), 0, KnownFolderDataBlock, 24, 4);
            return KnownFolderDataBlock;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("DisplayName: {0}", KNOWNFOLDERID.GetDisplayName(KnownFolderID));
            builder.AppendLine();
            builder.AppendFormat("KnownFolderID: {0}", KnownFolderID);
            builder.AppendLine();
            builder.AppendFormat("Offset: {0} (0x{0:X})", Offset);
            builder.AppendLine();
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static KnownFolderDataBlock FromByteArray(byte[] ba)
        {
            KnownFolderDataBlock KnownFolderDataBlock = new KnownFolderDataBlock();
            if (ba.Length < 0x1C)
            {
                throw new ArgumentException(String.Format("Size of the KnownFolderDataBlock Structure is less than 28 ({0})", ba.Length));
            }

            UInt32 BlockSize = BitConverter.ToUInt32(ba, 0);
            if (BlockSize > ba.Length)
            {
                throw new ArgumentException(String.Format("BlockSize is {0} is incorrect (expected {1})", BlockSize, KnownFolderDataBlock.BlockSize));
            }

            BlockSignature BlockSignature = (BlockSignature)BitConverter.ToUInt32(ba, 4);
            if (BlockSignature != KnownFolderDataBlock.BlockSignature)
            {
                throw new ArgumentException(String.Format("BlockSignature is {0} is incorrect (expected {1})", BlockSignature, KnownFolderDataBlock.BlockSignature));
            }

            byte[] KnownFolderID = new byte[16];
            Buffer.BlockCopy(ba, 8, KnownFolderID, 0, KnownFolderID.Length);
            KnownFolderDataBlock.KnownFolderID = new Guid(KnownFolderID);
            KnownFolderDataBlock.Offset = BitConverter.ToUInt32(ba, 24);

            return KnownFolderDataBlock;
        }
        #endregion // FromByteArray
    }
}
