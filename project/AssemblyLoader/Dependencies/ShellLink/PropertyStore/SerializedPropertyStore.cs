﻿using System;
using System.Linq;
using PropertyStore.Structures;
using System.Collections.Generic;
using System.Text;

namespace PropertyStore
{    public class SerializedPropertyStore : Structure
    {
        #region Constructor        public SerializedPropertyStore() : base()
        {
            PropertyStorage = new List<SerializedPropertyStorage>();
        }        public SerializedPropertyStore(List<SerializedPropertyStorage> SerializedPropertyStorage) : base()
        {
            this.PropertyStorage = SerializedPropertyStorage;
        }
        #endregion // Constructor

        #region FromByteArray        public static SerializedPropertyStore FromByteArray(byte[] ba)
        {
            SerializedPropertyStore PropertyStore = new SerializedPropertyStore();

            if (ba.Length < 8)
            {
                throw new ArgumentException(String.Format("Size of the SerializedPropertyStore is less than 8 ({0})", ba.Length));
            }

            UInt32 StoreSize = BitConverter.ToUInt32(ba, 0);
            if (ba.Length < StoreSize)
            {
                throw new ArgumentException(String.Format("Size of the SerializedPropertyStore is less than {0} ({1})", StoreSize, ba.Length));
            }

            ba = ba.Skip(4).ToArray();
            UInt32 StorageSize = BitConverter.ToUInt32(ba, 0);
            while (StorageSize > 5)
            {
                SerializedPropertyStorage PropertyStorage = SerializedPropertyStorage.FromByteArray(ba);
                PropertyStore.PropertyStorage.Add(PropertyStorage);
                ba = ba.Skip((int)StorageSize).ToArray();
                StorageSize = BitConverter.ToUInt32(ba, 0);
            }

            return PropertyStore;
        }
        #endregion // FromByteArray

        #region StorageSize        public virtual UInt32 StoreSize
        {
            get
            {
                UInt32 Size = 8;
                for(int i = 0; i < PropertyStorage.Count; i++)
                {
                    Size += PropertyStorage[i].StorageSize;
                }
                return Size;
            }
        }
        #endregion // StoreSize        public List<SerializedPropertyStorage> PropertyStorage { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            int Offset = 4;
            byte[] PropertyStore = new byte[StoreSize];
            Buffer.BlockCopy(BitConverter.GetBytes(StoreSize), 0, PropertyStore, 0, 4);
            for (int i = 0; i < this.PropertyStorage.Count; i++)
            {
                SerializedPropertyStorage PropertyStorage = this.PropertyStorage[i];
                Buffer.BlockCopy(PropertyStorage.GetBytes(), 0, PropertyStore, Offset, (int)PropertyStorage.StorageSize);
                Offset += (int)PropertyStorage.StorageSize;
            }
            return PropertyStore;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("StoreSize: {0} (0x{0X})", StoreSize);
            builder.AppendLine();
            for (int i = 0; i < PropertyStorage.Count; i++)
            {
                builder.Append(PropertyStorage[i].ToString());
            }
            return builder.ToString();
        }
        #endregion // ToString
    }
}
