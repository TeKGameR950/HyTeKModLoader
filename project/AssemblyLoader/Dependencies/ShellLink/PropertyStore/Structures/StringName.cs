﻿using PropertyStore.Flags;
using System;
using System.Linq;
using System.Text;

namespace PropertyStore.Structures
{    public class StringName : SerializedPropertyValue
    {
        #region Constructor        public StringName() : base() { }        public StringName(String Name, TypedPropertyValue TypedPropertyValue) : base()
        {
            this.Name = Name;
            this.TypedPropertyValue = TypedPropertyValue;
        }
        #endregion // StringName        public override UInt32 ValueSize => 9 + NameSize + (UInt32)TypedPropertyValue.GetBytes().Length;        public UInt32 NameSize => ((UInt32)Name.Length + 1) * 2;        public byte Reserved => 0x00;        public String Name { get; set; }

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] PropertyValue = new byte[ValueSize];
            Buffer.BlockCopy(BitConverter.GetBytes(ValueSize), 0, PropertyValue, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(NameSize), 0, PropertyValue, 4, 4);
            Buffer.BlockCopy(Encoding.Unicode.GetBytes(Name), 0, PropertyValue, 9, 4);
            Buffer.BlockCopy(TypedPropertyValue.GetBytes(), 0, PropertyValue, 9 + (int)NameSize, TypedPropertyValue.GetBytes().Length);
            return PropertyValue;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("ValueSize: {0} (0x{0:X})", ValueSize);
            builder.AppendLine();
            builder.AppendFormat("NameSize: {0} (0x{0:X})", NameSize);
            builder.AppendLine();
            builder.AppendFormat("Name: {0}", Name);
            builder.AppendLine();
            builder.Append(TypedPropertyValue.ToString());
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static SerializedPropertyValue FromByteArray(byte[] ba)
        {
            StringName StringName = new StringName();

            if (ba.Length < 9)
            {
                throw new ArgumentException(String.Format("Size of the StringName is less than 9 ({0})", ba.Length));
            }

            UInt32 ValueSize = BitConverter.ToUInt32(ba, 0);
            if (ba.Length < ValueSize)
            {
                throw new ArgumentException(String.Format("Size of the StringName is not equal to {0} ({1})", ValueSize, ba.Length));
            }

            UInt32 NameSize = BitConverter.ToUInt32(ba, 4);
            if (ba.Length - 9 < NameSize)
            {
                throw new ArgumentException(String.Format("Size of the NameSize is not equal to {0} ({1})", ValueSize, ba.Length - 8));
            }

            byte[] Name = ba.Skip(9).Take((int)NameSize).ToArray();
            StringName.Name = Encoding.Unicode.GetString(Name).TrimEnd(new char[] { (char)0 });

            PropertyType Type = (PropertyType)BitConverter.ToUInt16(ba, 9);
            byte[] Value = ba.Skip(9 + (int)NameSize).Take((int)(ValueSize - 9 - (int)NameSize)).ToArray();
            StringName.TypedPropertyValue = new TypedPropertyValue(Type, Value);

            return StringName;
        }
        #endregion // FromByteArray
    }
}
