﻿using PropertyStore.Flags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyStore.Structures
{    public class IntegerName : SerializedPropertyValue
    {
        #region Constructor        public IntegerName() : base() { }        public IntegerName(UInt32 ID, TypedPropertyValue TypedPropertyValue) : base()
        {
            this.ID = ID;
            this.TypedPropertyValue = TypedPropertyValue;
        }
        #endregion // IntegerName        public override UInt32 ValueSize => 9 + (UInt32)TypedPropertyValue.GetBytes().Length;        public UInt32 ID { get; set; }        public byte Reserved => 0x00;

        #region GetBytes        public override byte[] GetBytes()
        {
            byte[] PropertyValue = new byte[ValueSize];
            Buffer.BlockCopy(BitConverter.GetBytes(ValueSize), 0, PropertyValue, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(ID), 0, PropertyValue, 4, 4);
            Buffer.BlockCopy(TypedPropertyValue.GetBytes(), 0, PropertyValue, 9, TypedPropertyValue.GetBytes().Length);
            return PropertyValue;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());
            builder.AppendFormat("ValueSize: {0} (0x{0:X})", ValueSize);
            builder.AppendLine();
            builder.AppendFormat("ID: 0x{0:X}", ID);
            builder.AppendLine();
            builder.Append(TypedPropertyValue.ToString());
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static SerializedPropertyValue FromByteArray(byte[] ba)
        {
            IntegerName IntegerName = new IntegerName();

            if (ba.Length < 9)
            {
                throw new ArgumentException(String.Format("Size of the StringName is less than 9 ({0})", ba.Length));
            }

            UInt32 ValueSize = BitConverter.ToUInt32(ba, 0);
            if (ba.Length < ValueSize)
            {
                throw new ArgumentException(String.Format("Size of the StringName is not equal to {0} ({1})", ValueSize, ba.Length));
            }

            IntegerName.ID = BitConverter.ToUInt32(ba, 4);
            PropertyType Type = (PropertyType)BitConverter.ToUInt16(ba, 9);
            byte[] Value = ba.Skip(13).Take((int)(ValueSize - 13)).ToArray();
            IntegerName.TypedPropertyValue = new TypedPropertyValue(Type, Value);

            return IntegerName;
        }
        #endregion // FromByteArray
    }
}
