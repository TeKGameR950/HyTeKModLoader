﻿using System;

namespace PropertyStore.Structures
{    public abstract class SerializedPropertyValue : Structure
    {        public virtual UInt32 ValueSize => 9;        public TypedPropertyValue TypedPropertyValue { get; set; }
    }
}
