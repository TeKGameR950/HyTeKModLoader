﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using ShellLink.Flags;
using ShellLink.Structures;

namespace ShellLink
{    public class Shortcut : ShellLinkHeader
    {
        #region Constructor        public Shortcut() : base()
        {
            ExtraData = new ExtraData();
        }
        #endregion // Constructor        public ShellLinkHeader ShellLinkHeader => (ShellLinkHeader)this;

        #region LinkFlags        public override LinkFlags LinkFlags
        {
            get
            {
                LinkFlags linkFlags = base.LinkFlags;

                if (LinkTargetIDList != null)
                {
                    linkFlags |= LinkFlags.HasLinkTargetIDList;
                }
                else
                {
                    linkFlags &= ~LinkFlags.HasLinkTargetIDList;
                }

                if (LinkInfo != null)
                {
                    linkFlags |= LinkFlags.HasLinkInfo;
                }
                else
                {
                    linkFlags &= ~LinkFlags.HasLinkInfo;
                }

                if (StringData != null)
                {
                    linkFlags &= ~(LinkFlags.HasName | LinkFlags.HasRelativePath | LinkFlags.HasWorkingDir | LinkFlags.HasArguments | LinkFlags.HasIconLocation);
                    if (StringData.NameString != null)
                    {
                        linkFlags |= LinkFlags.HasName;
                    }
                    if (StringData.RelativePath != null)
                    {
                        linkFlags |= LinkFlags.HasRelativePath;
                    }
                    if (StringData.WorkingDir != null)
                    {
                        linkFlags |= LinkFlags.HasWorkingDir;
                    }
                    if (StringData.CommandLineArguments != null)
                    {
                        linkFlags |= LinkFlags.HasArguments;
                    }
                    if (StringData.IconLocation != null)
                    {
                        linkFlags |= LinkFlags.HasIconLocation;
                    }
                    if(StringData.IsUnicode)
                    {
                        linkFlags |= LinkFlags.IsUnicode;
                    }
                }
                else
                {
                    linkFlags &= ~(LinkFlags.HasName | LinkFlags.HasRelativePath | LinkFlags.HasWorkingDir | LinkFlags.HasArguments | LinkFlags.HasIconLocation);
                }

                if (ExtraData.EnvironmentVariableDataBlock != null)
                {
                    linkFlags |= LinkFlags.HasExpString;
                }
                else
                {
                    linkFlags &= ~LinkFlags.HasExpString;
                }

                if (ExtraData.DarwinDataBlock != null)
                {
                    linkFlags |= LinkFlags.HasDarwinID;
                }
                else
                {
                    linkFlags &= ~LinkFlags.HasDarwinID;
                }

                if (ExtraData.IconEnvironmentDataBlock != null)
                {
                    linkFlags |= LinkFlags.HasExpIcon;
                }
                else
                {
                    linkFlags &= ~LinkFlags.HasExpIcon;
                }

                if (ExtraData.ShimDataBlock != null)
                {
                    linkFlags |= LinkFlags.RunWithShimLayer;
                }
                else
                {
                    linkFlags &= ~LinkFlags.RunWithShimLayer;
                }

                if (ExtraData.PropertyStoreDataBlock != null)
                {
                    linkFlags |= LinkFlags.EnableTargetMetadata;
                }

                return linkFlags;
            }
        }
        #endregion // LinkFlags        public LinkTargetIDList LinkTargetIDList { get; set; }        public LinkInfo LinkInfo { get; set; }        public StringData StringData { get; set; }        public ExtraData ExtraData { get; set; }

        #region ReadFromFile        public static Shortcut ReadFromFile(String path)
        {
            return Shortcut.FromByteArray(File.ReadAllBytes(path));
        }

        #endregion // ReadFromFile

        #region CreateShortcut        public static Shortcut CreateShortcut(String path)
        {
            Shortcut lnk = new Shortcut();
            lnk.ExtraData.EnvironmentVariableDataBlock = new EnvironmentVariableDataBlock(path);
            return lnk;
        }        public static Shortcut CreateShortcut(String path, String iconpath, int iconindex)
        {
            Shortcut lnk = CreateShortcut(path);
            lnk.IconIndex = iconindex;
            lnk.StringData = new StringData(true)
            {
                IconLocation = iconpath
            };
            lnk.ExtraData.IconEnvironmentDataBlock = new IconEnvironmentDataBlock(iconpath);
            return lnk;
        }        public static Shortcut CreateShortcut(String path, String args)
        {
            Shortcut lnk = CreateShortcut(path);
            lnk.StringData = new StringData(true)
            {
                CommandLineArguments = args
            };
            return lnk;
        }        public static Shortcut CreateShortcut(String path, String args, String iconpath, int iconindex)
        {
            Shortcut lnk = CreateShortcut(path, args);
            lnk.IconIndex = iconindex;
            lnk.StringData.IconLocation = iconpath;
            lnk.ExtraData.IconEnvironmentDataBlock = new IconEnvironmentDataBlock(iconpath);
            return lnk;
        }        public static Shortcut CreateShortcut(String path, String args, String workdir)
        {
            Shortcut lnk = CreateShortcut(path, args);
            lnk.StringData.WorkingDir = workdir;
            return lnk;
        }        public static Shortcut CreateShortcut(String path, String args, String workdir, String iconpath, int iconindex)
        {
            Shortcut lnk = CreateShortcut(path, args, iconpath, iconindex);
            lnk.StringData.WorkingDir = workdir;
            return lnk;
        }
        #endregion // CreateShortcut

        #region WriteToFile        public void WriteToFile(String path)
        {
            File.WriteAllBytes(path, GetBytes());
        }
        #endregion // WriteToFile

        #region Size        public int Size
        {
            get
            {
                int size = (int)HeaderSize + ExtraData.ExtraDataSize;

                if ((LinkFlags & LinkFlags.HasLinkTargetIDList) != 0)
                {
                    size += LinkTargetIDList.IDListSize + 2;
                }

                if ((LinkFlags & LinkFlags.HasLinkInfo) != 0)
                {
                    size += (int)LinkInfo.LinkInfoSize;
                }

                if(StringData != null)
                {
                    size += StringData.StringDataSize;
                }

                return size;
            }
        }
        #endregion // Size

        #region GetBytes        public override byte[] GetBytes()
        {
            int Offset = 0;
            byte[] lnk = new byte[Size];
            Buffer.BlockCopy(base.GetBytes(), 0, lnk, 0, (int)HeaderSize);
            Offset += (int)HeaderSize;

            if ((LinkFlags & LinkFlags.HasLinkTargetIDList) != 0)
            {
                Buffer.BlockCopy(LinkTargetIDList.GetBytes(), 0, lnk, Offset, LinkTargetIDList.IDListSize + 2);
                Offset += LinkTargetIDList.IDListSize + 2;
            }

            if ((LinkFlags & LinkFlags.HasLinkInfo) != 0)
            {
                Buffer.BlockCopy(LinkInfo.GetBytes(), 0, lnk, Offset, (int)LinkInfo.LinkInfoSize);
                Offset += (int)LinkInfo.LinkInfoSize;
            }

            if(StringData != null)
            {
                Buffer.BlockCopy(StringData.GetBytes(), 0, lnk, Offset, (int)StringData.StringDataSize);
                Offset += (int)StringData.StringDataSize;
            }

            Buffer.BlockCopy(ExtraData.GetBytes(), 0, lnk, Offset, (int)ExtraData.ExtraDataSize);
            return lnk;
        }
        #endregion // GetBytes

        #region ToString        public override String ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToString());

            if ((LinkFlags & LinkFlags.HasLinkTargetIDList) != 0)
            {
                builder.Append(LinkTargetIDList.ToString());
            }
            if ((LinkFlags & LinkFlags.HasLinkInfo) != 0)
            {
                builder.Append(LinkInfo.ToString());
            }
            if (StringData != null)
            {
                builder.Append(StringData.ToString());
            }
            if (ExtraData.ExtraDataSize > 4)
            {
                builder.Append(ExtraData.ToString());
            }
            return builder.ToString();
        }
        #endregion // ToString

        #region FromByteArray        public static new Shortcut FromByteArray(byte[] ba)
        {
            Shortcut lnk = new Shortcut();

            #region SHELL_LINK_HEADER
            ShellLinkHeader Header = ShellLinkHeader.FromByteArray(ba);
            UInt32 HeaderSize = BitConverter.ToUInt32(ba, 0);
            Boolean IsUnicode = (Header.LinkFlags & LinkFlags.IsUnicode) != 0;
            lnk.LinkFlags = Header.LinkFlags;
            lnk.FileAttributes = Header.FileAttributes;
            lnk.CreationTime = Header.CreationTime;
            lnk.AccessTime = Header.AccessTime;
            lnk.WriteTime = Header.WriteTime;
            lnk.FileSize = Header.FileSize;
            lnk.IconIndex = Header.IconIndex;
            lnk.ShowCommand = Header.ShowCommand;
            lnk.HotKey = Header.HotKey;
            ba = ba.Skip((int)HeaderSize).ToArray();
            #endregion // SHELL_LINK_HEADER

            #region LINKTARGET_IDLIST
            if ((Header.LinkFlags & LinkFlags.HasLinkTargetIDList) != 0)
            {
                lnk.LinkTargetIDList = LinkTargetIDList.FromByteArray(ba);
                UInt16 IDListSize = BitConverter.ToUInt16(ba, 0);
                ba = ba.Skip(IDListSize + 2).ToArray();
            }
            #endregion // LINKTARGET_IDLIST

            #region LINKINFO
            if ((Header.LinkFlags & LinkFlags.HasLinkInfo) != 0)
            {
                lnk.LinkInfo = LinkInfo.FromByteArray(ba);
                UInt32 LinkInfoSize = BitConverter.ToUInt32(ba, 0);
                ba = ba.Skip((int)LinkInfoSize).ToArray();
            }
            #endregion // LINKINFO

            #region STRING_DATA
            if ((Header.LinkFlags & LinkFlags.HasName) != 0)
            {
                if (lnk.StringData == null)
                {
                    lnk.StringData = new StringData(IsUnicode);
                }

                UInt16 CountCharacters = BitConverter.ToUInt16(ba, 0);
                if (IsUnicode)
                {
                    lnk.StringData.NameString = Encoding.Unicode.GetString(ba.Skip(2).Take(CountCharacters * 2).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters * 2 + 2).ToArray();
                }
                else
                {
                    lnk.StringData.NameString = Encoding.Default.GetString(ba.Skip(2).Take(CountCharacters).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters + 2).ToArray();
                }

            }

            if ((Header.LinkFlags & LinkFlags.HasRelativePath) != 0)
            {
                if (lnk.StringData == null)
                {
                    lnk.StringData = new StringData(IsUnicode);
                }

                UInt16 CountCharacters = BitConverter.ToUInt16(ba, 0);
                if (IsUnicode)
                {
                    lnk.StringData.RelativePath = Encoding.Unicode.GetString(ba.Skip(2).Take(CountCharacters * 2).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters * 2 + 2).ToArray();
                }
                else
                {
                    lnk.StringData.RelativePath = Encoding.Default.GetString(ba.Skip(2).Take(CountCharacters).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters + 2).ToArray();
                }
            }

            if ((Header.LinkFlags & LinkFlags.HasWorkingDir) != 0)
            {
                if (lnk.StringData == null)
                {
                    lnk.StringData = new StringData(IsUnicode);
                }

                UInt16 CountCharacters = BitConverter.ToUInt16(ba, 0);
                if (IsUnicode)
                {
                    lnk.StringData.WorkingDir = Encoding.Unicode.GetString(ba.Skip(2).Take(CountCharacters * 2).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters * 2 + 2).ToArray();
                }
                else
                {
                    lnk.StringData.WorkingDir = Encoding.Default.GetString(ba.Skip(2).Take(CountCharacters).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters + 2).ToArray();
                }
            }

            if ((Header.LinkFlags & LinkFlags.HasArguments) != 0)
            {
                if (lnk.StringData == null)
                {
                    lnk.StringData = new StringData(IsUnicode);
                }

                UInt16 CountCharacters = BitConverter.ToUInt16(ba, 0);
                if (IsUnicode)
                {
                    lnk.StringData.CommandLineArguments = Encoding.Unicode.GetString(ba.Skip(2).Take(CountCharacters * 2).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters * 2 + 2).ToArray();
                }
                else
                {
                    lnk.StringData.CommandLineArguments = Encoding.Default.GetString(ba.Skip(2).Take(CountCharacters).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters + 2).ToArray();
                }
            }

            if ((Header.LinkFlags & LinkFlags.HasIconLocation) != 0)
            {
                if (lnk.StringData == null)
                {
                    lnk.StringData = new StringData(IsUnicode);
                }

                UInt16 CountCharacters = BitConverter.ToUInt16(ba, 0);
                if (IsUnicode)
                {
                    lnk.StringData.IconLocation = Encoding.Unicode.GetString(ba.Skip(2).Take(CountCharacters * 2).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters * 2 + 2).ToArray();
                }
                else
                {
                    lnk.StringData.IconLocation = Encoding.Default.GetString(ba.Skip(2).Take(CountCharacters).ToArray()).TrimEnd(new char[] { (char)0 });
                    ba = ba.Skip(CountCharacters + 2).ToArray();
                }
            }
            #endregion // STRING_DATA

            #region EXTRA_DATA
            if (ba.Length >= 4)
            {
                lnk.ExtraData = ExtraData.FromByteArray(ba);
            }
            #endregion // EXTRA_DATA

            return lnk;
        }
        #endregion // FromByteArray
    }
}