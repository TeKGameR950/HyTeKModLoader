﻿using System;
using System.Runtime.InteropServices;

namespace ShellLink.Internal
{    class Win32
    {        public const int MAX_PATH = 260;        [DllImport("shell32.dll")]
        public static extern bool SHGetPathFromIDListW(byte[] pidl, byte[] pszPath);        [DllImport("shell32.dll", SetLastError = true)]
        public static extern UInt32 SHGetNameFromIDList(byte[] pidl, SIGDN sigdnName, out IntPtr ppszName);        [DllImport("shell32.dll", SetLastError = true)]
        public static extern UInt32 SHGetNameFromIDList(IntPtr pidl, SIGDN sigdnName, out IntPtr ppszName);        [DllImport("shell32.dll")]
        public static extern int SHGetKnownFolderIDList([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags, IntPtr hToken, out IntPtr ppidl);        [DllImport("ole32.dll")]
        public static extern void CoTaskMemFree(IntPtr pv);        [DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr ILCreateFromPath([MarshalAs(UnmanagedType.LPWStr)] string pszPath);        [DllImport("shell32.dll")]
        public static extern void ILFree(IntPtr pidl);        [DllImport("shell32.dll")]
        public static extern IntPtr SHSimpleIDListFromPath([MarshalAs(UnmanagedType.LPWStr)] string pszPath);
    }

    enum SIGDN : UInt32
    {        SIGDN_NORMALDISPLAY = 0x00000000,        SIGDN_PARENTRELATIVEPARSING = 0x80018001,        SIGDN_DESKTOPABSOLUTEPARSING = 0x80028000,        SIGDN_PARENTRELATIVEEDITING = 0x80031001,        SIGDN_DESKTOPABSOLUTEEDITING = 0x8004c000,        SIGDN_FILESYSPATH = 0x80058000,        SIGDN_URL = 0x80068000,        SIGDN_PARENTRELATIVEFORADDRESSBAR = 0x8007c001,        SIGDN_PARENTRELATIVE = 0x80080001,        SIGDN_PARENTRELATIVEFORUI = 0x80094001
    }
}
