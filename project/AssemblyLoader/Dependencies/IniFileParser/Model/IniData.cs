using System;
using IniParser.Model.Configuration;
using IniParser.Model.Formatting;

namespace IniParser.Model
{
    public class IniData : ICloneable
    {
        #region Non-Public Members
        private SectionDataCollection _sections;
        #endregion

        #region Initialization
        public IniData()
            : this(new SectionDataCollection())
        { }
        public IniData(SectionDataCollection sdc)
        {
            _sections = (SectionDataCollection)sdc.Clone();
            Global = new KeyDataCollection();
            SectionKeySeparator = '.';
        }

        public IniData(IniData ori): this((SectionDataCollection)ori.Sections)
        {
            Global = (KeyDataCollection)ori.Global.Clone();
            Configuration = ori.Configuration.Clone();
        }
        #endregion

        #region Properties
        public IniParserConfiguration Configuration
        {
            get
            {
                // Lazy initialization
                if (_configuration == null)
                    _configuration = new IniParserConfiguration();

                return _configuration;
            }

            set { _configuration = value.Clone(); }
        }
        public KeyDataCollection Global { get; protected set; }
        public KeyDataCollection this[string sectionName]
        {
            get
            {
                if (!_sections.ContainsSection(sectionName))
                    if (Configuration.AllowCreateSectionsOnFly)
                        _sections.AddSection(sectionName);
                    else
                        return null;

                return _sections[sectionName];
            }
        }
        public SectionDataCollection Sections
        {
            get { return _sections; }
            set { _sections = value; }
        }
        public char SectionKeySeparator { get; set; }
        #endregion

        #region Object Methods
        public override string ToString()
        {
            return ToString(new DefaultIniDataFormatter(Configuration));
        }

        public virtual string ToString(IIniDataFormatter formatter)
        {
            return formatter.IniDataToString(this);
        }
        #endregion

        #region ICloneable Members
        public object Clone()
        {
            return new IniData(this);
        }

        #endregion

        #region Fields
        private IniParserConfiguration _configuration;
        #endregion
        public void ClearAllComments()
        {
            Global.ClearComments();

            foreach(var section in Sections)
            {
                section.ClearComments();
            }
        }
        public void Merge(IniData toMergeIniData)
        {

            if (toMergeIniData == null) return;

            Global.Merge(toMergeIniData.Global);

            Sections.Merge(toMergeIniData.Sections);

        }
        public bool TryGetKey(string key, out string value)
        {
            value = string.Empty;
            if (string.IsNullOrEmpty(key))
                return false;

            var splitKey = key.Split(SectionKeySeparator);
            var separatorCount = splitKey.Length - 1;
            if (separatorCount > 1)
                throw new ArgumentException("key contains multiple separators", "key");

            if (separatorCount == 0)
            {
                if (!Global.ContainsKey(key))
                    return false;

                value = Global[key];
                return true;
            }

            var section = splitKey[0];
            key = splitKey[1];

            if (!_sections.ContainsSection(section))
                return false;
            var sectionData = _sections[section];
            if (!sectionData.ContainsKey(key))
                return false;

            value = sectionData[key];
            return true;
        }
        public string GetKey(string key)
        {
            string result;
            return TryGetKey(key, out result) ? result : null;
        }
        private void MergeSection(SectionData otherSection)
        {
            // no overlap -> create no section
            if (!Sections.ContainsSection(otherSection.SectionName))
            {
                Sections.AddSection(otherSection.SectionName);
            }

            // merge section into the new one
            Sections.GetSectionData(otherSection.SectionName).Merge(otherSection);
        }
        private void MergeGlobal(KeyDataCollection globals)
        {
            foreach (var globalValue in globals)
            {
                Global[globalValue.KeyName] = globalValue.Value;
            }
        }
    }
}