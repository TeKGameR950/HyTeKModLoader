using System;
using System.Collections;
using System.Collections.Generic;

namespace IniParser.Model
{
    public class SectionDataCollection : ICloneable, IEnumerable<SectionData>
    {
        IEqualityComparer<string> _searchComparer;
        #region Initialization
        public SectionDataCollection()
            :this(EqualityComparer<string>.Default)
        {}
        public SectionDataCollection(IEqualityComparer<string> searchComparer)
        {
            _searchComparer = searchComparer;

            _sectionData = new Dictionary<string, SectionData>(_searchComparer);
        }
        public SectionDataCollection(SectionDataCollection ori, IEqualityComparer<string> searchComparer)
        {
            _searchComparer = searchComparer ?? EqualityComparer<string>.Default;
                
            _sectionData = new Dictionary<string, SectionData>(_searchComparer);
            foreach(var sectionData in ori)
            {
                _sectionData.Add(sectionData.SectionName, (SectionData)sectionData.Clone());
            };
        }

        #endregion

        #region Properties
        public int Count { get { return _sectionData.Count; } }
        public KeyDataCollection this[string sectionName]
        {
            get
            {
                if ( _sectionData.ContainsKey(sectionName) )
                    return _sectionData[sectionName].Keys;

                return null;
            }
        }

        #endregion

        #region Public Members
        public bool AddSection(string keyName)
        {
            //Checks valid section name
            //if ( !Assert.StringHasNoBlankSpaces(keyName) )
            //    throw new ArgumentException("Section name contain whitespaces");

            if ( !ContainsSection(keyName) )
            {
                _sectionData.Add( keyName, new SectionData(keyName, _searchComparer) );
                return true;
            }

            return false;
        }
        public void Add(SectionData data)
        {
            if (ContainsSection(data.SectionName))
            {
                SetSectionData(data.SectionName, new SectionData(data, _searchComparer));
            }
            else
            {
                _sectionData.Add(data.SectionName, new SectionData(data, _searchComparer));
            }
        }
        public void Clear()
        {
            _sectionData.Clear();
        }
        public bool ContainsSection(string keyName)
        {
            return _sectionData.ContainsKey(keyName);
        }
        public SectionData GetSectionData(string sectionName)
        {
            if (_sectionData.ContainsKey(sectionName))
                return _sectionData[sectionName];

            return null;
        }

        public void Merge(SectionDataCollection sectionsToMerge)
        {
            foreach(var sectionDataToMerge in sectionsToMerge)
            {
                var sectionDataInThis = GetSectionData(sectionDataToMerge.SectionName);

                if (sectionDataInThis == null)
                {
                    AddSection(sectionDataToMerge.SectionName);
                }

                this[sectionDataToMerge.SectionName].Merge(sectionDataToMerge.Keys);
            }
        }
        public void SetSectionData(string sectionName, SectionData data)
        {
            if ( data != null )
                _sectionData[sectionName] = data;
        }
        public bool RemoveSection(string keyName)
        {
            return _sectionData.Remove(keyName);
        }


        #endregion

        #region IEnumerable<SectionData> Members
        public IEnumerator<SectionData> GetEnumerator()
        {
            foreach (string sectionName in _sectionData.Keys)
                yield return _sectionData[sectionName];
        }

        #endregion

        #region IEnumerable Members
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region ICloneable Members
        public object Clone()
        {
            return new SectionDataCollection(this, _searchComparer);
        }

        #endregion

        #region Non-public Members
        private readonly Dictionary<string, SectionData> _sectionData;

        #endregion

    }
}