using System;
using System.Collections.Generic;

namespace IniParser.Model
{
    public class KeyData : ICloneable
    {
        #region Initialization
        public KeyData(string keyName)
        {
            if(string.IsNullOrEmpty(keyName))
                throw new ArgumentException("key name can not be empty");

            _comments = new List<string>();
            _value = string.Empty;
            _keyName = keyName;
        }
        public KeyData(KeyData ori)
        {
            _value = ori._value;
            _keyName = ori._keyName;
            _comments = new List<string>(ori._comments);
        }

        #endregion Constructors 

        #region Properties 
        public List<string> Comments
        {
            get { return _comments; }
            set { _comments = new List<string> (value) ; }
        }
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        public string KeyName
        {
            get
            {
                return _keyName;
            }

            set
            {
                if (value != string.Empty)
                    _keyName = value;
            }

        }

        #endregion Properties 

        #region ICloneable Members
        public object Clone()
        {
            return new KeyData(this);
        }

        #endregion

        #region Non-public Members

        // List with comment lines associated to this key 
        private List<string> _comments;

        // Unique value associated to this key
        private string _value;

        // Name of the current key
        private string _keyName;

        #endregion
    }
}
