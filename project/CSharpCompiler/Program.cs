﻿using CommandLine;
using ICSharpCode.SharpZipLib.Zip;
using ShellLink;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace csc
{
    enum ExitCode : int
    {
        InvalidArgs = -1,
        Success = 0,
        CompileFailed = 1,
        ParseError = 2,
        ReferenceError = 4
    }

    class Options
    {
        [Option('s', "source", Default = null, Required = true, HelpText = "file(s) path")]
        public IEnumerable<string> inputPath { get; set; }

        [Option('o', "output", Default = null, Required = true, HelpText = "path to save assembly")]
        public string outputPath { get; set; }

        [Option('r', "reference", Default = null, Required = false, HelpText = "reference assemblies path(s)")]
        public IEnumerable<string> referencePath { get; set; }

        [Option('n', "name", Default = "", Required = false, HelpText = "assembly name")]
        public string assemblyName { get; set; }

        [Option('c', "check", Default = "", Required = false, HelpText = "output string after compiling")]
        public string checkString { get; set; }

        [Option('p', "preload", Required = false, HelpText = "Call only to cache referenced assemblies")]
        public bool preload { get; set; }
    }

    class Program
    {
        static List<string[]> tasks = new List<string[]>();
        static bool taskInProgress = false;

#if GAME_IS_GREENHELL
        public static string folderName = "GreenHellModLoader";
        public static string exeName = "GH";
#elif GAME_IS_RAFT
        public static string folderName = "RaftModLoader";
        public static string exeName = "Raft";
#endif

        public static string appdataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), folderName);
        public static string logsFolder = Path.Combine(appdataFolder, "logs");
        public static string cacheFolder = Path.Combine(appdataFolder, "cache");
        public static string tempCacheFolder = Path.Combine(cacheFolder, "temp");
        static void Main()
        {
            InitParentExit();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(ErrorHandler);

            Console.SetIn(new StreamReader(Console.OpenStandardInput(8192)));
            string arguments = "";
            string[] args;

            HandleTasks();

            while (arguments != "exit")
            {
                arguments = Console.ReadLine();
                if (arguments != "exit")
                {
                    args = ParseArgs(arguments);
                    tasks.Add(args);
                }
            }
        }

        static void InitParentExit()
        {
            Process parent = ParentProcessUtilities.GetParentProcess();
            if (parent != null)
            {
                if (parent.ProcessName == exeName)
                {
                    parent.EnableRaisingEvents = true;
                    parent.Exited += Parent_Exited;
                    return;
                }
            }

            Process.GetCurrentProcess().Kill();
        }

        private static void Parent_Exited(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        static void ErrorHandler(object sender, UnhandledExceptionEventArgs args)
        {
#if GAME_IS_RAFT
            string modloaderFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RaftModLoader");
            string logfilePath = Path.Combine(modloaderFolder, "logs\\csc_errorhandler.log");
            File.AppendAllText(logfilePath, args.ExceptionObject.ToString() + "\n");
#elif GAME_IS_GREENHELL
            string modloaderFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GreenHellModLoader");
            string logfilePath = Path.Combine(modloaderFolder, "logs\\csc_errorhandler.log");
            File.AppendAllText(logfilePath, args.ExceptionObject.ToString() + "\n");
#endif
        }

        static async void HandleTasks()
        {
            if (tasks.Count > 0)
            {
                if (!taskInProgress)
                {
                    taskInProgress = true;
                    CompileTask(tasks.First());
                    tasks.RemoveAt(0);
                }
            }
            await Task.Delay(1);
            HandleTasks();
        }

        static string[] ParseArgs(string arguments)
        {
            List<string> args = new List<string>();
            foreach (var arg in Regex.Matches(arguments, "\"[^\"]*\"|[^ ]+"))
            {
                args.Add(arg.ToString().Trim(new char[] { '"' }));
            }
            return args.ToArray();
        }

        static async void CompileTask(string[] args)
        {
            Options options = new Options();
            int exitCode = (int)ExitCode.Success;
            string errorOutput = "";

            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed(o =>
                   {
                       options = o;

                   }).WithNotParsed((o) =>
                   {
                       Console.WriteLine("Args failed to parse");
                   });
            try
            {
                string filename = Path.GetFileName(options.inputPath.First());
                WriteLine($"{DateTime.Now.Second.ToString("D2")}:{DateTime.Now.Millisecond.ToString("D3")} Started compilation of \"" + filename + "\"");

                List<string> references = new List<string>();
                foreach (var referencePath in options.referencePath)
                {
                    if (Directory.Exists(referencePath) || File.Exists(referencePath))
                    {
                        if (referencePath.IsDll())
                            references.Add(referencePath);
                        else
                            references.AddRange(Directory.GetFiles(referencePath).Where(file => file.IsDll()));
                    }
                    else
                    {
                        if (options.checkString != "")
                        {
                            WriteLine($"Reference does not exist at path {referencePath}", options.checkString);
                        }
                    }
                }

                if (options.preload)
                {
                    Compiler.Cache(references);
                    return;
                }

                if (options.assemblyName == "")
                {
                    if (options.inputPath.Count() == 1)
                        options.assemblyName = Path.GetFileNameWithoutExtension(options.inputPath.First());
                    else
                        options.assemblyName = Path.GetRandomFileName();
                }

                List<byte[]> modReferences = new List<byte[]>();
                List<byte[]> code = new List<byte[]>();
                foreach (var codePath in options.inputPath)
                {
                    code.AddRange(GetScriptsFromFile(codePath));
                    modReferences.AddRange(GetScriptsFromFile(codePath, true));
                }
                WriteLine($"{DateTime.Now.Second.ToString("D2")}:{DateTime.Now.Millisecond.ToString("D3")} Compile called for \"" + filename + "\"");
                byte[] assembly = Compiler.Compile(code, references, modReferences, options.assemblyName, out errorOutput, out exitCode);
                WriteLine($"{DateTime.Now.Second.ToString("D2")}:{DateTime.Now.Millisecond.ToString("D3")} Compile done for \"" + filename + "\"");
                if (assembly.Length > 0)
                    File.WriteAllBytes(options.outputPath, assembly);
                if (!string.IsNullOrEmpty(errorOutput))
                {
                    WriteLine(errorOutput, options.checkString);
                }

                if (options.checkString != "")
                {
                    WriteLine(exitCode, options.checkString);
                }
            }
            catch(Exception e) { Console.WriteLine("An error occured when running a compile task ! ( "+e.Message+" ) Stacktrace : "+e.StackTrace.ToString()); }
            taskInProgress = false;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        static async void WriteLine(object o, string check = "")
        {
            Console.WriteLine(check + " " + Base64Encode(o.ToString()));
        }

        public static bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        static List<byte[]> GetScriptsFromFile(string file, bool dlls = false)
        {
            if (file.IsCs())
            {
                if (dlls) return new List<byte[]>();
                return GetScriptsFromCsFile(file);
            }
            else if (file.IsShortcut())
            {
                return GetScriptsFromFolder(file, dlls);
            }
            else
            {
                return GetScriptsFromModFile(file, dlls);
            }
        }

        static List<byte[]> GetScriptsFromCsFile(string file)
        {
            return new List<byte[]> { File.ReadAllBytes(file) };
        }

        static List<byte[]> GetScriptsFromFolder(string file, bool dlls = false)
        {
            List<byte[]> output = new List<byte[]>();

            string path = Shortcut.ReadFromFile(file).LinkTargetIDList.Path;
            if (!Directory.Exists(path)) { return null; }
            FileInfo[] files = GetFolderFiles(new DirectoryInfo(path));

            foreach (FileInfo entry in files.Where(f => (!dlls && f.FullName.IsCs()) || (dlls && f.FullName.IsDll())))
            {
                output.Add(File.ReadAllBytes(entry.FullName));
            }
            return output;
        }

        static FileInfo[] GetFolderFiles(DirectoryInfo dir)
        {
            List<FileInfo> files = new List<FileInfo>();
            files.AddRange(dir.GetFiles("*", SearchOption.TopDirectoryOnly));
            DirectoryInfo[] directories = dir.GetDirectories("*", SearchOption.AllDirectories);
            directories.ToList().ForEach(d =>
            {
                string parentname = d.FullName.Replace(dir.FullName + "\\", "");
                if (!parentname.StartsWith("bin") && !parentname.StartsWith("obj"))
                {
                    files.AddRange(d.GetFiles("*", SearchOption.AllDirectories));
                }
            });
            return files.ToArray();
        }

        /*static void AddReferencesFromFile(IEnumerable<string> path, List<string> references)
        {
            foreach (var codePath in path)
            {
                if (!codePath.IsCs())
                {
                    UnpackDllsFromModFile(codePath, references);
                }
            }
        }*/

        static List<byte[]> GetScriptsFromModFile(string file, bool dlls = false)
        {
            List<byte[]> output = new List<byte[]>();

            using (Stream stream = File.OpenRead(file))
            {
                using (var zipInputStream = new ZipInputStream(stream))
                {
                    while (zipInputStream.GetNextEntry() is ZipEntry v)
                    {
                        if (!v.IsFile)
                        {
                            continue;
                        }
                        bool validFile = dlls ? v.Name.EndsWith(".dll") : v.Name.EndsWith(".cs");
                        if (validFile)
                        {
                            var zipentry = v.Name;
                            StreamReader reader = new StreamReader(zipInputStream);
                            try
                            {
                                var bytes = default(byte[]);
                                using (var memstream = new MemoryStream())
                                {
                                    reader.BaseStream.CopyTo(memstream);
                                    bytes = memstream.ToArray();
                                }
                                output.Add(bytes);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("[Compiler] " + Path.GetFileNameWithoutExtension(file) + " > An error occured while loading the file " + zipentry + " !\nStacktrace : " + e.ToString());
                            }
                        }
                    }
                    return output;
                }
            }
        }

        static void UnpackDllsFromModFile(string file, List<string> references)
        {
            using (Stream stream = File.OpenRead(file))
            {
                using (var zipInputStream = new ZipInputStream(stream))
                {
                    while (zipInputStream.GetNextEntry() is ZipEntry v)
                    {
                        if (!v.IsFile)
                        {
                            continue;
                        }
                        if (v.Name.EndsWith(".dll"))
                        {
                            var zipentry = v.Name;
                            StreamReader reader = new StreamReader(zipInputStream);
                            try
                            {
                                var bytes = default(byte[]);
                                using (var memstream = new MemoryStream())
                                {
                                    reader.BaseStream.CopyTo(memstream);
                                    bytes = memstream.ToArray();
                                }
                                var path = Path.Combine(Path.GetTempPath(), "Raft Mod Loader", Path.GetRandomFileName() + ".dll");
                                FileInfo saveFile = new FileInfo(path);
                                saveFile.Directory.Create(); // If the directory already exists, this method does nothing.
                                File.WriteAllBytes(saveFile.FullName, bytes);
                                references.Add(saveFile.FullName);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("[Compiler] " + Path.GetFileNameWithoutExtension(file) + " > An error occured while loading the file " + zipentry + " !\nStacktrace : " + e.ToString());
                            }
                        }
                    }
                }
            }
        }

    }

    static class StringExtensions
    {
        public static bool IsDll(this string value)
        {
            return value.ToLower().EndsWith(".dll");
        }

        public static bool IsCs(this string value)
        {
            return value.ToLower().EndsWith(".cs");
        }

        public static bool IsShortcut(this string value)
        {
            return value.ToLower().EndsWith(".lnk");
        }
    }

    public struct ParentProcessUtilities
    {
        // These members must match PROCESS_BASIC_INFORMATION
        internal IntPtr Reserved1;
        internal IntPtr PebBaseAddress;
        internal IntPtr Reserved2_0;
        internal IntPtr Reserved2_1;
        internal IntPtr UniqueProcessId;
        internal IntPtr InheritedFromUniqueProcessId;

        [DllImport("ntdll.dll")]
        private static extern int NtQueryInformationProcess(IntPtr processHandle, int processInformationClass, ref ParentProcessUtilities processInformation, int processInformationLength, out int returnLength);

        /// <summary>
        /// Gets the parent process of the current process.
        /// </summary>
        /// <returns>An instance of the Process class.</returns>
        public static Process GetParentProcess()
        {
            return GetParentProcess(Process.GetCurrentProcess().Handle);
        }

        /// <summary>
        /// Gets the parent process of specified process.
        /// </summary>
        /// <param name="id">The process id.</param>
        /// <returns>An instance of the Process class.</returns>
        public static Process GetParentProcess(int id)
        {
            Process process = Process.GetProcessById(id);
            return GetParentProcess(process.Handle);
        }

        /// <summary>
        /// Gets the parent process of a specified process.
        /// </summary>
        /// <param name="handle">The process handle.</param>
        /// <returns>An instance of the Process class.</returns>
        public static Process GetParentProcess(IntPtr handle)
        {
            ParentProcessUtilities pbi = new ParentProcessUtilities();
            int returnLength;
            int status = NtQueryInformationProcess(handle, 0, ref pbi, Marshal.SizeOf(pbi), out returnLength);
            if (status != 0)
                throw new Win32Exception(status);

            try
            {
                return Process.GetProcessById(pbi.InheritedFromUniqueProcessId.ToInt32());
            }
            catch (ArgumentException)
            {
                // not found
                return null;
            }
        }
    }
}
