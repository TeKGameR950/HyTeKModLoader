﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace csc
{
    public class Compiler
    {
        public static byte[] Compile(List<byte[]> code, List<string> assemblies, List<byte[]> modReferences, string assemblyName, out string error, out int exitCode)
        {
            var cultureInfo = CultureInfo.GetCultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;

            exitCode = (int)ExitCode.Success;
            error = "";
            byte[] output = new byte[0];

            var parseOptions = new CSharpParseOptions(languageVersion: LanguageVersion.CSharp7_3);
            List<SyntaxTree> syntaxTree = new List<SyntaxTree>();
            try
            {
                code.ForEach(c => syntaxTree.Add(CSharpSyntaxTree.ParseText(Encoding.UTF8.GetString(c), options: parseOptions)));
            }
            catch (Exception e)
            {
                error += e + "\n";
                exitCode |= (int)ExitCode.ParseError;
            }

            List<MetadataReference> references = new List<MetadataReference>();
            foreach (var reference in assemblies)
            {
                try
                {
                    references.Add(MetadataReference.CreateFromFile(reference));
                }
                catch (Exception e)
                {
                    error += e + "\n";
                    exitCode |= (int)ExitCode.ReferenceError;
                }
            }
            foreach(var reference in modReferences)
            {
                try
                {
                    references.Add(MetadataReference.CreateFromImage(reference));
                }
                catch (Exception e)
                {
                    error += "RefError:"+e + "\n";
                    exitCode |= (int)ExitCode.ReferenceError;
                }
            }

            CSharpCompilation compilation = CSharpCompilation.Create(
            assemblyName,
            syntaxTrees: syntaxTree,
            references: references,
            options: new CSharpCompilationOptions(
                OutputKind.DynamicallyLinkedLibrary,
                optimizationLevel: OptimizationLevel.Debug,
                allowUnsafe: true));

            if (compilation == null)
            {
                exitCode |= (int)ExitCode.CompileFailed;
                return output;
            }

            using (var ms = new MemoryStream())
            {
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (Diagnostic diagnostic in failures)
                    {
                        error += diagnostic.ToString() + "\n";
                    }
                    error = error.TrimEnd('\n');
                    exitCode |= (int)ExitCode.CompileFailed;
                }
                else
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    output = ms.ToArray();
                }
            }
            return output;
        }

        public static void Cache(List<string> assemblies)
        {
            string codeToCompile = @"
                using System;
                namespace RoslynCompileSample
                {
                    public class Writer
                    {
                        public void Write(string message)
                        {
                            Console.WriteLine(1);
                        }
                    }
                }";

            List<MetadataReference> references = new List<MetadataReference>();
            foreach (var reference in assemblies)
            {
                try
                {
                    references.Add(MetadataReference.CreateFromFile(reference));
                }
                catch { }
            }

            Console.WriteLine(codeToCompile);

            CSharpCompilation compilation = null;
            compilation = CSharpCompilation.Create(
            Path.GetRandomFileName(),
            syntaxTrees: new List<SyntaxTree> { CSharpSyntaxTree.ParseText(codeToCompile) },
            references: references,
            options: new CSharpCompilationOptions(
                OutputKind.DynamicallyLinkedLibrary,
                optimizationLevel: OptimizationLevel.Release,
                allowUnsafe: true));

            try
            {
                using (var ms = new MemoryStream())
                {
                    EmitResult result = compilation.Emit(ms);

                    if (!result.Success)
                    {
                        IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                            diagnostic.IsWarningAsError ||
                            diagnostic.Severity == DiagnosticSeverity.Error);

                        foreach (Diagnostic diagnostic in failures)
                        {
                            Console.WriteLine(String.Format("\t{0}: {1}\n", diagnostic.Id, diagnostic.GetMessage()));
                        }
                    }
                    else
                    {
                        ms.Seek(0, SeekOrigin.Begin);
                        Console.WriteLine(ms.ToArray().Length);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Done");
        }
    }
}