﻿using System;
using System.Reflection;

[assembly: AssemblyVersion("0.0.0.1")]
[assembly: AssemblyTitle("Green Hell Mod Loader")]
[assembly: AssemblyDescription("Green Hell Mod Loader modifies parts of the code of Green Hell so that you can easily play with mods.")]
[assembly: AssemblyCompany("HyTeKGames")]
[assembly: AssemblyProduct("GHML")]
[assembly: AssemblyCopyright("Copyright greenhellmodding.com 2020")]

