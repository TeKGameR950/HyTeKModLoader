<?xml version="1.0"?>
<doc>
    <assembly>
        <name>AssemblyLoader</name>
    </assembly>
    <members>
        <member name="M:HMLLibrary.Mod.CanUnload(System.String@)">
            <summary>
            Allows mods to determine if they can be unloaded at the said moment.
            </summary>
            <param name="message">A message indicating the reason if the mod cannot be unloaded.</param>
            <example>
            <code>
            bool stillLoading = true;
            public override bool CanUnload(ref string message)
            {
                if (stillLoading)
                {
                    message = "The mod is still loading";
                    return false;
                }
                return base.CanUnload(ref message);
            }
            </code>
            </example>
            <method>public virtual bool CanUnload(ref string message)</method>
        </member>
        <member name="M:HMLLibrary.Mod.UnloadMod">
            <summary>
            Unloads the mod.
            </summary>
            <method>public virtual void UnloadMod()</method>
        </member>
        <member name="M:HMLLibrary.Mod.GetEmbeddedFileBytes(System.String)">
            <summary>
            Gets the bytes of an embedded file in the mod.
            </summary>
            <param name="path">The path to the embedded file.</param>
            <returns>The bytes of the embedded file, or null if the file doesn't exist.</returns>
            <example>
            <code>
            byte[] myBundleBytes = GetEmbeddedFileBytes("mybundle.assets");
            AssetBundle bundle = AssetBundle.LoadFromMemory(myBundleBytes);
            </code>
            </example>
            <method>public virtual byte[] GetEmbeddedFileBytes(string path)</method>
        </member>
        <member name="M:HMLLibrary.Mod.GetModInfo">
            <summary>
            Gets the mod information.
            </summary>
            <returns>The mod information from the modinfo.json file.</returns>
            <example>
            <code>
            Debug.Log("Mod version: "+GetModInfo().version);
            </code>
            </example>
            <method>public JsonModInfo GetModInfo()</method>
        </member>
        <member name="M:HMLLibrary.Mod.Log(System.Object)">
            <summary>
            Logs a message with the mod name as a prefix.
            </summary>
            <param name="message">The message to be logged.</param>
            <example>
            <code>
            Log("This is a message"); // Outputs: "[ModName] This is a message".
            </code>
            </example>
            <method>public void Log(object message)</method>
        </member>
        <member name="M:HMLLibrary.Mod.WorldEvent_WorldLoaded">
            <summary>
            The WorldEvent_WorldLoaded event triggers on world load complete.
            </summary>
            <example>
            <code>
            public override void WorldEvent_WorldLoaded()
            {
                Debug.Log("The world has loaded");
            }
            </code>
            </example>
            <method>public virtual void WorldEvent_WorldLoaded()</method>
        </member>
        <member name="M:HMLLibrary.Mod.WorldEvent_WorldSaved">
            <summary>
            The WorldEvent_WorldSaved event triggers on world save.
            </summary>
            <example>
            <code>
            public override void WorldEvent_WorldSaved()
            {
                Debug.Log("The world has been saved");
            }
            </code>
            </example>
            <method>public virtual void WorldEvent_WorldSaved()</method>
        </member>
        <member name="M:HMLLibrary.Mod.LocalPlayerEvent_Hurt(System.Single,UnityEngine.Vector3,UnityEngine.Vector3,EntityType)">
            <summary>
            The LocalPlayerEvent_Hurt event triggers when the local player takes damage.
            </summary>
            <param name="damage">The amount of damage taken.</param>
            <param name="hitPoint">The point where the damage was dealt.</param>
            <param name="hitNormal">The normal vector at the point of impact.</param>
            <param name="damageInflictorEntityType">The entity type causing the damage.</param>
            <example>
            <code>
            public override void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)
            {
                Debug.Log("Player hurt: " + damage + " damage taken");
            }
            </code>
            </example>
            <method>public virtual void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)</method>
        </member>
        <member name="M:HMLLibrary.Mod.LocalPlayerEvent_Death(UnityEngine.Vector3)">
            <summary>
            The LocalPlayerEvent_Death event triggers when the local player dies.
            </summary>
            <param name="deathPosition">The position where the player died.</param>
            <example>
            <code>
            public override void LocalPlayerEvent_Death(Vector3 deathPosition)
            {
                Debug.Log("Player died at: " + deathPosition);
            }
            </code>
            </example>
            <method>public virtual void LocalPlayerEvent_Death(Vector3 deathPosition)</method>
        </member>
        <member name="M:HMLLibrary.Mod.LocalPlayerEvent_Respawn">
            <summary>
            The LocalPlayerEvent_Respawn event triggers when the local player respawns.
            </summary>
            <example>
            <code>
            public override void LocalPlayerEvent_Respawn()
            {
                Debug.Log("Player respawned");
            }
            </code>
            </example>
            <method>public virtual void LocalPlayerEvent_Respawn()</method>
        </member>
        <member name="M:HMLLibrary.Mod.LocalPlayerEvent_ItemCrafted(Item_Base)">
            <summary>
            The LocalPlayerEvent_ItemCrafted event triggers when the local player crafts an item.
            </summary>
            <param name="item">The crafted item.</param>
            <example>
            <code>
            public override void LocalPlayerEvent_ItemCrafted(Item_Base item)
            {
                Debug.Log("Item crafted: " + item.UniqueName);
            }
            </code>
            </example>
            <method>public virtual void LocalPlayerEvent_ItemCrafted(Item_Base item)</method>
        </member>
        <member name="M:HMLLibrary.Mod.LocalPlayerEvent_PickupItem(PickupItem)">
            <summary>
            The LocalPlayerEvent_PickupItem event triggers when the local player picks up a dropped item.
            </summary>
            <param name="item">The picked up item.</param>
            <example>
            <code>
            public override void LocalPlayerEvent_PickupItem(PickupItem item)
            {
                Debug.Log("Picked up item: " + item.itemInstance?.UniqueName);
            }
            </code>
            </example>
            <method>public virtual void LocalPlayerEvent_PickupItem(PickupItem item)</method>
        </member>
        <member name="M:HMLLibrary.Mod.LocalPlayerEvent_DropItem(ItemInstance,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)">
            <summary>
            The LocalPlayerEvent_DropItem event triggers when the local player drops an item.
            </summary>
            <param name="item">The dropped item.</param>
            <param name="position">The position where the item was dropped.</param>
            <param name="direction">The direction in which the item was dropped.</param>
            <param name="parentedToRaft">Specifies if the item is parented to the raft.</param>
            <example>
            <code>
            public override void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)
            {
                Debug.Log("Dropped item: " + item.UniqueName + " at position: " + position);
            }
            </code>
            </example>
            <method>public virtual void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)</method>
        </member>
        <member name="M:HMLLibrary.Mod.WorldEvent_OnPlayerConnected(Steamworks.CSteamID,RGD_Settings_Character)">
            <summary>
            The WorldEvent_OnPlayerConnected event triggers when a player connects to the world.
            </summary>
            <param name="steamid">The SteamID of the connected player.</param>
            <param name="characterSettings">The character settings of the connected player.</param>
            <example>
            <code>
            public override void WorldEvent_OnPlayerConnected(CSteamID steamid, RGD_Settings_Character characterSettings)
            {
                Debug.Log("Player connected: " + steamid);
            }
            </code>
            </example>
            <method>public virtual void WorldEvent_OnPlayerConnected(CSteamID steamid, RGD_Settings_Character characterSettings)</method>
        </member>
        <member name="M:HMLLibrary.Mod.WorldEvent_OnPlayerDisconnected(Steamworks.CSteamID,DisconnectReason)">
            <summary>
            The WorldEvent_OnPlayerDisconnected event triggers when a player disconnects from the world.
            </summary>
            <param name="steamid">The SteamID of the disconnected player.</param>
            <param name="disconnectReason">The reason for disconnection.</param>
            <example>
            <code>
            public override void WorldEvent_OnPlayerDisconnected(CSteamID steamid, DisconnectReason disconnectReason)
            {
                Debug.Log("Player disconnected: " + steamid + " Reason: " + disconnectReason);
            }
            </code>
            </example>
            <method>public virtual void WorldEvent_OnPlayerDisconnected(CSteamID steamid, DisconnectReason disconnectReason)</method>
        </member>
        <member name="M:HMLLibrary.Mod.WorldEvent_WorldUnloaded">
            <summary>
            The WorldEvent_WorldUnloaded event triggers on world unload.
            </summary>
            <example>
            <code>
            public override void WorldEvent_WorldUnloaded()
            {
                Debug.Log("World unloaded");
            }
            </code>
            </example>
            <method>public virtual void WorldEvent_WorldUnloaded()</method>
        </member>
        <member name="M:HMLLibrary.Mod.ModEvent_OnModLoaded(HMLLibrary.Mod)">
            <summary>
            The ModEvent_OnModLoaded event triggers when a mod is loaded.
            </summary>
            <param name="mod">The loaded mod.</param>
            <example>
            <code>
            public override void ModEvent_OnModLoaded(Mod mod)
            {
                Debug.Log("Mod loaded: " + mod.name);
            }
            </code>
            </example>
            <method>public virtual void ModEvent_OnModLoaded(Mod mod)</method>
        </member>
        <member name="M:HMLLibrary.Mod.ModEvent_OnModUnloaded(HMLLibrary.Mod)">
            <summary>
            The ModEvent_OnModUnloaded event triggers when a mod is unloaded.
            </summary>
            <param name="mod">The unloaded mod.</param>
            <example>
            <code>
            public override void ModEvent_OnModUnloaded(Mod mod)
            {
                Debug.Log("Mod unloaded: " + mod.name);
            }
            </code>
            </example>
            <method>public virtual void ModEvent_OnModUnloaded(Mod mod)</method>
        </member>
        <member name="M:HMLLibrary.UnityMainThreadDispatcher.Enqueue(System.Collections.IEnumerator)">
            <summary>
            Locks the queue and adds the IEnumerator to the queue
            </summary>
            <param name="action">IEnumerator function that will be executed from the main thread.</param>
        </member>
        <member name="M:HMLLibrary.UnityMainThreadDispatcher.Enqueue(System.Action)">
            <summary>
            Locks the queue and adds the Action to the queue
            </summary>
            <param name="action">function that will be executed from the main thread.</param>
        </member>
    </members>
</doc>
