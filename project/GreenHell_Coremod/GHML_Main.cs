﻿using AssemblyLoader;
using GHML;
using HarmonyLib;
using HMLLibrary;
using System;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class GHML_Main : MonoBehaviour
{
    public static KeyCode MenuKey = KeyCode.F9;
    public static KeyCode ConsoleKey = KeyCode.F10;
    public static TcpClient tcpclient;
    public static LauncherConfiguration modloader_config = null;
    public static GHML_Main instance;

    async void Start()
    {
        instance = this;
        Application.runInBackground = true;
        Application.backgroundLoadingPriority = ThreadPriority.Low;

        HLib.CanUnloadMod += CanUnloadMod;
        HLib.CanLoadMod += CanLoadMod;

        modloader_config = HLoader.config;
        if (modloader_config.skipSplashScreen)
        {
            MainMenu mainmenu = FindObjectOfType<MainMenu>();
            Traverse.Create(mainmenu).Field("m_FadeInDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_FadeOutDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_FadeOutSceneDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_CompanyLogoDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_GameLogoDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_BlackScreenDuration").SetValue(0f);
        }

        foreach (string file in Directory.GetFiles(HLib.path_cacheFolder_temp, "*", SearchOption.AllDirectories))
        {
            if (File.Exists(file) && file.ToLower().EndsWith(".cs") || file.ToLower().EndsWith(".dll"))
                File.Delete(file);
        }

        Application.backgroundLoadingPriority = ThreadPriority.High;
        Directory.CreateDirectory(HLib.path_dataFolder);
        Directory.CreateDirectory(HLib.path_binariesFolder);
        Directory.CreateDirectory(HLib.path_logsFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder_mods);
        Directory.CreateDirectory(HLib.path_cacheFolder_textures);
        Directory.CreateDirectory(HLib.path_cacheFolder_temp);
        Directory.CreateDirectory(HLib.path_modsFolder);
        Directory.SetCurrentDirectory(Path.Combine(Application.dataPath, "..\\"));
        HLib.bundle = await HUtils.TaskLoadAssetBundleFromMemoryAsync(File.ReadAllBytes(Path.Combine(HLib.path_dataFolder, "ghml.assets")));
        gameObject.AddComponent<GHConsole>();
        gameObject.AddComponent<RawSharp>();
        gameObject.AddComponent<UnityMainThreadDispatcher>();
        while (!GameObject.Find("GHConsole")) { await Task.Delay(100); }
        try
        {
            var harmony = new Harmony("hytekgames.greenhellmodloader");
            harmony.PatchAll();
            Debug.Log("Successfully Loaded Default Harmony Patches!");
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        while (!GreenHellGame.Instance.m_InitialSequenceComplete) { await Task.Delay(10); }
        Traverse.Create(FindObjectOfType<MainMenu>()).Field("m_StateStartTime").SetValue(Time.time - 3f);
        await Task.Delay(1000);
        InitializeModLoader();
    }

    public bool CanUnloadMod(string name, string version)
    {
        return true;
    }

    public bool CanLoadMod(ModData data)
    {
        return true;
    }

    public async void InitializeModLoader()
    {
        GameObject mainmenu = Instantiate(await HLib.bundle.TaskLoadAssetAsync<GameObject>("MainMenu"), gameObject.transform);
        mainmenu.AddComponent<GHMainMenu>().Initialize();

        HLib.missingTexture = (await HLib.bundle.TaskLoadAssetAsync<Sprite>("missing")).texture;

        GameObject ghnotify = Instantiate(await HLib.bundle.TaskLoadAssetAsync<GameObject>("NotificationSystem"), gameObject.transform);
        ghnotify.AddComponent<HNotify>();

        ghnotify.AddComponent<CustomLoadingScreen>();

        try
        {
            tcpclient = new TcpClient();
            await tcpclient.ConnectAsync("62.210.119.159", 666);
        }
        catch { }
        RuntimeTCPChecker();
    }

    public async void RuntimeTCPChecker()
    {
        bool connected = true;
        try
        {
            NetworkStream netStream = tcpclient.GetStream();
            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes("Is anybody there?");
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }
            else
            {
                tcpclient.Close();
                netStream.Close();
                connected = false;
            }
        }
        catch
        {
            connected = false;
        }

        if (!connected)
        {
            try
            {
                tcpclient = new TcpClient();
                await tcpclient.ConnectAsync("62.210.119.159", 666);
            }
            catch { }
        }

        await Task.Delay(5000);
        RuntimeTCPChecker();
    }
}