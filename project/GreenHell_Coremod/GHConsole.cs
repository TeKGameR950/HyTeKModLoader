﻿using GHML;
using HarmonyLib;
using HMLLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class GHConsole : HConsole
{
    public static bool isOpen;
    private static List<GameObject> logs = new List<GameObject>();
    private static LatestLog latestlog;
    private List<string> lastCommands = new List<String>();
    private static Dictionary<string, CommandEntry> commandMap = new Dictionary<string, CommandEntry>();
    private CanvasGroup canvasgroup;
    private static Transform viewportContent;
    private GameObject lineEntryPrefab;
    private List<string> blockedWords = new List<string>()
    {
        "opt-out",
        "jobtempalloc",
        "tla_debug_stack_leak",
        "sendwillrendercanvases",
        "missing default terrain shader.",
        "your current multi-scene setup has inconsistent lighting",
        "\"setdestination\" can only be called on an active agent that has been placed on a navmesh.",
        "duplicate basemap name: '_maintex'. ignoring.",
        "is registered with more than one lodgroup",
        "the minimum cubemap resolution is 16. the reflection probe",
        "upgrading font asset [",
        "upgrading sprite asset [",
        "screen position out of view frustum",
        " is registered with more than one lodgroup ('",
        "kinematic body only supports",
        "[achivements] setstat - failed,",
        "can't load anim evets script -",
        "can't load animator data script -",
        "[scriptparser::parse] cannot load resource ",
        "[scriptparser::parse] loaded file does not contain any data - ",
        "[dialogsmanager:start] error - duplicated clip names",
        "loading with thread priority low and op priority",
        "ondeactivategroup - inactive group!",
        "life place not selected for actor:",
        "savegameinfo::readsavefile "
    };
    private ScrollRect scrollRect;
    private static InputField inputfield;
    private GameObject autocomplete;
    private Button SendButton;
    private Button BiggifyInputfieldButton;
    private delegate string commandAction(params string[] args);
    private delegate string simpleCommandAction();
    private delegate void silentCommandAction(string[] args);
    private delegate void simpleSilentCommandAction();
    private static bool isInputfieldBig = false;
    private static GameObject CurrentHoveredLine;
    private GameObject selectedLine;
    private int PreviousCommandId = -1;
    private GameObject RightClickMenu;

    private async void Start()
    {
        HConsole.instance = this;
        GameObject gameobject = Instantiate(await HLib.bundle.TaskLoadAssetAsync<GameObject>("ConsoleCanvas"), transform);
        lineEntryPrefab = await HLib.bundle.TaskLoadAssetAsync<GameObject>("ConsoleLinePrefab");
        inputfield = gameobject.transform.Find("Background").Find("InputField").GetComponent<InputField>();
        autocomplete = gameobject.transform.Find("Background").Find("InputField").Find("Autocomplete").gameObject;
        SendButton = gameobject.transform.Find("Background").Find("Send").GetComponent<Button>();
        RightClickMenu = gameobject.transform.Find("Background").Find("Scroll View").Find("RightClickMenu").gameObject;
        gameobject.transform.Find("Background").Find("Scroll View").gameObject.AddComponent<ConsoleRightClickHandler>();
        RightClickMenu.transform.Find("Background").Find("Copy").GetComponent<Button>().onClick.AddListener(() =>
        {
            if (selectedLine != null)
            {
                selectedLine.GetComponent<Text>().text.CopyToClipboard();
                Debug.Log("<color=#298f2e>Log succcessfully copied to clipboard!</color>");
            }
            RightClickMenu.SetActive(false);
            selectedLine = null;
        });
        RightClickMenu.transform.Find("Background").Find("Remove").GetComponent<Button>().onClick.AddListener(() =>
        {
            if (logs.Contains(selectedLine))
            {
                logs.Remove(selectedLine);
                Destroy(selectedLine);
            }
            RightClickMenu.SetActive(false);
            selectedLine = null;
        });
        RightClickMenu.transform.Find("Background").Find("Clear Console").GetComponent<Button>().onClick.AddListener(() =>
        {
            RightClickMenu.SetActive(false);
            selectedLine = null;
            DefaultConsoleCommands.clear();
        });
        SendButton.onClick.AddListener(() =>
        {
            string t = SilentlyRunCommand(inputfield.text);
            if (!string.IsNullOrWhiteSpace(t))
            {
                Debug.Log(t);
            }
        });
        BiggifyInputfieldButton = gameobject.transform.Find("Background").Find("ToggleBiggerInput").GetComponent<Button>();
        BiggifyInputfieldButton.onClick.AddListener(() =>
        {
            isInputfieldBig = !isInputfieldBig;
            if (isInputfieldBig)
            {
                inputfield.textComponent.alignment = TextAnchor.UpperLeft;
                canvasgroup.GetComponent<Animation>().Play("BiggerInputfield");
            }
            else
            {
                inputfield.textComponent.alignment = TextAnchor.MiddleLeft;
                canvasgroup.GetComponent<Animation>().Play("NormalInputfield");
            }
        });
        inputfield.onEndEdit.AddListener(val =>
        {
            if (isInputfieldBig) { return; }
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                string t = SilentlyRunCommand(inputfield.text);
                if (!string.IsNullOrWhiteSpace(t))
                {
                    Debug.Log(t);
                }
            }
        });
        inputfield.onValueChanged.AddListener(val =>
        {
            OnCommandInputChange();
        });
        scrollRect = gameobject.transform.Find("Background").Find("Scroll View").GetComponent<ScrollRect>();
        viewportContent = gameobject.transform.Find("Background").Find("Scroll View").Find("Viewport").Find("Content");
        gameobject.name = "GHConsole";
        canvasgroup = gameobject.GetComponent<CanvasGroup>();
        canvasgroup.alpha = 0;
        canvasgroup.blocksRaycasts = false;
        canvasgroup.interactable = false;
        RefreshCommands();
        Application.logMessageReceived += HandleUnityLog;

        DefaultConsoleCommands.LoadBoundCommands();
    }

    public static void ClearConsole()
    {
        logs.Clear();
        foreach (Transform child in viewportContent)
        {
            Destroy(child.gameObject);
        }
    }

    private IEnumerator AutocompleteSetCaret()
    {
        yield return new WaitForEndOfFrame();
        inputfield.MoveTextEnd(true);
        yield return new WaitForEndOfFrame();
        inputfield.caretPosition = inputfield.text.Length;
        inputfield.ForceLabelUpdate();
    }

    private string SilentlyRunCommand(string commandString)
    {
        if (string.IsNullOrWhiteSpace(commandString))
        {
            inputfield.Select();
            inputfield.ActivateInputField();
            return "";
        }
        Debug.Log(" > " + commandString);
        lastCommands.Add(commandString);
        PreviousCommandId = -1;
        inputfield.text = String.Empty;
        string[] splitCommand = commandString.Split(' ');
        string commandName = splitCommand[0];
        CommandEntry command = null;
        if (commandMap.TryGetValue(commandName, out command))
        {
            try
            {
                inputfield.Select();
                inputfield.ActivateInputField();
                return command.action(splitCommand.Skip(1).ToArray());
            }
            catch (Exception e)
            {
                inputfield.Select();
                inputfield.ActivateInputField();
                return e.Message;
            }
        }
        else
        {
            Debug.LogWarning("Unknown command! Type help for help.");
            inputfield.Select();
            inputfield.ActivateInputField();
            return "";
        }
    }

    private static string Help(string[] options)
    {
        if (options.Length == 0)
        {
            string result = "Available commands:";
            string[] commands = commandMap.Keys.ToArray();
            Array.Sort(commands);
            int maxCommandLength = commands.Select(x => x.Length).Max();
            foreach (string c in commands)
            {
                result += "\n <b>" + c + "</b> - <i>" + commandMap[c].docs + "</i>";
            }
            return result;
        }

        CommandEntry command = null;
        if (commandMap.TryGetValue(options[0], out command))
        {
            return command.docs;
        }

        return "Command not found: " + options[0];
    }

    public override async void RefreshCommands()
    {
        commandMap.Clear();
        commandMap["help"] = new CommandEntry() { docs = "View available commands as well as their documentation.", action = Help };
        GetCommandsInAssembly(Assembly.GetAssembly(typeof(GHConsole)));
        foreach (ModData md in ModManagerPage.modList.Where(t => t.modinfo.modState == ModInfo.ModStateEnum.running))
        {
            GetCommandsInAssembly(md.modinfo.assembly);
        }
    }

    private static void GetCommandsInAssembly(Assembly assembly)
    {
        foreach (Type type in assembly.GetTypes())
        {
            foreach (MethodInfo method in type.GetMethods(BindingFlags.Public | BindingFlags.Static))
            {
                ConsoleCommand[] attrs = method.GetCustomAttributes(typeof(ConsoleCommand), true) as ConsoleCommand[];
                if (attrs.Length == 0)
                    continue;

                commandAction action = Delegate.CreateDelegate(typeof(commandAction), method, false) as commandAction;
                if (action == null)
                {
                    simpleCommandAction simpleAction = Delegate.CreateDelegate(typeof(simpleCommandAction), method, false) as simpleCommandAction;
                    if (simpleAction != null)
                    {
                        action = _ => simpleAction();
                    }
                    else
                    {
                        silentCommandAction silentAction = Delegate.CreateDelegate(typeof(silentCommandAction), method, false) as silentCommandAction;
                        if (silentAction != null)
                        {
                            action = args => { silentAction(args); return ""; };
                        }
                        else
                        {
                            simpleSilentCommandAction simpleSilentAction = Delegate.CreateDelegate(typeof(simpleSilentCommandAction), method, false) as simpleSilentCommandAction;
                            action = args => { simpleSilentAction(); return ""; };
                        }
                    }
                }

                if (action == null)
                {
                    Debug.LogError(string.Format(
                        "Method {0}.{1} is the wrong type for a console command! It must take either no argumets, or just an array " +
                        "of strings, and its return type must be string or void.", type, method.Name));
                    continue;
                }

                foreach (ConsoleCommand cmd in attrs)
                {
                    if (string.IsNullOrEmpty(cmd.commandName))
                    {
                        cmd.commandName = method.Name;
                    }
                    if (!commandMap.ContainsKey(cmd.commandName))
                    {
                        commandMap[cmd.commandName] = new CommandEntry() { docs = cmd.docstring ?? "", action = action };
                    }
                }
            }
        }
    }

    private void OnCommandInputChange()
    {
        if (!isOpen)
        {
            inputfield.text = "";
            inputfield.DeactivateInputField();
            return;
        }
        string command = inputfield.text.ToLower();

        if (command != "")
        {
            bool found = false;
            foreach (KeyValuePair<string, CommandEntry> c in commandMap)
            {
                if (c.Key.ToLower().StartsWith(command))
                {
                    if (c.Key.ToLower() == command)
                    {
                        autocomplete.transform.GetChild(0).GetComponent<Text>().text = "";
                        autocomplete.SetActive(false);
                        return;
                    }
                    found = true;
                    autocomplete.SetActive(true);
                    autocomplete.transform.GetChild(0).GetComponent<Text>().text = c.Key;
                    return;
                }
            }
            if (!found)
            {
                autocomplete.transform.GetChild(0).GetComponent<Text>().text = "";
                autocomplete.SetActive(false);
            }
        }
        else
        {
            autocomplete.transform.GetChild(0).GetComponent<Text>().text = "";
            autocomplete.SetActive(false);
        }
    }

    private IEnumerator CloseRightClickMenuLate()
    {
        yield return new WaitForEndOfFrame();
        RightClickMenu.SetActive(false);
        selectedLine = null;
    }

    private void LateUpdate()
    {
        if (!CursorManager.Get().IsCursorVisible() && !isOpen)
        {
            foreach (var boundCommand in DefaultConsoleCommands.boundCommands)
            {
                if (Input.GetKeyDown(boundCommand.Key))
                {
                    string t = SilentlyRunCommand(boundCommand.Value);
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        Debug.Log(t);
                    }
                }
            }
        }


        if (isOpen && inputfield.isFocused)
        {
            if (lastCommands.Count() > 0 && !isInputfieldBig)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (PreviousCommandId == -1) { PreviousCommandId = lastCommands.Count(); }
                    if (PreviousCommandId > 0)
                    {
                        inputfield.text = lastCommands[PreviousCommandId - 1];
                        StartCoroutine(AutocompleteSetCaret());
                        PreviousCommandId--;
                    }
                    else
                    {
                        StartCoroutine(AutocompleteSetCaret());
                    }
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (PreviousCommandId >= 0 && PreviousCommandId < lastCommands.Count() - 1)
                    {
                        PreviousCommandId++;
                        inputfield.text = lastCommands[PreviousCommandId];
                        StartCoroutine(AutocompleteSetCaret());
                    }
                    else
                    {
                        PreviousCommandId = -1;
                        inputfield.text = "";
                    }
                }
            }
        }

        if (Input.GetKeyDown(GHML_Main.ConsoleKey))
        {
            isOpen = !isOpen;
            if (isOpen)
            {
                if (!GHMainMenu.IsOpen)
                {
                    InternalGHAPI.GHInternal_ShowCursor(true);
                }
                canvasgroup.GetComponent<Animation>().Play("ConsoleOpen");
                isInputfieldBig = false;
                inputfield.textComponent.alignment = TextAnchor.MiddleLeft;
                inputfield.lineType = InputField.LineType.SingleLine;
                StartCoroutine(EnableInputfieldLate());
            }
            else
            {
                if (!GHMainMenu.IsOpen)
                {
                    InternalGHAPI.GHInternal_ShowCursor(false);
                }
                canvasgroup.GetComponent<Animation>().Play("ConsoleClose");
                RightClickMenu.SetActive(false);
                selectedLine = null;
                inputfield.OnDeselect(null);
                inputfield.DeactivateInputField();
            }
        }

        if (isOpen)
        {
            if (Input.GetMouseButtonDown(1))
            {
                if (CurrentHoveredLine != null)
                {
                    if (CurrentHoveredLine == scrollRect.gameObject)
                    {
                        RightClickMenu.SetActive(true);
                        RightClickMenu.transform.position = Input.mousePosition;
                        RightClickMenu.GetComponent<Animation>().Play("RightClickOpening_Console");
                    }
                    else
                    {
                        selectedLine = CurrentHoveredLine;
                        RightClickMenu.SetActive(true);
                        RightClickMenu.transform.position = Input.mousePosition;
                        RightClickMenu.GetComponent<Animation>().Play("RightClickOpening_Line");
                    }
                }
                else
                {
                    RightClickMenu.SetActive(false);
                    selectedLine = null;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (RightClickMenu.activeSelf)
                    StartCoroutine(CloseRightClickMenuLate());
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                isOpen = false;
                if (!GHMainMenu.IsOpen)
                {
                    InternalGHAPI.GHInternal_ShowCursor(false);
                }
                RightClickMenu.SetActive(false);
                selectedLine = null;
                canvasgroup.GetComponent<Animation>().Play("ConsoleClose");
                inputfield.OnDeselect(null);
                inputfield.DeactivateInputField();
            }
            if (autocomplete.transform.GetChild(0).GetComponent<Text>().text == "")
            {
                autocomplete.SetActive(false);
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    inputfield.text = autocomplete.transform.GetChild(0).GetComponent<Text>().text;
                    StartCoroutine(AutocompleteSetCaret());
                }
            }
        }
    }

    private IEnumerator EnableInputfieldLate()
    {
        yield return new WaitForEndOfFrame();
        inputfield.Select();
        inputfield.ActivateInputField();
    }

    private void HandleUnityLog(string logString, string stackTrace, LogType type)
    {
        if (logs.Count >= 100)
        {
            Destroy(logs.First());
            logs.RemoveAt(0);
        }
        foreach (string s in blockedWords)
        {
            if (logString.ToLower().Contains(s) || stackTrace.ToLower().Contains(s))
                return;
        }
        if (!string.IsNullOrEmpty(stackTrace))
        {
            logString += "\n" + stackTrace.TrimEnd('\n');
        }
        if (latestlog != null && latestlog.t == type && latestlog.l == logString)
        {
            Text t = latestlog.g.transform.GetChild(1).GetChild(0).GetComponent<Text>();
            latestlog.amount++;
            t.text = (latestlog.amount < 100) ? "x" + latestlog.amount : "+99";
            t.transform.parent.gameObject.SetActive(true);
            return;
        }

        GameObject line = Instantiate(lineEntryPrefab, viewportContent);
        line.transform.GetChild(1).gameObject.SetActive(false);
        ConsoleTooltipHandler hover = line.AddComponent<ConsoleTooltipHandler>();
        hover.tooltip = line.transform.GetChild(0).gameObject;
        Text text = line.GetComponent<Text>();
        switch (type)
        {
            case LogType.Warning:
                text.color = new Color(0.9098f, 0.6235f, 0.2509f);
                break;
            case LogType.Error:
            case LogType.Assert:
            case LogType.Exception:
                if (!isOpen && HNotify.errornotification != null)
                    HNotify.errornotification.AddNewError();
                text.color = new Color(0.7372f, 0.1451f, 0.1451f);
                break;
        }
        text.text = logString;
        logs.Add(line);
        latestlog = new LatestLog(line, logString, type);
        StartCoroutine(ScrollToBottom());
    }

    private IEnumerator ScrollToBottom()
    {
        yield return new WaitForEndOfFrame();
        scrollRect.verticalNormalizedPosition = 0f;
    }

    [HarmonyPatch(typeof(InputField))]
    [HarmonyPatch("MoveUp")]
    [HarmonyPatch(new Type[] { typeof(bool), typeof(bool) })]
    private class Patch_InputField_ConsoleInput
    {
        static bool Prefix(InputField __instance, bool shift, bool goToFirstChar)
        {
            if (__instance == GHConsole.inputfield && !GHConsole.isInputfieldBig)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    private class CommandEntry
    {
        public string docs;
        public commandAction action;
    }

    private class LatestLog
    {
        public GameObject g;
        public string l;
        public LogType t;
        public int amount = 1;
        public LatestLog(GameObject _g, string _l, LogType _t) { g = _g; l = _l; t = _t; }
    }

    public class ConsoleRightClickHandler : MonoBehaviour, IPointerDownHandler, IPointerExitHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            if (GHConsole.CurrentHoveredLine == null)
                GHConsole.CurrentHoveredLine = gameObject;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (GHConsole.CurrentHoveredLine == gameObject)
                GHConsole.CurrentHoveredLine = null;
        }
    }

    public class ConsoleTooltipHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public GameObject tooltip;

        public void Start()
        {
            if (tooltip)
                tooltip.SetActive(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (tooltip)
                tooltip.SetActive(true);
            GHConsole.CurrentHoveredLine = gameObject;
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            if (tooltip)
                tooltip.SetActive(false);
            GHConsole.CurrentHoveredLine = null;
        }
    }
}

[AttributeUsage(AttributeTargets.Method)]
public class ConsoleCommand : Attribute
{
    public string commandName;
    public string docstring;

    public ConsoleCommand(string name = null, string docs = null)
    {
        commandName = name;
        docstring = docs;
    }
}
