﻿namespace Launcher
{
    partial class ModInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModInstaller));
            this.text = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.marqueeBar = new System.Windows.Forms.Panel();
            this.marqueeBar_BG = new System.Windows.Forms.Panel();
            this.modBanner = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.Panel();
            this.moddescription = new System.Windows.Forms.Label();
            this.modauthor = new System.Windows.Forms.Label();
            this.modname = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Yes = new System.Windows.Forms.Button();
            this.No = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.disclaimer = new System.Windows.Forms.Label();
            this.marqueeBar_BG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modBanner)).BeginInit();
            this.panel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // text
            // 
            this.text.Font = new System.Drawing.Font("Arial", 12F);
            this.text.ForeColor = System.Drawing.Color.White;
            this.text.Location = new System.Drawing.Point(13, 5);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(479, 29);
            this.text.TabIndex = 5;
            this.text.Text = "Retrieving mod information...";
            this.text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.label3.Font = new System.Drawing.Font("Arial", 10F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-8, 500);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(995, 68);
            this.label3.TabIndex = 8;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // marqueeBar
            // 
            this.marqueeBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(29)))), ((int)(((byte)(35)))));
            this.marqueeBar.Location = new System.Drawing.Point(0, 0);
            this.marqueeBar.Name = "marqueeBar";
            this.marqueeBar.Size = new System.Drawing.Size(74, 25);
            this.marqueeBar.TabIndex = 10;
            // 
            // marqueeBar_BG
            // 
            this.marqueeBar_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.marqueeBar_BG.Controls.Add(this.marqueeBar);
            this.marqueeBar_BG.Location = new System.Drawing.Point(13, 38);
            this.marqueeBar_BG.Name = "marqueeBar_BG";
            this.marqueeBar_BG.Size = new System.Drawing.Size(479, 25);
            this.marqueeBar_BG.TabIndex = 9;
            // 
            // modBanner
            // 
            this.modBanner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.modBanner.BackgroundImage = global::Launcher.Properties.Resources.nobanner;
            this.modBanner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.modBanner.Image = global::Launcher.Properties.Resources.nobanner;
            this.modBanner.Location = new System.Drawing.Point(9, 10);
            this.modBanner.Name = "modBanner";
            this.modBanner.Size = new System.Drawing.Size(330, 100);
            this.modBanner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.modBanner.TabIndex = 10;
            this.modBanner.TabStop = false;
            this.modBanner.Tag = "r:5";
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.Controls.Add(this.moddescription);
            this.panel.Controls.Add(this.modauthor);
            this.panel.Controls.Add(this.modname);
            this.panel.Controls.Add(this.label4);
            this.panel.Controls.Add(this.Yes);
            this.panel.Controls.Add(this.No);
            this.panel.Controls.Add(this.panel2);
            this.panel.Controls.Add(this.modBanner);
            this.panel.Location = new System.Drawing.Point(631, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(348, 341);
            this.panel.TabIndex = 12;
            // 
            // moddescription
            // 
            this.moddescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.moddescription.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic);
            this.moddescription.ForeColor = System.Drawing.Color.White;
            this.moddescription.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.moddescription.Location = new System.Drawing.Point(9, 167);
            this.moddescription.Name = "moddescription";
            this.moddescription.Size = new System.Drawing.Size(330, 68);
            this.moddescription.TabIndex = 24;
            this.moddescription.Text = "................................................................................." +
    "................................................................................" +
    ".......";
            this.moddescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // modauthor
            // 
            this.modauthor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modauthor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modauthor.ForeColor = System.Drawing.Color.White;
            this.modauthor.Location = new System.Drawing.Point(9, 141);
            this.modauthor.Name = "modauthor";
            this.modauthor.Size = new System.Drawing.Size(330, 26);
            this.modauthor.TabIndex = 21;
            this.modauthor.Text = "..........";
            this.modauthor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // modname
            // 
            this.modname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modname.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modname.ForeColor = System.Drawing.Color.White;
            this.modname.Location = new System.Drawing.Point(9, 115);
            this.modname.Name = "modname";
            this.modname.Size = new System.Drawing.Size(330, 26);
            this.modname.TabIndex = 19;
            this.modname.Text = ".....";
            this.modname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(11, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 26);
            this.label4.TabIndex = 17;
            this.label4.Text = "Do you want to install this mod?";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Yes
            // 
            this.Yes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Yes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(163)))), ((int)(((byte)(90)))));
            this.Yes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(163)))), ((int)(((byte)(90)))));
            this.Yes.FlatAppearance.BorderSize = 0;
            this.Yes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.Yes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.Yes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Yes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Yes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Yes.Location = new System.Drawing.Point(9, 267);
            this.Yes.Name = "Yes";
            this.Yes.Size = new System.Drawing.Size(116, 23);
            this.Yes.TabIndex = 15;
            this.Yes.Tag = "r:5";
            this.Yes.Text = "Yes, Install it!";
            this.Yes.UseVisualStyleBackColor = false;
            this.Yes.Click += new System.EventHandler(this.Yes_Click);
            // 
            // No
            // 
            this.No.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.No.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(60)))), ((int)(((byte)(48)))));
            this.No.DialogResult = System.Windows.Forms.DialogResult.No;
            this.No.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(60)))), ((int)(((byte)(48)))));
            this.No.FlatAppearance.BorderSize = 0;
            this.No.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.No.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.No.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.No.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.No.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.No.Location = new System.Drawing.Point(131, 267);
            this.No.Name = "No";
            this.No.Size = new System.Drawing.Size(208, 23);
            this.No.TabIndex = 14;
            this.No.Tag = "r:5";
            this.No.Text = "No, Cancel the installation.";
            this.No.UseVisualStyleBackColor = false;
            this.No.Click += new System.EventHandler(this.No_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.panel2.Controls.Add(this.disclaimer);
            this.panel2.Location = new System.Drawing.Point(-10, 296);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(372, 58);
            this.panel2.TabIndex = 13;
            // 
            // disclaimer
            // 
            this.disclaimer.Font = new System.Drawing.Font("Arial", 8F);
            this.disclaimer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.disclaimer.Location = new System.Drawing.Point(11, 8);
            this.disclaimer.Name = "disclaimer";
            this.disclaimer.Size = new System.Drawing.Size(338, 32);
            this.disclaimer.TabIndex = 12;
            this.disclaimer.Text = "GreenHellModding takes no liability for user-created mods.\r\nInstallation is at yo" +
    "ur own risk.";
            this.disclaimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ModInstaller
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ClientSize = new System.Drawing.Size(979, 500);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.marqueeBar_BG);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.text);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ModInstaller";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Green Hell Mod Installer";
            this.marqueeBar_BG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.modBanner)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label text;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Panel marqueeBar;
        public System.Windows.Forms.Panel marqueeBar_BG;
        private System.Windows.Forms.PictureBox modBanner;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label disclaimer;
        private System.Windows.Forms.Button Yes;
        private System.Windows.Forms.Button No;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label modauthor;
        private System.Windows.Forms.Label modname;
        private System.Windows.Forms.Label moddescription;
    }
}