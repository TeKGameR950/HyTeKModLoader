﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Launcher
{
    public class SettingsTab : LauncherTab
    {
        public override void Initialize()
        {
            panel = Main.Get().settingsPanel;
            panel.Location = new Point(801, 40);
        }

        public async override void OnTabOpen()
        {
            base.OnTabOpen();
            Main.DownloadsTab.OnTabClose();
            Main.ServicesTab.OnTabClose();
            UpdateCacheSize();
            while (panel.Location.X > 324 && IsOpened)
            {
                await Task.Delay(1);
                panel.Location = new Point(Main.Clamp(panel.Location.X - 40, 324, 801), 40);
            }
        }

        public async override void OnTabClose()
        {
            base.OnTabClose();
            while (panel.Location.X < 801 && !IsOpened)
            {
                await Task.Delay(1);
                panel.Location = new Point(Main.Clamp(panel.Location.X + 40, 324, 801), 40);
            }
        }

        public static void UpdateCacheSize()
        {
            var modscachesize = DirSize(new DirectoryInfo(Main.FOLDER_CACHE_MODS));
            Main.Get().modscachelabel.Text = "Mods Cache (" + Math.Round((modscachesize / 1024.0f) / 1024.0f, 2) + "Mb)";
            var texturescachesize = DirSize(new DirectoryInfo(Main.FOLDER_CACHE_TEXTURES));
            Main.Get().texturescachelabel.Text = "Textures Cache (" + Math.Round((texturescachesize / 1024.0f) / 1024.0f, 2) + "Mb)";
        }

        static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
    }
}
