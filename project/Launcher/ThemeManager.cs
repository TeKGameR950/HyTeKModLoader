﻿using Launcher;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static LauncherConfiguration;

public class ThemeManager
{

    [DllImport("gdi32.dll")]
    static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);
    static PrivateFontCollection fonts = new PrivateFontCollection();

    public static void InitializeTheme()
    {
        Main window = Main.Get();
#if GAME_IS_RAFT
        window.BackColor = THEME_BACKGROUND_COLOR;
        window.topbar.BackColor = THEME_TOPBAR_COLOR;
        window.windowIcon.BackColor = THEME_TOPBAR_COLOR;
        window.windowTitle.BackColor = THEME_TOPBAR_COLOR;
        window.minimizeBtnHitbox.BackColor = THEME_TOPBAR_COLOR;
        window.closeBtn_Hitbox.BackColor = THEME_TOPBAR_COLOR;
        window.minimizeBtn.BackColor = THEME_TOPBAR_COLOR;
        window.closeBtn.BackColor = THEME_TOPBAR_COLOR;
        window.bottomBar.BackColor = THEME_TOPBAR_COLOR;
        window.downloadBtn_hitbox.BackColor = THEME_TOPBAR_COLOR;
        window.downloadsBtn.BackColor = THEME_TOPBAR_COLOR;
        window.websiteBtn_hitbox.BackColor = THEME_TOPBAR_COLOR;
        window.websiteBtn.BackColor = THEME_TOPBAR_COLOR;
        window.discordBtn_hitbox.BackColor = THEME_TOPBAR_COLOR;
        window.discordBtn.BackColor = THEME_TOPBAR_COLOR;
        window.statusBtn_hitbox.BackColor = THEME_TOPBAR_COLOR;
        window.statusBtn.BackColor = THEME_TOPBAR_COLOR;
        window.settingsBtn_hitbox.BackColor = THEME_TOPBAR_COLOR;
        window.settingsBtn.BackColor = THEME_TOPBAR_COLOR;
        window.triangleScifi2.BackColor = THEME_TOPBAR_COLOR;
        window.triangleScifi3.BackColor = THEME_TOPBAR_COLOR;
        window.triangleScifi1.BackColor = THEME_TOPBAR_COLOR;
        window.scifiBoxBottomRight.BackColor = THEME_TOPBAR_COLOR;
        window.modsFolderIcon.BackColor = THEME_TOPBAR_COLOR;
        window.modCreatorIcon.BackColor = THEME_TOPBAR_COLOR;
        window.modCreatorHitbox.BackColor = THEME_TOPBAR_COLOR;
        window.modsFolderHitbox.BackColor = THEME_TOPBAR_COLOR;
        window.modsFolderLabel.BackColor = THEME_TOPBAR_COLOR;
        window.modCreatorLabel.BackColor = THEME_TOPBAR_COLOR;
        window.lineSpacer.BackColor = THEME_LINE_COLOR;
        window.settingsPanel.BackColor = THEME_PANELS_BACKGROUND_COLOR;
        window.downloadPanel.BackColor = THEME_PANELS_BACKGROUND_COLOR;
        window.statusPanel.BackColor = THEME_PANELS_BACKGROUND_COLOR;
        window.noDownloadsLabel.BackColor = THEME_PANELS_BACKGROUND_COLOR;
        window.downloadPanel_Topbar.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusPanel_Topbar.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.settingsPanel_Topbar.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.downloadsPanelTitle.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusPanelTitle.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.settingsPanelTitle.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.downloadPanelCloseBtn.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusPanelCloseBtn.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.settingsPanelCloseBtn.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.coremodPanel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.assetsPanel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.coremodLabel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.coremodPercentage.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.assetsLabel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.assetsPercentage.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.coremodBar_BG.BackColor = THEME_BACKGROUND_COLOR;
        window.assetsBar_BG.BackColor = THEME_BACKGROUND_COLOR;
        window.coremodBar.BackColor = THEME_PROGRESSBAR_COLOR;
        window.assetsBar.BackColor = THEME_PROGRESSBAR_COLOR;
        window.apiStatus_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.fastdlStatus_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.masterServerStatus_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.websiteStatus_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusWebsiteLabel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusFastDLLabel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusRuntimeLabel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.statusAPILabel.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.websiteStatus.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.fastdlStatus.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.masterStatus.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.apiStatus.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.gamepath_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.gamePathTitle.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.cachemanagement_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.cacheManagementTitle.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.cachemanagement_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.cacheManagementTitle.BackColor = THEME_PANELS_TOPBAR_COLOR;
        //window.other_BG.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.automaticGameStart_BG.BackColor = THEME_PANELS_BACKGROUND_COLOR;
        window.automaticGameStart_BG2.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.automaticGameStartBackground.BackColor = THEME_TOPBAR_COLOR;
        window.StartGameFromSteamLabel.BackColor = THEME_TOPBAR_COLOR;
        window.startGameFromSteam.BackColor = THEME_TOPBAR_COLOR;
        window.gamePathBtn.BackColor = THEME_BACKGROUND_COLOR;
        window.gamePathLabel.BackColor = THEME_BACKGROUND_COLOR;
        window.gamePathBtn.FlatAppearance.BorderColor = THEME_BACKGROUND_COLOR;
        window.gamePathBtn.FlatAppearance.MouseDownBackColor = THEME_BACKGROUND_COLOR;
        window.gamePathBtn.FlatAppearance.MouseOverBackColor = THEME_SETTINGS_BUTTONS_HOVER_COLOR;
        window.modscache_BG.BackColor = THEME_BACKGROUND_COLOR;
        window.texturecache_BG.BackColor = THEME_BACKGROUND_COLOR;
        window.modscachelabel.BackColor = THEME_BACKGROUND_COLOR;
        window.texturescachelabel.BackColor = THEME_BACKGROUND_COLOR;
        window.ModsCacheClearBtn.BackColor = THEME_TOPBAR_COLOR;
        window.ModsCacheClearBtn.FlatAppearance.BorderColor = THEME_TOPBAR_COLOR;
        window.ModsCacheClearBtn.FlatAppearance.MouseDownBackColor = THEME_TOPBAR_COLOR;
        window.ModsCacheClearBtn.FlatAppearance.MouseOverBackColor = Color.FromArgb(70, 70, 70);
        window.TexturesCacheClearBtn.BackColor = THEME_TOPBAR_COLOR;
        window.TexturesCacheClearBtn.FlatAppearance.BorderColor = THEME_TOPBAR_COLOR;
        window.TexturesCacheClearBtn.FlatAppearance.MouseDownBackColor = THEME_TOPBAR_COLOR;
        window.TexturesCacheClearBtn.FlatAppearance.MouseOverBackColor = Color.FromArgb(70, 70, 70);
        //window.other_BG2.BackColor = THEME_PANELS_TOPBAR_COLOR;
        window.uninstall.BackColor = THEME_TOPBAR_COLOR;
        window.uninstall.FlatAppearance.BorderColor = THEME_TOPBAR_COLOR;
        window.uninstall.FlatAppearance.MouseDownBackColor = THEME_TOPBAR_COLOR;
        window.uninstall.FlatAppearance.MouseOverBackColor = Color.FromArgb(70, 70, 70);
        window.skipSplashScreen_BG.BackColor = THEME_TOPBAR_COLOR;
        window.splashscreenlabel.BackColor = THEME_TOPBAR_COLOR;
        window.checkBoxSplashScreen.BackColor = THEME_TOPBAR_COLOR;
        window.playBtn.BackColor = THEME_BACKGROUND_COLOR;
        window.playBtn.FlatAppearance.BorderColor = THEME_BACKGROUND_COLOR;
        window.playBtn.FlatAppearance.MouseDownBackColor = THEME_BACKGROUND_COLOR;
        window.playBtn.FlatAppearance.MouseOverBackColor = THEME_BACKGROUND_COLOR;
        window.downloadInfo.BackgroundImage = Launcher.Properties.Resources.raft_downloadPending;
        window.playBtn.BackgroundImage = Launcher.Properties.Resources.raft_playBtn;
        window.windowIcon.Image = Launcher.Properties.Resources.raft_rml_logo;
        window.Icon = Launcher.Properties.Resources.raft_rml_icon;
        window.gameLogo.BackgroundImage = Launcher.Properties.Resources.raft_logo;
        window.triangleScifi2.BackgroundImage = Launcher.Properties.Resources.raft_triangle;
        window.triangleScifi3.BackgroundImage = Launcher.Properties.Resources.raft_triangle;
        window.triangleScifi1.BackgroundImage = Launcher.Properties.Resources.raft_triangle;
        window.skipSplashScreen_BG.Hide();
        window.checkBoxSplashScreen.Hide();
        window.splashscreenlabel.Hide();
        //window.uninstall.Location = new Point(5, 5);
        //window.uninstall.Size = new Size(456, 52);
        window.coremodLabel.Text = "Downloading " + FILES_COREMOD + "...";
        window.assetsLabel.Text = "Downloading " + FILES_ASSETS + "...";
#endif
    }

    public static void InitializeFonts()
    {
        Main window = Main.Get();
        try
        {
            byte[] fontData = Launcher.Properties.Resources.Montserrat_Regular;
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Launcher.Properties.Resources.Montserrat_Regular.Length);
            AddFontMemResourceEx(fontPtr, (uint)Launcher.Properties.Resources.Montserrat_Regular.Length, IntPtr.Zero, ref dummy);
            Marshal.FreeCoTaskMem(fontPtr);

            fontData = Launcher.Properties.Resources.Montserrat_Bold;
            fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            dummy = 0;
            fonts.AddMemoryFont(fontPtr, Launcher.Properties.Resources.Montserrat_Bold.Length);
            AddFontMemResourceEx(fontPtr, (uint)Launcher.Properties.Resources.Montserrat_Bold.Length, IntPtr.Zero, ref dummy);
            Marshal.FreeCoTaskMem(fontPtr);

            window.windowTitle.Font = new Font(fonts.Families[0], 14.0F);
            window.websiteLabel.Font = new Font(fonts.Families[1], 10.0F);
            window.discordLabel.Font = new Font(fonts.Families[1], 10.0F);
            window.settingsLabel.Font = new Font(fonts.Families[1], 10.0F);
            window.statusLabel.Font = new Font(fonts.Families[1], 10.0F);
            window.downloadsLabel.Font = new Font(fonts.Families[1], 10.0F);
            window.welcomeLabel.Font = new Font(fonts.Families[1], 15.0F);
            window.playBtn.Font = new Font(fonts.Families[1], 10.0F);
            window.updateStatus.Font = new Font(fonts.Families[0], 12.0F);
            window.downloadWindowTitle.Font = new Font(fonts.Families[0], 15.0F);
            window.coremodLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.assetsLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.coremodPercentage.Font = new Font(fonts.Families[0], 10.0F);
            window.assetsPercentage.Font = new Font(fonts.Families[0], 10.0F);
            window.downloadsPanelTitle.Font = new Font(fonts.Families[0], 12.0F);
            window.statusPanelTitle.Font = new Font(fonts.Families[0], 12.0F);
            window.statusFastDLLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.statusRuntimeLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.statusWebsiteLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.statusAPILabel.Font = new Font(fonts.Families[0], 12.0F);
            window.noDownloadsLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.websiteStatus.Font = new Font(fonts.Families[0], 12.0F);
            window.fastdlStatus.Font = new Font(fonts.Families[0], 12.0F);
            window.masterStatus.Font = new Font(fonts.Families[0], 12.0F);
            window.apiStatus.Font = new Font(fonts.Families[0], 12.0F);
            window.modsFolderLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.modCreatorLabel.Font = new Font(fonts.Families[0], 12.0F);
            window.settingsPanelTitle.Font = new Font(fonts.Families[0], 12.0F);
            window.gamePathTitle.Font = new Font(fonts.Families[0], 12.0F);
            window.gamePathLabel.Font = new Font(fonts.Families[0], 10.0F);
            window.gamePathBtn.Font = new Font(fonts.Families[0], 13.0F);
            window.ModsCacheClearBtn.Font = new Font(fonts.Families[0], 11.0F);
            window.TexturesCacheClearBtn.Font = new Font(fonts.Families[0], 11.0F);
            window.uninstall.Font = new Font(fonts.Families[0], 11.0F);
            window.StartGameFromSteamLabel.Font = new Font(fonts.Families[0], 11.0F);
            window.splashscreenlabel.Font = new Font(fonts.Families[0], 11.0F);
            window.cacheManagementTitle.Font = new Font(fonts.Families[0], 12.0F);
            window.modscachelabel.Font = new Font(fonts.Families[0], 10.0F);
            window.texturescachelabel.Font = new Font(fonts.Families[0], 10.0F);
        }
        catch { }
    }
}
