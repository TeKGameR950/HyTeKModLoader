﻿namespace LauncherUpdater
{
    partial class UpdateAvailable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateAvailable));
            this.newVersionAvailable = new System.Windows.Forms.Label();
            this.later = new System.Windows.Forms.Button();
            this.updateNow = new System.Windows.Forms.Button();
            this.versionLabel = new System.Windows.Forms.Label();
            this.changelogLabel = new System.Windows.Forms.Label();
            this.viewFullChangelog = new System.Windows.Forms.LinkLabel();
            this.logo = new System.Windows.Forms.PictureBox();
            this.changelog = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // newVersionAvailable
            // 
            this.newVersionAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newVersionAvailable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newVersionAvailable.ForeColor = System.Drawing.Color.White;
            this.newVersionAvailable.Location = new System.Drawing.Point(65, 14);
            this.newVersionAvailable.Name = "newVersionAvailable";
            this.newVersionAvailable.Size = new System.Drawing.Size(245, 18);
            this.newVersionAvailable.TabIndex = 0;
            this.newVersionAvailable.Text = "A new version is available!";
            this.newVersionAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // later
            // 
            this.later.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.later.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(60)))), ((int)(((byte)(48)))));
            this.later.DialogResult = System.Windows.Forms.DialogResult.No;
            this.later.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(60)))), ((int)(((byte)(48)))));
            this.later.FlatAppearance.BorderSize = 0;
            this.later.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.later.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.later.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.later.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.later.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.later.Location = new System.Drawing.Point(243, 385);
            this.later.Name = "later";
            this.later.Size = new System.Drawing.Size(65, 23);
            this.later.TabIndex = 1;
            this.later.Tag = "r:5";
            this.later.Text = "Later";
            this.later.UseVisualStyleBackColor = false;
            // 
            // updateNow
            // 
            this.updateNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateNow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(163)))), ((int)(((byte)(90)))));
            this.updateNow.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.updateNow.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(163)))), ((int)(((byte)(90)))));
            this.updateNow.FlatAppearance.BorderSize = 0;
            this.updateNow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.updateNow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.updateNow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateNow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.updateNow.Location = new System.Drawing.Point(140, 385);
            this.updateNow.Name = "updateNow";
            this.updateNow.Size = new System.Drawing.Size(97, 23);
            this.updateNow.TabIndex = 2;
            this.updateNow.Tag = "r:5";
            this.updateNow.Text = "Update Now";
            this.updateNow.UseVisualStyleBackColor = false;
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.versionLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.ForeColor = System.Drawing.Color.White;
            this.versionLabel.Location = new System.Drawing.Point(64, 34);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(244, 19);
            this.versionLabel.TabIndex = 3;
            this.versionLabel.Text = "ModLoader 0.0.0";
            this.versionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // changelogLabel
            // 
            this.changelogLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.changelogLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changelogLabel.ForeColor = System.Drawing.Color.White;
            this.changelogLabel.Location = new System.Drawing.Point(12, 66);
            this.changelogLabel.Name = "changelogLabel";
            this.changelogLabel.Size = new System.Drawing.Size(293, 19);
            this.changelogLabel.TabIndex = 4;
            this.changelogLabel.Text = "Changelog :";
            this.changelogLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // viewFullChangelog
            // 
            this.viewFullChangelog.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.viewFullChangelog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.viewFullChangelog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewFullChangelog.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.viewFullChangelog.Location = new System.Drawing.Point(16, 353);
            this.viewFullChangelog.Name = "viewFullChangelog";
            this.viewFullChangelog.Size = new System.Drawing.Size(288, 23);
            this.viewFullChangelog.TabIndex = 6;
            this.viewFullChangelog.TabStop = true;
            this.viewFullChangelog.Text = "View Full Changelog";
            this.viewFullChangelog.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.viewFullChangelog.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.viewFullChangelog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // logo
            // 
            this.logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("logo.BackgroundImage")));
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Location = new System.Drawing.Point(9, 8);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(50, 50);
            this.logo.TabIndex = 7;
            this.logo.TabStop = false;
            // 
            // changelog
            // 
            this.changelog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.changelog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.changelog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.changelog.DetectUrls = false;
            this.changelog.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic);
            this.changelog.ForeColor = System.Drawing.Color.White;
            this.changelog.Location = new System.Drawing.Point(10, 91);
            this.changelog.Name = "changelog";
            this.changelog.ReadOnly = true;
            this.changelog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.changelog.ShortcutsEnabled = false;
            this.changelog.Size = new System.Drawing.Size(300, 258);
            this.changelog.TabIndex = 8;
            this.changelog.Tag = "r:5";
            this.changelog.Text = "..................";
            // 
            // UpdateAvailable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ClientSize = new System.Drawing.Size(320, 420);
            this.Controls.Add(this.changelog);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.viewFullChangelog);
            this.Controls.Add(this.changelogLabel);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.updateNow);
            this.Controls.Add(this.later);
            this.Controls.Add(this.newVersionAvailable);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(336, 459);
            this.Name = "UpdateAvailable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Version Available!";
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label newVersionAvailable;
        private System.Windows.Forms.Button later;
        private System.Windows.Forms.Button updateNow;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label changelogLabel;
        private System.Windows.Forms.LinkLabel viewFullChangelog;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.RichTextBox changelog;
    }
}