﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using static LauncherConfiguration;

namespace Launcher
{
    public partial class TOSForm : Form
    {
        Main defaultForm = null;
        public TOSForm(Main t)
        {
            defaultForm = t;
            InitializeComponent();
            InitializeTheme();
            Main.InitFormUtils(this);
        }

        public void InitializeTheme()
        {
#if GAME_IS_RAFT
            Text = "Raft Mod Loader - Terms Of Use";
            label2.Text = "By clicking on \"Agree\" I accept the terms and conditions of use and I have read the RaftModding confidentiality policy available at";
            Icon = Launcher.Properties.Resources.raft_rml_icon;
            BackColor = THEME_BACKGROUND_COLOR;
            linkLabel1.Text = "https://www.raftmodding.com/terms";
            linkLabel2.Text = "https://www.raftmodding.com/terms/software/";
            tos.Url = new Uri("https://www.raftmodding.com/terms/software/");
#endif
        }


        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            defaultForm.Opacity = 1;
            base.OnFormClosing(e);
        }

        bool mouseDown;
        int mouseX = 0, mouseY = 0;
        int mouseinX = 0, mouseinY = 0;

        private void DragMouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseinX = MousePosition.X - Bounds.X;
            mouseinY = MousePosition.Y - Bounds.Y;
        }

        private void DragMouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - mouseinX;
                mouseY = MousePosition.Y - mouseinY;

                SetDesktopLocation(mouseX, mouseY);
            }
        }

        private void denyBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void agreeBtn_Click(object sender, EventArgs e)
        {
            //defaultForm.ReopenForm(1);
            if (Main.CurrentConfig == null)
                Main.CurrentConfig = new LauncherConfiguration();
            Main.CurrentConfig.agreeWithTOS = 1;
            Main.UpdateConfigFile();
            Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://www." + MODLOADER_SITE + "/terms");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://www." + MODLOADER_SITE + "/terms/software/");
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
