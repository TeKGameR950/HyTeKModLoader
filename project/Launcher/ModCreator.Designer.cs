﻿namespace Launcher
{
    partial class ModCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModCreator));
            this.closeBtn = new System.Windows.Forms.PictureBox();
            this.windowIcon = new System.Windows.Forms.PictureBox();
            this.windowTitle = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.projectname = new System.Windows.Forms.TextBox();
            this.info = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.createBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.projectpath = new System.Windows.Forms.TextBox();
            this.custompathbutton = new System.Windows.Forms.Button();
            this.dots = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.closeBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // closeBtn
            // 
            this.closeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.closeBtn.BackgroundImage = global::Launcher.Properties.Resources.close;
            this.closeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeBtn.Location = new System.Drawing.Point(470, 10);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(20, 20);
            this.closeBtn.TabIndex = 27;
            this.closeBtn.TabStop = false;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // windowIcon
            // 
            this.windowIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.windowIcon.BackgroundImage = global::Launcher.Properties.Resources.greenhell_ghml_logo;
            this.windowIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.windowIcon.Location = new System.Drawing.Point(5, 5);
            this.windowIcon.Name = "windowIcon";
            this.windowIcon.Size = new System.Drawing.Size(30, 30);
            this.windowIcon.TabIndex = 24;
            this.windowIcon.TabStop = false;
            this.windowIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.windowIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.windowIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // windowTitle
            // 
            this.windowTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.windowTitle.Font = new System.Drawing.Font("Arial", 15F);
            this.windowTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.windowTitle.Location = new System.Drawing.Point(35, 5);
            this.windowTitle.Name = "windowTitle";
            this.windowTitle.Size = new System.Drawing.Size(419, 30);
            this.windowTitle.TabIndex = 25;
            this.windowTitle.Text = "Green Hell Mod C# Project Creator";
            this.windowTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.windowTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.windowTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(503, 40);
            this.pictureBox7.TabIndex = 26;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.pictureBox7.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.pictureBox7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.label1.Font = new System.Drawing.Font("Arial", 13F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label1.Location = new System.Drawing.Point(11, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 23);
            this.label1.TabIndex = 28;
            this.label1.Text = "Project Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectname
            // 
            this.projectname.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.projectname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.projectname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projectname.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectname.ForeColor = System.Drawing.Color.White;
            this.projectname.Location = new System.Drawing.Point(12, 211);
            this.projectname.Name = "projectname";
            this.projectname.Size = new System.Drawing.Size(478, 20);
            this.projectname.TabIndex = 29;
            this.projectname.Tag = "r:5";
            // 
            // info
            // 
            this.info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.info.Font = new System.Drawing.Font("Arial", 10F);
            this.info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.info.Location = new System.Drawing.Point(12, 50);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(476, 123);
            this.info.TabIndex = 32;
            this.info.Text = resources.GetString("info.Text");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.pictureBox1.Location = new System.Drawing.Point(0, 178);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(501, 182);
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "r:5";
            // 
            // createBtn
            // 
            this.createBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.createBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(163)))), ((int)(((byte)(90)))));
            this.createBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.createBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(163)))), ((int)(((byte)(90)))));
            this.createBtn.FlatAppearance.BorderSize = 0;
            this.createBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.createBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.createBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.createBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.createBtn.Location = new System.Drawing.Point(12, 285);
            this.createBtn.Name = "createBtn";
            this.createBtn.Size = new System.Drawing.Size(478, 46);
            this.createBtn.TabIndex = 35;
            this.createBtn.Tag = "r:5";
            this.createBtn.Text = "Create Project!";
            this.createBtn.UseVisualStyleBackColor = false;
            this.createBtn.Click += new System.EventHandler(this.createBtn_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.label3.Font = new System.Drawing.Font("Arial", 13F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label3.Location = new System.Drawing.Point(11, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(478, 23);
            this.label3.TabIndex = 38;
            this.label3.Text = "Project Path";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectpath
            // 
            this.projectpath.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.projectpath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.projectpath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projectpath.Enabled = false;
            this.projectpath.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectpath.ForeColor = System.Drawing.Color.White;
            this.projectpath.Location = new System.Drawing.Point(12, 259);
            this.projectpath.Name = "projectpath";
            this.projectpath.Size = new System.Drawing.Size(435, 20);
            this.projectpath.TabIndex = 39;
            this.projectpath.Tag = "r:5";
            // 
            // custompathbutton
            // 
            this.custompathbutton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.custompathbutton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.custompathbutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.custompathbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.custompathbutton.Font = new System.Drawing.Font("Arial", 20F);
            this.custompathbutton.ForeColor = System.Drawing.Color.White;
            this.custompathbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.custompathbutton.Location = new System.Drawing.Point(453, 259);
            this.custompathbutton.Name = "custompathbutton";
            this.custompathbutton.Size = new System.Drawing.Size(37, 20);
            this.custompathbutton.TabIndex = 40;
            this.custompathbutton.Tag = "r:5";
            this.custompathbutton.Text = "...";
            this.custompathbutton.UseVisualStyleBackColor = true;
            this.custompathbutton.Click += new System.EventHandler(this.button1_Click);
            // 
            // dots
            // 
            this.dots.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dots.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.dots.CausesValidation = false;
            this.dots.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dots.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dots.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.dots.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.dots.Location = new System.Drawing.Point(459, 256);
            this.dots.Name = "dots";
            this.dots.Size = new System.Drawing.Size(25, 27);
            this.dots.TabIndex = 0;
            this.dots.Tag = "r:5";
            this.dots.Text = "...";
            this.dots.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.dots.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dots_MouseClick);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.label5.Font = new System.Drawing.Font("Arial", 13F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label5.Location = new System.Drawing.Point(445, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 23);
            this.label5.TabIndex = 42;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.label6.Font = new System.Drawing.Font("Arial", 13F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label6.Location = new System.Drawing.Point(445, 279);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 23);
            this.label6.TabIndex = 43;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ModCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(500, 343);
            this.Controls.Add(this.createBtn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dots);
            this.Controls.Add(this.custompathbutton);
            this.Controls.Add(this.projectpath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.projectname);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.info);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.windowIcon);
            this.Controls.Add(this.windowTitle);
            this.Controls.Add(this.pictureBox7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimizeBox = false;
            this.Name = "ModCreator";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ModCreator";
            ((System.ComponentModel.ISupportInitialize)(this.closeBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox closeBtn;
        private System.Windows.Forms.PictureBox windowIcon;
        private System.Windows.Forms.Label windowTitle;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox projectname;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button createBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox projectpath;
        private System.Windows.Forms.Button custompathbutton;
        private System.Windows.Forms.Label dots;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}