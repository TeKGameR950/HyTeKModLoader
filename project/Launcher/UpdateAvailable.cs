﻿using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using static LauncherConfiguration;

namespace LauncherUpdater
{
    public partial class UpdateAvailable : Form
    {
        ModLoaderVersion latestUpdateInfo;

        public UpdateAvailable(ModLoaderVersion version)
        {
            InitializeComponent();
            latestUpdateInfo = version;
            versionLabel.Text = MODLOADER_NAME_NOSPACES + " " + version.version;
            changelog.Text = version.rawChangelog;
            InitializeTheme();
            Launcher.Main.InitFormUtils(this);
        }

        void InitializeTheme()
        {
#if GAME_IS_RAFT
            logo.BackgroundImage = Launcher.Properties.Resources.raft_rml_logo;
            BackColor = THEME_BACKGROUND_COLOR;
            changelog.BackColor = THEME_PANELS_BACKGROUND_COLOR;
#endif
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(latestUpdateInfo.fullChangelogUrl);
        }
    }
}
