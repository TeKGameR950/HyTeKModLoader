﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using ICSharpCode.SharpZipLib.Zip;
using System.Text;
using System.Net;
using System.ComponentModel;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Launcher
{
    public partial class ModInstaller : Form
    {
        public string modSlug = "";
        public bool isUriMod;
        public ModInfo modinfo = null;
        public JsonModInfo jsonmodinfo = null;
        public bool doMarqueeAnimation = true;
        public string gamePath;
        public ModInstaller(string slug, bool _isUriMod)
        {
            gamePath = Main.GetLauncherConfiguration().gamePath;
            if (!Main.IsGamePathValid(gamePath))
            {
                MessageBox.Show("Your Raft path is invalid! Please restart the launcher and try again.\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
            }

            isUriMod = _isUriMod;
            modSlug = slug;
            InitializeComponent();
            InitializeTheme();
            Size = new Size(519, 115);
            panel.Hide();
            Main.InitFormUtils(this);
            StartMarqueeProgressBar();
            FetchModInfo(modSlug);
        }

        public async void FetchModInfo(string slug)
        {
            if (isUriMod)
            {
                try
                {
                    Task<string> task = Main.RequestData("https://www." + LauncherConfiguration.MODLOADER_SITE + "/api/v1/mods/" + slug);
                    var waitTask = Task.Delay(15000);
                    Exception error = new Exception("https://www." + LauncherConfiguration.MODLOADER_SITE + "/api/v1/mods/" + slug + " Timed Out!\n\nPLEASE NOTE, WE CAN NOT HELP YOU WITH THIS PROBLEM, It is probably related to your antivirus or windows defender, your firewall or your ISP (internet provider) in some extremely rare cases.");
                    if (await Task.WhenAny(task, waitTask) == task)
                    {
                        if (!task.IsFaulted && task.Exception == null && !task.IsCanceled && task.IsCompleted)
                        {
                            string json = task.Result;
                            if (json == "null" || !json.StartsWith("{"))
                            {
                                MessageBox.Show("This mod seems to not be available anymore!\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Process.GetCurrentProcess().Kill();
                            }

                            ModInfo info = new ModInfo();

                            try
                            {
                                info = JsonConvert.DeserializeObject<ModInfo>(json);
                                modname.Text = info.name;
                                modauthor.Text = "Made by " + info.author;
                                moddescription.Text = info.description;
                            }
                            catch
                            {
                                MessageBox.Show("An error occured while fetching the mod information\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Process.GetCurrentProcess().Kill();
                            }

                            try { modBanner.Load(info.bannerImageUrl); } catch { }

                            modinfo = info;
                            doMarqueeAnimation = false;
                            Size = new Size(348 + 16, 351 + 29);
                            panel.Show();
                            CenterToScreen();
                        }
                        else
                        {
                            MessageBox.Show("An error occured while fetching the mod information\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Process.GetCurrentProcess().Kill();
                        }
                    }
                    else
                    {
                        MessageBox.Show("An error occured while fetching the mod information\n\n" + ErrorForm.FlattenException(error) + "\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                        Process.GetCurrentProcess().Kill();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured while fetching the mod information\n\n" + ErrorForm.FlattenException(ex) + "\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    Process.GetCurrentProcess().Kill();
                }
            }
            else
            {
                if (Path.GetDirectoryName(slug) == Path.Combine(gamePath, "mods"))
                {
                    MessageBox.Show("This mod is already installed properly!", "Mod Already Installed Properly!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process.GetCurrentProcess().Kill();
                }
                Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();
                using (Stream stream = File.OpenRead(slug))
                {
                    using (var zipInputStream = new ZipInputStream(stream))
                    {
                        while (zipInputStream.GetNextEntry() is ZipEntry v)
                        {
                            var zipentry = v.Name;
                            StreamReader reader = new StreamReader(zipInputStream);

                            var bytes = default(byte[]);
                            using (var memstream = new MemoryStream())
                            {
                                reader.BaseStream.CopyTo(memstream);
                                bytes = memstream.ToArray();
                            }
                            if (!files.ContainsKey(zipentry))
                                files.Add(zipentry, bytes);
                            if (zipentry.ToLower().EndsWith("modinfo.json"))
                            {
                                try
                                {
                                    jsonmodinfo = JsonConvert.DeserializeObject<JsonModInfo>(Encoding.UTF8.GetString(bytes));
                                }
                                catch { }
                            }
                        }
                    }
                    stream.Close();
                }

                if (jsonmodinfo == null)
                {
                    MessageBox.Show("This mod can't be installed using our installer!\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Process.GetCurrentProcess().Kill();
                }

                try
                {
                    modname.Text = jsonmodinfo.name;
                    modauthor.Text = "Made by " + jsonmodinfo.author;
                    moddescription.Text = jsonmodinfo.description;
                }
                catch
                {
                    MessageBox.Show("An error occured while fetching the mod information\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Process.GetCurrentProcess().Kill();
                }

                try { modBanner.Image = ByteToImage(files[jsonmodinfo.banner]); } catch { }

                doMarqueeAnimation = false;
                Size = new Size(348 + 16, 351 + 29);
                panel.Show();
                CenterToScreen();
            }
        }

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        void InitializeTheme()
        {
#if GAME_IS_RAFT
            marqueeBar.BackColor = LauncherConfiguration.THEME_PROGRESSBAR_COLOR;
            marqueeBar_BG.BackColor = LauncherConfiguration.THEME_PANELS_TOPBAR_COLOR;
            this.Text = "Raft Mod Installer";
            modBanner.BackColor = LauncherConfiguration.THEME_BACKGROUND_COLOR;
            BackColor = LauncherConfiguration.THEME_BACKGROUND_COLOR;
            panel2.BackColor = LauncherConfiguration.THEME_TOPBAR_COLOR;

            disclaimer.Text = disclaimer.Text.Replace("GreenHellModding", "RaftModding");
#endif
        }

        public async void StartMarqueeProgressBar()
        {
            if (!doMarqueeAnimation) { return; }
            marqueeBar.Location = new System.Drawing.Point(marqueeBar.Location.X + 5, marqueeBar.Location.Y);
            if (marqueeBar.Location.X > 479)
            {
                marqueeBar.Location = new System.Drawing.Point(-74, marqueeBar.Location.Y);
            }
            await Task.Delay(1);
            StartMarqueeProgressBar();
        }

        private void Yes_Click(object sender, EventArgs e)
        {
            InstallMod();
        }

        public void InstallMod()
        {
            if (!isUriMod)
            {
                string[] mods = Directory.GetFiles(Path.Combine(gamePath, "mods"), "*" + LauncherConfiguration.MOD_EXTENSION);
                foreach (string mod in mods)
                {
                    try
                    {
                        string name = "Default Mod Name";
                        using (Stream stream = File.OpenRead(mod))
                        {
                            using (var zipInputStream = new ZipInputStream(stream))
                            {
                                while (zipInputStream.GetNextEntry() is ZipEntry v)
                                {
                                    var zipentry = v.Name;
                                    StreamReader reader = new StreamReader(zipInputStream);

                                    var bytes = default(byte[]);
                                    using (var memstream = new MemoryStream())
                                    {
                                        reader.BaseStream.CopyTo(memstream);
                                        bytes = memstream.ToArray();
                                    }

                                    if (zipentry.ToLower().EndsWith("modinfo.json"))
                                    {
                                        try
                                        {
                                            name = JsonConvert.DeserializeObject<JsonModInfo>(Encoding.UTF8.GetString(bytes)).name;
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        if (name == jsonmodinfo.name)
                        {
                            File.Delete(mod);
                        }
                    }
                    catch
                    {
                        MessageBox.Show("An error occured while opening the mod file! It seems like the file is corrupted. Please try to redownload the latest mod version.\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Process.GetCurrentProcess().Kill();
                        return;
                    }
                }

                try
                {
                    string path = Path.Combine(gamePath, "mods/" + Path.GetFileName(modSlug));
                    File.Copy(modSlug, path, true);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured while installing the mod!\n" + ErrorForm.FlattenException(ex) + "\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    Process.GetCurrentProcess().Kill();
                }
                doMarqueeAnimation = false;
                marqueeBar.Size = new Size(479, 25);
                marqueeBar.Location = new Point(0, 0);
                text.Text = "Sucessfully installed mod \"" + jsonmodinfo.name + "\"!";
                MessageBox.Show("The mod has been successfully installed!", LauncherConfiguration.GAME_NAME + " Mod Installer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Process.GetCurrentProcess().Kill();
                return;
            }
            marqueeBar.Location = new Point(0, 0);
            marqueeBar.Size = new Size(0, 25);
            panel.Hide();
            Size = new Size(519, 115);
            CenterToScreen();
            // Download the latest file available for this mod and put it in cache temporarily until we do the move.
            text.Text = "Downloading mod \"" + modinfo.name + "\"... (0%)";
            WebClient assetsClient = new WebClient();
            assetsClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ModProgressChanged);
            assetsClient.DownloadFileCompleted += new AsyncCompletedEventHandler(ModDownloadCompleted);
            assetsClient.DownloadFileAsync(new Uri("https://www." + LauncherConfiguration.MODLOADER_SITE + modinfo.versions[0].downloadUrl + "?ignoreVirusScan=true"), Path.Combine(Main.FOLDER_CACHE_TEMPORARY, "modinstaller." + modSlug + LauncherConfiguration.MOD_EXTENSION));
        }

        private void ModProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            text.Text = "Downloading mod \"" + modinfo.name + "\"... (" + Math.Round(percentage).ToString() + "%)";
            float progress = (479f / 100f) * (float)Math.Round(percentage);
            marqueeBar.Size = new Size((int)Math.Floor(progress), 25);
            marqueeBar.Location = new Point(0, 0);
        }

        private void ModDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            marqueeBar.Size = new Size(74, 25);
            doMarqueeAnimation = true;
            StartMarqueeProgressBar();
            text.Text = "Installing mod \"" + modinfo.name + "\"...";

            string downloadedFilePath = Path.Combine(Main.FOLDER_CACHE_TEMPORARY, "modinstaller." + modSlug + LauncherConfiguration.MOD_EXTENSION);
            if (!File.Exists(downloadedFilePath) && new FileInfo(downloadedFilePath).Length > 10)
            {
                MessageBox.Show("An error occured while downloading the mod! The downloaded file doesn't exists! It might have been removed by an antivirus software or something else.\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
            }
            string fileHash = CalculateSHA256(downloadedFilePath);
            string requestedHash = modinfo.versions[0].fileHashSha256;
            if (requestedHash != null && !fileHash.Equals(requestedHash))
            {
                File.Delete(downloadedFilePath);
                MessageBox.Show("An error occured while downloading the mod! The downloaded file has a different hash than the requested file!\n\nLocal File Hash = " + fileHash + "\nRequested File Hash = " + requestedHash + "\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
                return;
            }

            string downloadedModName = "";
            try
            {
                // Get the downloaded mod name from modinfo.json
                using (Stream stream = File.OpenRead(downloadedFilePath))
                {
                    using (var zipInputStream = new ZipInputStream(stream))
                    {
                        while (zipInputStream.GetNextEntry() is ZipEntry v)
                        {
                            var zipentry = v.Name;
                            StreamReader reader = new StreamReader(zipInputStream);

                            var bytes = default(byte[]);
                            using (var memstream = new MemoryStream())
                            {
                                reader.BaseStream.CopyTo(memstream);
                                bytes = memstream.ToArray();
                            }

                            if (zipentry.ToLower().EndsWith("modinfo.json"))
                            {
                                try
                                {
                                    downloadedModName = JsonConvert.DeserializeObject<JsonModInfo>(Encoding.UTF8.GetString(bytes)).name;
                                }
                                catch { }
                            }
                        }
                    }
                }

            }
            catch
            {
                MessageBox.Show("An error occured while opening the mod file! It seems like the file is corrupted.\n\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
                return;
            }
            // Check if another mod has the same name variable in modinfo.json and delete it as it would result in two times the same mod.
            string[] mods = Directory.GetFiles(Path.Combine(gamePath, "mods"), "*" + LauncherConfiguration.MOD_EXTENSION);
            foreach (string mod in mods)
            {
                try
                {
                    string name = "Default Mod Name";
                    using (Stream stream = File.OpenRead(mod))
                    {
                        using (var zipInputStream = new ZipInputStream(stream))
                        {
                            while (zipInputStream.GetNextEntry() is ZipEntry v)
                            {
                                var zipentry = v.Name;
                                StreamReader reader = new StreamReader(zipInputStream);

                                var bytes = default(byte[]);
                                using (var memstream = new MemoryStream())
                                {
                                    reader.BaseStream.CopyTo(memstream);
                                    bytes = memstream.ToArray();
                                }

                                if (zipentry.ToLower().EndsWith("modinfo.json"))
                                {
                                    try
                                    {
                                        name = JsonConvert.DeserializeObject<JsonModInfo>(Encoding.UTF8.GetString(bytes)).name;
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                    if (name == downloadedModName)
                    {
                        File.Delete(mod);
                    }
                }
                catch { }
            }
            try
            {
                string path = Path.Combine(gamePath, "mods/modinstaller." + modSlug + LauncherConfiguration.MOD_EXTENSION);
                File.Copy(downloadedFilePath, path, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while installing the mod!\n" + ErrorForm.FlattenException(ex) + "\nIf the problem persists, please visit " + LauncherConfiguration.DISCORD_INVITEURL + " and ask for help in the #support channel.", LauncherConfiguration.MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
            }

            doMarqueeAnimation = false;
            marqueeBar.Size = new Size(479, 25);
            marqueeBar.Location = new Point(0, 0);
            text.Text = "Sucessfully installed mod \"" + modinfo.name + "\"!";
            MessageBox.Show("The mod has been successfully installed!");
            Process.GetCurrentProcess().Kill();
        }

        static string CalculateSHA256(string filename)
        {
            if (!File.Exists(filename))
            {
                return "FILE DOESN'T EXIST!";
            }
            using (var sha512 = SHA256.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = sha512.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        private void No_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }
    }
    public class JsonModInfo
    {
        public string name = "Unknown";
        public string author = "Unknown";
        public string description = "Unknown";
        public string banner = "Unknown";
    }

    public class ModInfo
    {
        [JsonProperty("title")]
        public string name = "Unknown";
        public string description = "Unknown";
        public string author = "Unknown";
        public string bannerImageUrl = "Unknown";

        [JsonProperty("mod-versions")]
        public ModInfoVersion[] versions;
    }

    public class ModInfoVersion
    {
        public string version = "Unknown";
        public string downloadUrl = "Unknown";
        public string fileHashSha256 = "Unknown";
    }

}
