﻿using System;
using System.Net;
using System.Reflection;
using System.Windows.Forms;

namespace LauncherUpdater
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Application.Run(new Main());
        }
    }
}
