﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LauncherUpdater
{
    public partial class UpdateAvailable : Form
    {
        ModLoaderVersion latestUpdateInfo;

        public UpdateAvailable(ModLoaderVersion version)
        {
            InitializeComponent();
            latestUpdateInfo = version;
            versionLabel.Text = "Launcher " + version.version;
            changelog.Text = version.rawChangelog;
            InitializeTheme();
            InitFormUtils(this);
        }

        public static void InitControlUtils(Control ctrl, Color backColorOverride = new Color())
        {
            RoundCorners(ctrl, GetRoundTag(ctrl));
        }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // width of ellipse
            int nHeightEllipse // height of ellipse
        );

        public static void RoundCorners(Control ctrl, int radius = 10)
        {
            if (radius <= 0) return;
            ctrl.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, ctrl.Width, ctrl.Height, radius, radius));
        }

        public static void InitFormUtils(Form form)
        {
            foreach (Control ctrl in GetAllControls(form))
                InitControlUtils(ctrl);
        }

        public static List<Control> GetAllControls(Form form)
        {
            List<Control> controlList = new List<Control>();
            GetAllControls(form, controlList);
            return controlList;
        }

        public static List<Control> GetAllControls(Control ctrl)
        {
            List<Control> controlList = new List<Control>();
            GetAllControls(ctrl, controlList);
            return controlList;
        }

        public static void GetAllControls(Control container, List<Control> controlList)
        {
            foreach (Control c in container.Controls)
            {
                controlList.Add(c);
                GetAllControls(c, controlList);
            }
        }

        public static int GetRoundTag(Control ctrl)
        {
            int result = -1;
            if (HasTag(ctrl, "r:"))
                result = int.Parse(GetTag(ctrl, "r:"));
            return result;
        }

        public static bool HasTag(Control ctrl, string tag)
        {
            if (ctrl.Tag == null) return false;
            return ctrl.Tag.ToString().Split(';').Any(x => x.StartsWith(tag));
        }

        public static string GetTag(Control ctrl, string tag)
        {
            if (ctrl.Tag == null) return "";
            string[] tags = ctrl.Tag.ToString().Split(';');
            foreach (string t in tags)
            {
                if (t.ToLower().StartsWith(tag))
                    return t.Substring(tag.Length);
            }
            return "";
        }

        void InitializeTheme()
        {
#if GAME_IS_RAFT
            logo.BackgroundImage = Properties.Resources.raft_rml_logo;
            BackColor = Color.FromArgb(79, 79, 79);
            changelog.BackColor = Color.FromArgb(122, 122, 122);
#endif
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(latestUpdateInfo.fullChangelogUrl);
        }
    }
}
